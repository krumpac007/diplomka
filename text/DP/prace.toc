\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {czech}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Úvod}{15}{chapter*.7}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Dobývání znalostí z~databází}{17}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Stručná historie}{17}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Dva pohledy na dobývání znalostí z~dat}{17}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Typické úlohy dobývání znalostí}{20}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Algoritmus Knowledge Explorer}{23}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Značení}{23}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Cíl algoritmu}{24}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Popis algoritmu}{25}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Modifikace pro problém s~více jak 2 cílovými třídami}{26}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Zpracování číselných atributů}{28}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Evaluace}{31}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Softwarové aspekty}{33}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}RapidMiner Studio}{33}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Vývoj doplňků pro sytém Rapidminer}{34}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Problémy při vývoji}{35}{subsection.3.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Postup vývoje}{35}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Uložení dat}{39}{subsection.3.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Implementace vlastního operátoru}{41}{subsection.3.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Dokumentace a pomoc při vývoji}{42}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementace}{43}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Datové třídy}{43}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Třída InputDataTable}{44}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Třída Catagory}{44}{subsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Třída Combination}{45}{subsection.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.4}Třída AssociationRule}{46}{subsection.4.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.5}Třída ValueClassCount}{46}{subsection.4.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.6}Třída Interval}{47}{subsection.4.1.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.7}Třída ResultCategory}{47}{subsection.4.1.7}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.8}Třída ValidationResult}{48}{subsection.4.1.8}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Operátory}{49}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Operátor Knowledge Explorer}{49}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Konstruktor}{49}{section*.32}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Parametry operátoru}{50}{section*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Metoda \textit {learn()}}{53}{section*.35}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Metoda \textit {supportsCapability()}}{55}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Výstup operátoru}{55}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Operátor KEX Discretization}{58}{section.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Objekty pro výstup operátoru a renderery}{59}{section.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Třída KnowledgeExplorerModel}{60}{subsection.4.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Renderery}{60}{subsection.4.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Pomocné třídy}{62}{section.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}Třída Functions}{62}{subsection.4.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.2}Třída TestClass}{63}{subsection.4.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Testovací třídy}{63}{section.4.6}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Testování a výsledky}{65}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Testování správnosti algoritmu}{65}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Testované případy}{67}{subsection.5.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Rozíly ve výsledcích}{68}{subsection.5.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Porovnání s~jinými algoritmy}{70}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Optimalizace}{73}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Vliv parametrů algoritmu na přesnost klasifikace a dobu běhu}{74}{section.5.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.1}Paramatr $l_{max}$}{74}{subsection.5.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.2}Parametr $f_{min}$}{75}{subsection.5.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.3}Parametr $P_{min}$}{76}{subsection.5.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3.4}Parametr $\alpha $}{78}{subsection.5.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Testování a porovnání operátoru KEX Discretization}{79}{section.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Závěr}{83}{chapter*.63}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Seznam použité literatury}{85}{chapter*.64}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Důkaz komutativity a asociativity funkce $w^{\oplus }$}{91}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {B}Adresářová struktura projektu a instalační příručka}{93}{appendix.B}%
