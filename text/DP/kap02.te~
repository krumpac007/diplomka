\chapter{Algoritmus Knowledge Explorer}
\label{chap:kex}
V této kapitole bude popsán algoritmus Knowledge Explorer, jehož implementace v systému Rapidminer Studio \cite{Rapidminer} je hlavním úkolem a cílem této práce. Popis algoritmu vychází z článku \cite{KEX}. Jelikož je algoritmus stěžejní součástí této práce, považuji za vhodné uvést jeho popis i zde a nejen jako odkaz na článek. Krom toho jsem také v průběhu studia algoritmu a jeho implementace narazil na několik chyb, které článek obsahuje. Rozdíly oproti původnímu článku budou na příslušných místech popsány.

\section{Značení}
Algoritmus KEX (Knowledge EXplorer) pracuje s kategoriálními daty v podobě datové matice. Řádky matice odpovídají objektům (v dalším textu též označeno jako \textit{příklad} nebo \textit{pozorování}), sloupce odpovídají \textit{atributům} (charakteristikám) daných objektů. Jako \textit{kategorie} bude označována konkrétní hodnota určitého atributu, značena bude jako $A(v)$, kde $A$ označuje atribut a $v$ jeho hodnotu. \textit{Kategorií} tedy může být například $motor(benzín)$ odpovídající kategorii \textit{benzínový motor}. 

\textit{Kombinací} budeme nadále rozumět konjunkci \textit{kategorií}. Počet \textit{kategorií} v této konjuknci budeme označovat jako \textit{délku} kombinace. Kombinace $C$ bude značena následovně:
\begin{equation}
C = \left[ A_{j_1} (v_{k_1}), A_{j_2} (v_{k_2}), \dots, A_{j_l} (v_{k_l}) \right] =
 A_{j_1} (v_{k_1}) \land A_{j_2} (v_{k_2}) \land \dots \land A_{j_l} (v_{k_l}) 
\end{equation}

Délka takové kombinace je $l$\footnote{zde například článek nesprávně uvádí jako délku kombinace $k$}.
Jako \textit{frekvenci} kombinace $C$ budeme rozumět relativní počet výskytů dané kombinace ve vstupních datech $D$. Značena bude $ \lVert C \rVert $.
\begin{equation}
\lVert C \rVert = \frac{\text {počet objektů splňující vlastnosti kombinace C}}{\text{celkový počet objektů v datasetu}}
\end{equation}

Článek \cite{KEX} uvádí jako \textit{frekvenci} pouze počet objektů, spňující vlastnosti dané kombinace, nicméně v algoritmu se s frekvencí pracuje tak, jak je definovaná zde. Pro odlišení bude pro frekvenci ve významu počtu objektů splňujících vlastnosti kombinace $C$ využíváno značení $\lvert C \rvert$.


Pro 2 kombinace $C_{1}$ a $C_{2}$, které nemají žádný společný atribut, můžeme vytvořit \textit{asociační pravidla} 
$C_{1} \Rightarrow C_{2}$ a čtyřpolní tabulku ve formě:

\begin{table}[H]
\centering
\begin{tabular}{ccccc}
           & $C_2$ & $\neg C_2$ & $\sum$ &  \\ \hline
$C_1$      & $a$     & $b$          & $r$      &  \\ 
$\neg C_1$ & $c$     & $d$          & $s$      &  \\ \hline
           & $k$     & $l$          & $n$      & 
\end{tabular}
\end{table}

kde 
\begin{itemize}
 \item $a$ označuje počet pozorování, které splňují $C_1$ i $C_2$
 \item $b$ označuje počet pozorování, které splňují $C_1$, ale ne $C_2$
 \item $c$ označuje počet pozorování, které nesplňují $C_1$, ale splňují $C_2$ 
 \item $d$ označuje počet pozorování, které nesplňují $C_1$ ani $C_2$
\end{itemize}

Z kontingenční tabulky tak vyplývá:
\[
\lvert C_1 \land C_2 \rvert = a
\]
\[
\lvert C_1 \rvert = a + b
\]
\[
\lvert C_2 \rvert = a +c
\]

U asociačního pravidla $C_{1} \Rightarrow C_{2}$ můžeme $C_1$ označit jako předpoklad (\textit{antecedent}) a $C_2$ jako důsledek (\textit{sukcedent}).

\textit{Validitu} asociačního pravidla $C_{1} \Rightarrow C_{2}$ definujeme jako podmíněnou pravděpodobnost označenou $P(C_2 | C_1)$.

\begin{equation}
P(C_2 | C_1) = \frac{\lvert C_1 \land C_2 \rvert}{\lvert C_1 \rvert} = \frac{a}{a+b}
\end{equation}

\textit{Pokrytí} asociačního pravidla $C_{1} \Rightarrow C_{2}$ definujeme jako podmíněnou pravděpodobnost označenou $P(C_1 | C_2)$\footnote{Zde je další chyba ve zdrojovém článku, kdy ve jmenovateli zlomku uvádí $\lvert C_1 \rvert$ namísto $\lvert C_2 \rvert$}.

\begin{equation}
P(C_1 | C_2) = \frac{\lvert C_1 \land C_2 \rvert}{\lvert C_2 \rvert} = \frac{a}{a+c}
\end{equation}

Pro účely algoritmu Knowledge Explorer tvoří \textit{sukcedenty} asociačních pravidel pouze kombinace délky 1 odpovídající hledaným třídám definovaným ve vstupních datech.

\section{Cíl algoritmu}
Cílem algoritmu Knowledge Explorer je nalézt taková asociační pravidla, která budou tvořit znalostní základnu pro daný dataset, tzn. na jejichž základě bude možné klasifikovat nová pozorování. Výstupem algoritmu je tedy znalostní báze (KB), skládající se z pravidel 
\[
Ant \Rightarrow C (w)
\] 
kde
\begin{itemize}
 \item $Ant$ označuje kombinaci
 \item $C$ označuje výslednou třídu
 \item $w$ označuje váhu (nejistotu) tohoto pravidla
\end{itemize}

Odvození cílové třídy v případě klasifikace nových pozorování na základě znalostní báze se provádí pomocí řetězení pravidel. Nejprve se pro dané pozorování naleznou všechna pravidla, jejichž \textit{antecedenty} jsou splněny daným pozorováním a následně se z těchto pravidel spočítá kombinovaná váha pro každou z cílových tříd. Dané pozorování je poté přiřazeno k té třídě, jejíž kombinovaná váha je nejvyšší. Pro výpočet kombinované váhy $w^{\oplus}$ dvou pravidel o vahách $w_1$ a $w_2$ je použita následující kombinační funkce:

\begin{equation}
w^{\oplus} = w_1 \oplus w_2 = \frac{w_1 \cdot w_2}{w_1 \cdot w_2 + (1 - w_1) \cdot (1 - w_2)}
\end{equation}

Tato kombinační funkce je komutativní a asociativní. Jelikož článek \cite{KEX} tuto informaci (která se dále ukáže jako důležitá) neposkytuje, je toto tvrzení dokázáno v příloze \ref{chap:dukaz}.

\section{Popis algoritmu}
Hledání znalostní báze v algoritmu Knowledge Explorer probíhá iterativním způsobem pomocí rozšiřování pravidel $Ant \Rightarrow C$. Na začátku algoritmu jsou do znalostní báze přidána \textit{prázdná pravidla} $\emptyset \Rightarrow C$ (pro každou z cílových tříd $C$). Váha těchto pravidel je nastavena jako frekvence dané třídy $C$ v datasetu. Algoritmus končí při otestování všech možných pravidel splňujících vstupní parametry algoritmu (maximální délka antecedentu, minimální frekvence antecedentu a minimální validita pravidla $Ant \Rightarrow C$). Pravidla jsou evaluována postupně na základě klesající frekvence antecedentu v pravidle (což má za následek, že `nejsilnější` pravidla jsou otestována jako první).

Evaluace pravidel spočívá v porovnání validity daného pravidla a jeho váhy. Pokud se tyto hodnoty \textit{signifikantně liší} na základě testu $\chi^2$ (mj. např. \cite{chisq}), tak je toto pravidlo přidáno do znalostní báze. Pro výpočet hodnoty $\chi^2$ se používá následující vzorec:
\begin{equation}
\label{math:chisq}
CHISQ(r) = \sum_{i=1}^{T} \frac{({\lvert Ant \rvert}_i - {\lvert Ant \rvert} \cdot w_{i}^{\oplus}(Ant))^2}{{\lvert Ant \rvert} \cdot w_{i}^{\oplus}(Ant)}
\end{equation}

kde 
\begin{itemize}
 \item $r$ představuje pravidlo, pro které počítáme hodnotu $\chi^2$
 \item $T$ představuje počet cílových tříd
 \item ${\lvert Ant \rvert}_i$ představuje počet příkladů třídy $C_i$, které jsou pokryty antecedentem \textit{Ant}
 \item $ w_{i}^{\oplus}(Ant)$ představuje složenou váhu vypočtenou pomocí kombinační funkce $\oplus$ z vah všech pravidel ze znalostní báze, jejichž antecedenty jsou obsaženy v \textit{Ant} a sukcedenty jsou $C_i$ -- tj. podpravidel pravidla $r$
\end{itemize}


Tento vzorec vychází z klasického vzorce pro $\chi^2$ test: \[\chi^2 = \sum\frac{(O-E)^2}{E}\]
kde $O$ představuje pozorovaný počet případů a $E$ předpokládaný počet případů. Modifikace vzorce použitá v algoritmu spočívá v tom, že se předpokládaný počet hodnot $E$ nepočítá tradičním způsobem (tzn. ze čtyřpolní tabulky), ale pomocí váhy $w^\oplus$ pravidla.

Je-li vypočtená hodnota $CHISQ$ větší než hodnota $\chi^2$ distribuce, přidáme do znalostní báze pravidlo $Ant \Rightarrow C$ pro každou z cílových tříd $C_i$. Váha $w$ každého tohoto pravidla je následně nastavena tak, aby platilo: 
\begin{equation}
\label{math:newruleweight}
 w^{\oplus}(Ant) \oplus w = P(C_i|Ant) 
\end{equation}

Aby tato rovnice platila, je možné váhu $w$ spočítat následovně:
\begin{equation}
\label{math:newruleweightcalc}
w = \frac{u}{1+u}
\end{equation}

kde 
\begin{equation}
u = \dfrac{\dfrac{P(C_i|Ant)}{1 - P(C_i|Ant)}}{\dfrac{w^{\oplus}(Ant)}{1 - w^{\oplus}(Ant)}}
\end{equation}


Celkový popis algoritmu demonstruje pseudokód \ref{algo:kex}.

\begin{algorithm}
\SetAlgoVlined
\DontPrintSemicolon
\SetKwInput{KwInput}{Vstup}
\SetKwInput{KwOutput}{Výstup}
\SetKwIF{If}{ElseIf}{Else}{Pokud}{Pak}{}{Jinak}{}%

\SetKwFor{While}{Dokud}{}%

\SetKwFor{For}{Pro}{dělej}{endfor}%

\SetKwFor{ForEach}{Pro všechny}{}{}%


\KwInput{datová matice $D$, maximální délka antecedentu $l_{max}$, minimální frekvence antecedentu $f_{min}$, minimální validita pravidla $P_{min}$, seznam cílových tříd $C$, hladina významnosti $\alpha$ }
\KwOutput{seznam pravidel reprezentujících znalostní bázi $KB$}
\vspace{\baselineskip}

\hrulefill

\textbf{Inicializace}


\nlset{1.} Nechť $CAT$ je seznam kategorií $A(v)$, kde ${\lVert A(v) \rVert} \geq f_{min}$, seřazený v sestupném pořadí podle frekvence ${\lVert A(v) \rVert}$

\nlset{2.} Nechť $OPEN$ je seznam asociačních pravidel $A(v) \Rightarrow C$, pro které platí ${\lVert A(v) \rVert} \geq f_{min}$, seřazený v sestupném pořadí podle frekvence ${\lVert A(v) \rVert}$

\nlset{3.} Nechť $KB$ je seznam obsahující prázdná pravidla $\emptyset \Rightarrow C(w)$ pro každou z cílových tříd $C$, kde $w$ je relativní frekvence třídy $C$ v $D$

\nlset{4.} Nechť $T$ je počet cílových tříd

\hrulefill

\textbf{Hlavní část}

\While{$ OPEN \neq \emptyset $}{
\nlset{1.} nechť $r$ je první pravidlo $Ant \Rightarrow C$ z $OPEN$

\nlset{2.} nechť $P$ je validita $r$, tedy $P=P(C|Ant)$

\nlset{3.} \If{$P(C|Ant) \geq P_{min} \lor P(C|Ant) \leq (1-P_{min})$}{
    \nlset{3.1} nechť $w^{\oplus}(Ant)$ je váha složená z vah všech pravidel přítomných v $KB$, jež jsou podpravidlem $r$
    
    \nlset{3.1.1} \If{$w^{\oplus}(Ant)$ se na základě $\chi^2$ testu o $T-1$ stupních volnosti signifikantně liší od $P$ na hladině významnosti $\alpha$}
    {přidáme  do $KB$ pravidla $Ant \Rightarrow C_i(w)$ pro každou cílovou třídu $C_i$, kde pro $w$ platí: $w^{\oplus}(Ant) \oplus w = P(C_i|Ant)$  }
}

\nlset{4.} \If{$\lvert Ant \rvert$ < $l_{max}$ }{
    \nlset{4.1} \ForEach{kategorie $A(v)$ z $CAT$ takové, že $A$ se nevyskytuje v Ant a zároveň $A(v)$ se v $CAT$ vyskytuje až za všemi kategoriemi z Ant}{
    \nlset{4.1.1} vytvořme novou kombinaci $AntA = Ant \land A(v)$
    
    \nlset{4.1.2} \If{$\lVert AntA \rVert \geq f_{min}$}{
        přidejme pravidlo $AntA \Rightarrow C$ do $OPEN$ tak, aby $OPEN$ zůstalo seřazené
    }
    }
} 
\nlset{5.} Odeberme $r$ z $OPEN$ 
}
\caption{Algoritmus Knowledge Explorer}
\label{algo:kex}
\end{algorithm}

\subsection{Modifikace pro problém s více jak 2 cílovými třídami}
\label{sub:multiclassproblem}
Pro problémy s 3 a více cílovými třídami je potřeba implementovat určité modifikace. Hlavním důvodem je rozlišné zpracování neurčitosti při práci s váhami, resp. validitou. Při práci s váhou pravidla je maximální neurčitost při hodnotě 0,5 (nehledě na počet cílových tříd), zatímco pro validitu je maximální neurčitost při hodnotě validity $1/T$, kde $T$ je počet cílových tříd (pro problém se dvěma cílovými třídami jsou tyto dvě hodnoty stejné).
Z tohoto důvodu je při více jak 2 cílových třídách potřeba využít mapování mezi vahou a validitou takové, že:
\begin{itemize}
 \item váha 0 odpovídá validitě 0
 \item váha 1 odpovídá validitě 1
 \item váha 0,5 odpovídá validitě $1/T$
\end{itemize}

Článek \cite{KEX} přináší takové mapování pomocí následujících vztahů:


\begin{gather}
\label{math:validitytoweight}
weight = \frac{T}{2} \cdot validity \text{  pro } validity \in [0; 1/T]\\
weight = \frac{T}{2(T-1)} \cdot validity + \left(1-\frac{T}{2(T-1)}\right) \text{  pro } validity \in [1/T; 1]
\end{gather}

resp.

\begin{gather}
validity = \frac{2}{T} \cdot weight \text{  pro } weight \in [0; 0,5]\\
validity = \frac{2(T-1)}{T} \cdot weight + \left(1 - \frac{2(T-1)}{T} \right) \text{  pro } weight \in [0,5; 1]
\end{gather}

Za využití tohoto mapování je již možné ukázat změny v algoritmu pro řešení problému s více než 2 cílovými třídami:

\begin{enumerate}
 \item váha \textit{prázdného pravidla} odpovídá relativní frekvenci dané třídy transformované na váhu (krok 3)
 \item $\chi^2$ provádí porovnání mezi validitou a složenou váhou transformovanou na validitu (krok 3.1.1)
 \item váha $w$ pravidla $Ant \Rightarrow C(w)$ je vypočtena ze vztahu $w \oplus w^{\oplus}(Ant) = P'(C_i|Ant)$, kde $P'(C_i|Ant)$ odpovídá validitě transformované na váhu (krok 3.1.1)
\end{enumerate}


\section{Zpracování číselných atributů}

Jak již bylo řečeno, algoritmus Knowledge Explorer zpracovává pouze vstupy s kategoriálními atributy. Číselné atributy tedy musí být určitým způsobem diskretizovány. Článek \cite{KEX} přináší vlastní způsob diskretizace, který funguje na podobném principu jako celý algoritmus, tedy na základě $\chi^2$ testu. V případě diskretizace se $\chi^2$ test využívá tak, aby se vytvořili intervaly, pro které platí, že aposteriorní distribuce tříd $P(C|interval)$ se signifikantně liší od apriorní distribuce třídy $P(C)$. Výpočet bude ilustrován na příkladu vycházejícím z následující kontingenční tabulky vytvořené z hodnot atributu

\begin{table}[h]
\centering
\begin{tabular}{ccccc}
                             & \multicolumn{3}{l}{Třída}         &     \\
\multicolumn{1}{l|}{Hodnota} & $C_1$ & $C_2$ & \multicolumn{1}{l|}{$C_3$} & $\sum$ \\ \hline
\multicolumn{1}{l|}{6}       & 1  & 0  & \multicolumn{1}{l|}{0}  & 1   \\
\multicolumn{1}{l|}{6.5}     & 0  & 1  & \multicolumn{1}{l|}{1}  & 2   \\
\multicolumn{1}{l|}{7}       & 0  & 3  & \multicolumn{1}{l|}{0}   & 3   \\
\multicolumn{1}{l|}{7.5}     & 0  & 0  & \multicolumn{1}{l|}{3}  & 3   \\ \hline
\multicolumn{1}{l|}{$\sum$}        & 1  & 4  & \multicolumn{1}{l|}{4}  & 9  
\end{tabular}
\end{table}

$\chi^2$ pro hodnotu atributu 6 se vypočítá následujícím způsobem:

\begin{gather*}
    E(C_{1})_{6} = \frac{1 \cdot 1}{9} = \frac{1}{9}\\
    E(C_{2})_{6} = \frac{4 \cdot 1}{9} = \frac{4}{9}\\
    E(C_{3})_{6} = \frac{4 \cdot 1}{9} = \frac{4}{9}\\
    \chi^2(6) = \sum_{i=1}^T \frac{(O_i-E_i)^2}{E_i} = \frac{(1-\frac{4}{9})^2}{\frac{4}{9}} + \frac{(0-\frac{4}{9})^2}{\frac{4}{9}} + \frac{(0-\frac{4}{9})^2}{\frac{4}{9}} = 8
\end{gather*}
Celý algoritmus diskretizace popisuje pseudokód \ref{algo:discretization}.

Pro dokončení předchozího příkladu s výpočtem hodnoty $\chi^2$ je ještě vhodné podotknout, že pro hladinu významnosti $\alpha = 0,05$ by byla hodnotě 6 přiřazena třída $C_1$ (tabulková hodnota $\chi^2$ je přibližně 5,9), ale v případě hladiny významnosti $\alpha = 0,01$ třída '\texttt{UNKNOWN}' (tabulková hodnota $\chi^2$ je přibližně 9,2).

\begin{algorithm}
\SetAlgoVlined
\DontPrintSemicolon
\SetKwInput{KwInput}{Vstup}
\SetKwInput{KwOutput}{Výstup}
\SetKwIF{If}{ElseIf}{Else}{Pokud}{Pak}{Jinak pokud}{Jinak}{}%

\SetKwFor{While}{Dokud}{}%

\SetKwFor{For}{Pro}{dělej}{endfor}%

\SetKwFor{ForEach}{Pro všechny}{}{}%


\KwInput{datová matice $D$, číselný atribut k diskretizaci $att$, hladina významnosti $\alpha$}
\KwOutput{intervaly pokrývající hodnoty atributu $att$}
\vspace{\baselineskip}

\hrulefill

\textbf{Hlavní část}

nechť $l$ je seznam hodnot atributu $att$

nechť $T$ je počet cílových tříd v datasetu $D$


\nlset{1.} seřadíme seznam $l$

\nlset{2.} \ForEach{hodnoty $v$ ze seznamu $l$}{
    \nlset{2.1} vypočteme počet výskytů hodnoty $v$ pro každou z cílových tříd $C$
    
    \nlset{2.2} hodnotě $v$ přiřadíme indikátor třídy pomocí procedury \texttt{ASSIGN}
}

\nlset{3.} vytvoříme intervaly pomocí procedury \texttt{INTERVAL}

\hrulefill

\textbf{Procedura ASSIGN}

\nlset{1.} \If{ pro hodnotu v všechny objekty spadají do jedné třídy C}
{
\nlset{1.1}    přiřadíme hodnotě $v$ tuto třídu
}
\ElseIf{ se rozložení objektů mezi cílové třídy signifikantně liší od frekvencí cílových tříd v datasetu (na základě $\chi^2$ testu s $T-1$ stupni volnosti na hladině významnosti $\alpha$)}
{
\nlset{1.2}    přiřadíme hodnotě $v$ třídu, se kterou se v $D$ vyskytuje nejčastěji
}
\Else{
\nlset{1.3} přiřadíme hodnotě $v$ třídu \texttt{UNKNOWN}}

\hrulefill

\textbf{Procedura INTERVAL}

\nlset{1}\If {sekvence hodnot spadá do stejné třídy}{
\nlset{1.1}vytvořme z těchto hodnot interval $INT_i = [LBound_i; RBound_i]$, kde $LBound_i$, resp. $RBound_i$ je první, resp. poslední hodnota dané sekvence}

\nlset{2}\If{interval $INT_i$ spadá do třídy \texttt{UNKNOWN}}{
\nlset{2.1}    \If {sousední intervaly $INT_{i-1}$ $INT_{i+1}$ spadají do stejné třídy $C$}{
        Vytvořme interval spojením intervalů $INT_{i-1} \cup INT_{i} \cup INT_{i+1}$
    }
\nlset{2.2}    \Else{ Vytvořme interval připojením intervalu $INT_{i}$ buď k intervalu $INT_{i-1}$ nebo k intervalu $INT_{i+1}$}
}

\nlset{3} Vytvořme spojité pokrytí hodnot atributu $att$ přiřazením {                                                                             $UBound_{i-1} = UBound_{i-1} + (LBound_{i} - UBound_{i-1})/2$

$\text{ } LBound_{i} = UBound_{i-1}$
}

\caption{Algoritmus KEX Discretization}
\label{algo:discretization}
\end{algorithm}

\clearpage

\section{Evaluace}
\label{sec:evaluace}

Pro algoritmy řešící problém klasifikace je vhodné testovat jejich přesnost (úspěšnost klasifikace). Mezi základní způsoby, jak odhadnout přesnost klasifikace patří \textit{křížová validace} \cite[str. 370]{Han}. Při křížové validaci se vstupní dataset rozdělí do dvou částí (\textit{trénovací} a \textit{testovací} data) na základě vstupního parametru (počtu iterací). Např. pro křížovou validaci o 10 iteracích tvoří testovací data 10 \% datasetu a trénovací data 90 \% datasetu. Algoritmus se poté \textit{učí} na trénovacích datech a na základě získaných znalostí klasifikuje data testovací. Výsledná přesnost se pak počítá z tzv. matice záměn, kterou pro problém s 2 cílovými třídami ('+' a '-') ilustruje tabulka \ref{tab:confmatrix}.

\begin{table}[htbp]
\caption{Matice záměn}
\label{tab:confmatrix}
\centering
\begin{tabular}{ccc}
     & \multicolumn{2}{l}{Klasifikace} \\ 
Data & +              & -              \\ \hline
+    & TP             & FP             \\ \hline
-    & FN             & TN            
\end{tabular}
\end{table}

kde:

\begin{itemize}
 \item TP značí počet pozorování správně klasifikovaných jako spadající do třídy '+'
 \item FP značí počet pozorování klasifikovaných do třídy '+' ale patřících do třídy '-'
 \item FN značí počet pozorování klasifikovaných do třídy '-' ale patřících do třídy '+'
 \item TN značí počet pozorování správně klasifikovaných jako spadající do třídy '-'
\end{itemize}

Přesnost klasifikace $ACC$ se následně spočítá jako 
\[
ACC = \frac{TP + TN}{TP + TN + FP + FN}
\]

Pro křížovou validaci o $n$ iteracích se tento proces opakuje $n$-krát a výsledná přesnost je průměrem přesností jednotlivých iterací. Nakonec je vhodné poznamenat, že je při křížové validaci vhodné řádky datové matice před začátkem validace náhodně promíchat. Z tohoto důvodu nemusí být výsledky (přesnosta klasifikace) křížové validace deterministické.

Obdobným způsobem lze matice záměn rozšířit i pro problémy s více cílovými třídami.

Algoritmus Knowledge Explorer provádí tzv. \textit{soft-klasifikaci} (tzn. že při klasifikaci přiřazuje každému pozorování váhu, s jakou patří do dané třídy). Pozorování je klasifikováno do třídy $C$, pokud váha této klasifikace $w_c^{\oplus}$ přesahuje hodnotu $0,5 + \delta$, kde $\delta \in [0; 0,5)$ je uživatelem zvolený parametr (nazvaný \textit{šířka nejistoty}). 

S přihlédnutím k tomuto můžeme vytvořit rozšířenou matici záměn, ilustrovanou tabulkou \ref{tab:expconfmatrix}, kde klasifikace označené za nejisté reprezentuje třída \textit{unc}.

\begin{table}[htb]
\caption{Rozšířená matice záměn}
\label{tab:expconfmatrix}
\centering
\begin{tabular}{cccc}
     & \multicolumn{3}{l}{Klasifikace} \\
Data & +      & -      & $unc$         \\ \hline
+    & TP     & FP     & $unc_{+}$     \\ \hline
-    & FN     & TN     & $unc_{-}$    
\end{tabular}
\end{table}

Pokud není pro žádnou třídu váha klasifikace vyšší než $0,5 + \delta$, je takové pozorování označeno jako nejisté \textit{(uncertain)}. Pokud pro dané pozorování existuje více tříd s váhou klasifikace větší než $0,5 + \delta$ (váhy nejsou pravděpodobnosti, tudíž jejich součet může být vyšší než 1), pozorování se klasifikuje do té třídy, jíž váha je nejvyšší.

Zbývá dodat, že pro klasifikace s vysokou váhou bývá přesnost takto ohodnoceného pozorování vysoká. Zároveň celková přesnost klasifikace obvykle roste s vyšší hodnotou $\delta$, nicméně tím zároveň roste počet pozorování, která jsou označena jako nejistá.
