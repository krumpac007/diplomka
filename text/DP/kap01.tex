%%% Fiktivní kapitola s ukázkami sazby

\chapter{Dobývání znalostí z~databází}
\label{chap:dobyvani}
V~této kapitole bude vysvětlen pojem dobývání znalostí z~databází a pojmy s~ním spojené. Zároveň bude stručně popsána historie, metodiky, které se v~něm používají, a ukázány typické úlohy, které se při něm řeší. V~dalším textu bude používáno zkratky DZD, anglického výrazu \textit{Data Mining}, obojí ve významu dobývání znalostí z~databází.

%\section{Základní pojmy}
Pro vysvětlení dobývání znalostí z~databází neexistuje pouze jedna přesná definice. Encyklopedie Britannica vysvětluje pojem jako „proces nacházení zajímavých a využitelných vzorů a vztahů ve velkém objemu dat“ \cite{britannica}.  Další možnou definicí data miningu může být např.: „proces výběru, prohledávání a modelování ve velkých objemech dat, sloužící k~odhalení dříve neznámých vztahů mezi daty za účelem získání obchodní výhody“ \cite{Liska}. 
Poslední definicí, kterou zde uvedu, je definice z~článku \cite{Fayyad}, která data mining definuje jako „netriviální získávání implicitních, dříve neznámých a potenciálně užitečných informací z~dat“. Tuto definici využívá i publikace \cite{Berka} profesora Berky. Ten mj. uvádí jako hlavní rozdíl mezi data miningem a prostým využitím statistických metod na data fakt, že při data miningu je kladen důraz na předzpracování dat a následnou interpretaci nalezených znalostí. 
Fayyad ve svém článku \cite{Fayyad} popisuje, že s~rostoucí velikostí databází a dat v~nich uložených (podle \cite[str. 5]{Witten} se počet dat uložených v~databázích zdvojnásobuje přibližně každých 20 měsíců) potřeba jejich strojového zpracování a vyhledávání znalostí z~nich roste. Je dobré si uvědomit, že Fayyadův článek je z~roku 1996 a dnes jeho slova platí ještě v~mnohem větší míře. 

\section{Stručná historie}
První náznaky dobývání znalostí z~dat můžeme vidět od 60. let 20. století. Hlavním důvodem je rozvoj výpočetní techniky a zvyšování výpočetní kapacity počítačů spolu s~kapacitou datových úložišť. V~této době byly využívány především regresní analýzy a první metody rozhodovajích stromů \cite{Liska}. Od této doby nejen akademický zájem o~tuto oblast neustále roste.

Za přelomový rok můžeme považovat rok 1989, kdy se na konferenci IJCAI'89, věnované umělé inteligenci, pořádali první workshopy týkající se právě dobývání znalostí z~dat. Postupem času se zájem o~tuto oblast začal projevovat nárůstem počtu konferencí věnovaných tomuto tématu, stejně tak jako rostoucím počtem odborných článků a žurnálů, které se dobývání znalostí věnují.\cite{Berka}

\section{Dva pohledy na dobývání znalostí z~dat}
Na problém dobývání znalostí z~dat existují dva pohledy -- pohled technologický a pohled manažerský. Technologický pohled poskytuje právě například Fayyad \cite{Fayyad} a ilustruje ho obrázek \ref{img:techn_pohled}. 
\begin{figure}[htbp!]\centering
\includegraphics[width=\textwidth]{img/techn_pohled}
\caption{Technologický pohled na dobývání znalostí z~dat. Zdroj: \cite{Fayyad}}
\label{img:techn_pohled}
\end{figure}

Pohled manažerský poskytuje ve své publikaci \cite{Anand} Anand a ilustruje ho obrázek \ref{img:manaz_pohled}.

\begin{figure}[htbp!]\centering
\includegraphics[width=\textwidth]{img/manaz_pohled}
\caption{Manažerský pohled na dobývání znalostí z~dat. Zdroj: \cite{Berka}}
\label{img:manaz_pohled}
\end{figure}

Z~obrázků je vidět, že oba pohledy se částečně překrývají, ale každý z~pohledů se více zaměřuje na jinou část celého procesu. Nyní budou jednotlivé části stručně popsány tak, jak je vysvětluje Anand \cite{Anand}.

\begin{enumerate}
 \item \textbf{Vytvoření týmu řešitelů}
 
    Jedná se o~první fázi řešení problému. Obvykle je potřebné ustanovit tři role: doménového experta, experta na data a experta na data mining. U~rozsáhlejších problémů je možné pro tyto role mít celé týmy \cite{Liska}.
 \item \textbf{Specifikace problému}
 
    V~této části je specifikován problém, který řešíme. Takovým problémem může být například hledání asociačních pravidel, klasifikace nebo hledání nugetů. Specifikace problému je důležitá z~toho důvodu, že pro každý problém jsou následně zvoleny jiné metody řešení. Druhou fází tohoto kroku je specifikace cílového uživatele. Pokud je cílovým uživatelem člověk, výstup musí být v~lidsky čitelném formátu. Pokud je ovšem data miningový úkol součástí většího projektu a jeho výstupy jsou dále strojově zpracovány, je potřeba zvolit adekvátní strojově čitelný formát.
 \item \textbf{Získání dat}
 
    Získání (kvalitních) dat je nedílnou součástí řešení každého data miningového problému. Platí, že z~nekvalitních dat nemohou vzniknout kvalitní znalosti (tzv. \textit{garbage in -- garbage out}). Problém je, že kvalitní data často nemáme k~dispozici. Hlavními kroky této fáze jsou nalezení relevantních atributů, zajišťení dostupnosti dat a zajištění dostatečného množství dat (často jsou zkoumaná data řídká -- obsahují hodně chybějích hodnot v~relevantních atributech). Z~technologického pohledu tato fáze odpovídá fázi Selection.
 \item \textbf{Výběr metod}
 
    V~tomto kroku jsou vybrány metody, kterými se budou data zpracovávat. Mezi ně mohou patřit například klasifikační metody, dobývání asociačních pravidel, genetické algoritmy nebo neuronové sítě. Není nutné zaměřit se pouze na jednu metodu, naopak použití více metod může vézt k~nalezení většího množství zajímavých znalostí.
 \item \textbf{Předzpracování dat}
 
    Předzpracování dat je důležitým krokem, protože některé metody vyžadují specifické vlastnosti dat. Příkladem předzpracování může být odstranění chybějících hodnot nebo kategorizace numerických atributů(vyžadována mj. algoritmem Knowledge explorer popisovaným v~této práci). Dále může být vhodné odstranění odlehlých hodnot (zde je ovšem důležité porozumění datům, odlehlé hodnoty mohou zároveň poskytovat zajímavé znalosti).
    Tato fáze je považována za jednu z~klíčových, protože při nesprávném předzpracováním dat může dojít ke znehodnocení výsledků. \cite{Liska}. Z~technologického pohledu odpovídá fázím Preprocessing a Transformation.
 \item \textbf{Data mining - dobývání znalostí}
 
    V~této fázi dochází k~aplikaci vybraných metod na předzpracovaná data. 
 \item \textbf{Interpretace}
 
    Výsledky analýz bývají často určeny odborníkům z~jiných oblastí. Proto je důležité výsledky předchozí fáze správně interpretovat a upravit do podoby, které daní odborníci porozumí, a to např. ve formě grafů, tabulek, případně odpovědí na otázky specifikovaných ve fázi 2. 
\end{enumerate}

Zodpovědnosti jednotlivých rolí ilustruje obrázek \ref{img:role}.
\begin{figure}[htbp!]\centering
\includegraphics[width=\textwidth]{img/role}
\caption{Zodpovědnosti jednotlivých rolí z~manažerského pohledu. Zdroj: \cite{Anand}}
\label{img:role}
\end{figure}

\section{Typické úlohy dobývání znalostí}
Úlohy dobývání znalostí lze rozdělit do několika kategorií. Takové rozdělení poskytuje například stěžejní text metodiky CRISP-DM \cite{CRISP} --- tj. metodiky pokrývající celý proces řešení data miningových úloh \cite{RauchSimunek} --- a můžeme ho vidět v~seznamu níže. 

\begin{itemize}
 \item \textbf{Popis dat a sumarizace}
 
    Cílem je stručný popis dat, který poskytne uživateli přehled o~jejich struktuře. Někdy se může jednat o~samotný cíl data miningového úkolu, obvykle se ale jedná o~mezistupeň celého procesu. Součástí popisu dat může být například jejich grafická vizualizace a z~ní patrné odlehlé hodnoty. Případně může být výstupem i statistický popis dat (například zjištění, že většina zákazníků firmy je ve středním věku, může mít za následek rozšíření nabídky produktů, které si tato skupina koupí spíše, než skupina lidí mladších).
    
 \item \textbf{Segmentace}
 
    Cílem segmentace je rozdělení dat do skupin sdílejících podobné vlastnosti a charakteristiky. Segmentace může být plně automatická, příp. může být na vstupu využito již nějaké existující znalosti (vycházející například z~popisu dat zmíněného výše). Segmentace je stejně jako popis dat často využita jako mezikrok k~řešení komplexnějšího problému. Cílem tak může být zmenšení objemu vstupních dat. 
    
 \item \textbf{Popis konceptů}
    
    Cílem je najít co nejlepší popis konceptů nebo tříd. Cílem naopak není co nejpřesnější prediktivní model (to je cílem \textit{klasifikace}). Příklad uvedený v~\cite{CRISP} popisuje např. jako popis konceptů rozdělení zákazníků firmy na zákazníky věrné značce (kupují si téměř pouze její produkty) a zákazníky značce nevěrné. Cílem popisu konceptů je poté najít takové vlastnosti obou skupin, aby firma mohla přijmout opatření, aby se ze zákazníku věrných značce nestali zákazníci značčce nevěrní a naopak ze zákazníků nevěrných zákazníci věrní. Více informací poskytuje například \cite[str. 15--16]{Han}.
    
 \item \textbf{Klasifikace}
    
    Cílem klasifikace je ze známých dat (které lze rozdělit do určitých cílových tříd) najít takové vlastnosti, které pomohou u~nových dat, jejichž třídu neznáme, tuto třídu odhadnout. Příkladem mohou být data o~pasažérech lodi Titanic. Cílovými třídami pak jsou hodnoty, zda cestující přežil, nebo ne. Klasifikace by poté ze známých vlastností pasažérů (například pohlaví, třída, kterou cestují atp.) stanovila, zda by cestující při hávárii spíše přežil, nebo nepřežil. Klasifikace tedy narozdíl od predikce očekává rozdělení do konečného množství tříd. Tato oblast je pro nás nejzajímavější, jelikož algoritmus popisovaný v~této práci spadá právě do ní.
    
 \item \textbf{Predikce}
    
    Predikce je v~postatě stejná jako klasifikace, nicméně cílový atribut nemusí být diskrétní, ale může být spojitý. Příkladem může být odhad nějaké veličiny v~čase.    
\end{itemize}

V~\cite{Han} můžeme nalézt rozdělení data miningových úloh na prediktivní a deskriptivní. Ze seznamu výše by mezi úlohy prediktivní patřily klasifikace a predikce, naopak mezi deskriptivní by patřily popis dat, segmentace a popis konceptů.


