package com.rapidminer.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.extension.data.Category;
import com.rapidminer.extension.data.Combination;
import com.rapidminer.extension.data.InputDataTable;
import com.rapidminer.extension.util.Functions;
import com.rapidminer.extension.util.TestClass;

public class OperatorTest {
	private static InputDataTable titanicIdt;
	private static InputDataTable irisIdt;
	private static InputDataTable acreditIdt; //acredit with deleted rows containing miss. values
	
	
	private Category empty, survivedYes, survivedNo, sexMale, sexFemale, portCherbourg, portQueenstown, portSouthampton, portUnknown, classFirst, classSecond, classThird;
	private Combination emptyCombination, maleSurvived, femaleDied, maleDiedFirst, femaleSurvivedSecond, maleThirdCherbourg, femaleFirstSouthampton;
	private Combination maleThird, female, femaleSouthampton, male;
	private AssociationRule emptyRule, emptyRule2, rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10, rule11, rule12, rule13, rule14;
	private double eps = 0.000001;
	@BeforeClass
	public static void initAll() {
		titanicIdt  = new InputDataTable("unittests\\datasets\\titanic.csv", ",", "Survived");
		acreditIdt = new InputDataTable("unittests\\datasets\\acredit_discretized_kex.csv", ",", "label");
		irisIdt = new InputDataTable("unittests\\datasets\\iris_discretized_kex.csv", ",", "label");
		//Functions.discretize(acreditNomissingIdt, 0.05);		
	}

	@Before
	public void init() {
		
		empty = new Category("","", titanicIdt);
		survivedYes = new Category ("Survived", "Yes", titanicIdt);
		survivedNo = new Category ("Survived", "No", titanicIdt);
		sexMale = new Category ("Sex", "Male", titanicIdt);
		sexFemale = new Category ("Sex", "Female", titanicIdt);
		portCherbourg= new Category ("Port_of_Embarkation", "Cherbourg", titanicIdt);
		portQueenstown = new Category ("Port_of_Embarkation", "Queenstown", titanicIdt);
		portSouthampton = new Category ("Port_of_Embarkation", "Southampton", titanicIdt);
		portUnknown = new Category ("Port_of_Embarkation", "?", titanicIdt);
		classFirst = new Category ("Passenger_Class", "First", titanicIdt);
		classSecond = new Category ("Passenger_Class", "Second", titanicIdt);
		classThird = new Category ("Passenger_Class", "Third", titanicIdt);
		
		emptyCombination = new Combination(titanicIdt); emptyCombination.addToCobination(empty);
		maleSurvived = new Combination(titanicIdt); maleSurvived.addToCobination(survivedYes); maleSurvived.addToCobination(sexMale);
		femaleDied = new Combination(); femaleDied.addToCobination(sexFemale); femaleDied.addToCobination(survivedNo);
		maleDiedFirst = new Combination(); maleDiedFirst.addToCobination(sexMale); maleDiedFirst.addToCobination(survivedNo); maleDiedFirst.addToCobination(classFirst);
		femaleSurvivedSecond = new Combination(); femaleSurvivedSecond.addToCobination(sexFemale); femaleSurvivedSecond.addToCobination(survivedYes); femaleSurvivedSecond.addToCobination(classSecond);
		maleThirdCherbourg = new Combination(); maleThirdCherbourg.addToCobination(sexMale);maleThirdCherbourg.addToCobination(classThird); maleThirdCherbourg.addToCobination(portCherbourg);
		femaleFirstSouthampton = new Combination(); femaleFirstSouthampton.addToCobination(sexFemale);femaleFirstSouthampton.addToCobination(classFirst);femaleFirstSouthampton.addToCobination(portSouthampton);
		maleThird = new Combination(); maleThird.addToCobination(sexMale); maleThird.addToCobination(classThird);
		female = new Combination(); female.addToCobination(sexFemale);
		male = new Combination(); male.addToCobination(sexMale);
		femaleSouthampton = new Combination(); femaleSouthampton.addToCobination(sexFemale); femaleSouthampton.addToCobination(portSouthampton);
		
		emptyRule = new AssociationRule(null, survivedNo, survivedNo.getFrequency());
		emptyRule2 = new AssociationRule(null, survivedYes, survivedYes.getFrequency());
		rule1 = new AssociationRule(maleThirdCherbourg, survivedNo);
		rule2 = new AssociationRule(maleThirdCherbourg, survivedYes);
		rule3 = new AssociationRule(femaleFirstSouthampton, survivedNo);
		rule4 = new AssociationRule(femaleFirstSouthampton, survivedYes);
		rule5 = new AssociationRule(maleThird, survivedNo);
		rule6 = new AssociationRule(maleThird, survivedYes);
		rule7 = new AssociationRule(female, survivedNo);
		rule8 = new AssociationRule(femaleSouthampton, survivedNo);
		rule9 = new AssociationRule(new Combination(portSouthampton), survivedYes);
		rule10 = new AssociationRule(new Combination(portSouthampton), survivedNo);
		rule11 = new AssociationRule(male, survivedNo);		
		rule12 = new AssociationRule(new Combination(classThird), survivedNo);
		
		rule13 = new AssociationRule(male, survivedYes);
		rule14 = new AssociationRule(new Combination(classThird), survivedYes);
	}

	@Test
	public void idtCountTest(){
		assertEquals(1309, titanicIdt.countRows());
	}
	
	@Test
	public void categoryEqualsTest(){
		Category c1 = new Category("Sex", "Male", titanicIdt);		
		Category c3 = c1;
		assertEquals(c1, sexMale);
		assertEquals(c3, sexMale);
		assertEquals(c3, c1);		
	}
	
	@Test
	public void categoryCompareTest() {
		Category c1 = new Category("Sex", "Male", titanicIdt);
		assertEquals(0, c1.compareTo(sexMale));
		assertTrue(sexMale.compareTo(sexFemale) > 0); //male > female
		assertTrue(sexFemale.compareTo(sexMale) < 0); //male > female
		assertTrue(sexMale.compareTo(survivedYes) < 0); //sEx < sUrvived
		assertTrue(sexFemale.compareTo(portCherbourg) > 0); //sex > port
	}
	
	@Test
	public void categoryItemsCountTest() {
		assertEquals(0, empty.getItemsCount(), eps);
		assertEquals(843, sexMale.getItemsCount(), eps);
		assertEquals(466, sexFemale.getItemsCount(), eps);
		assertEquals(500, survivedYes.getItemsCount(), eps);
		assertEquals(809, survivedNo.getItemsCount(), eps);		
		assertEquals(277, classSecond.getItemsCount(), eps);
		assertEquals(270, portCherbourg.getItemsCount(), eps);	
		
		//test category without automatic calculation
		Category sexFemaleNotCalculated = new Category ("Sex", "Female", titanicIdt, false);
		assertEquals(-1, sexFemaleNotCalculated.getItemsCount(), eps);	
		sexFemaleNotCalculated.computeItemsCount(titanicIdt);
		assertEquals(466, sexFemaleNotCalculated.getItemsCount(), eps);		
	}
	
	@Test
	public void combinationEqualsTest() {
		List<Category> l = new LinkedList<>();
		l.add(portCherbourg);
		l.add(classThird);
		l.add(sexMale);
		Combination maleCherbourgThird = new Combination(l, titanicIdt);
		maleCherbourgThird.equals(maleCherbourgThird);
		assertEquals(maleCherbourgThird, maleThirdCherbourg);
		
		//add one more category, combinations differ now
		maleCherbourgThird.addToCobination(survivedNo);
		assertNotEquals(maleCherbourgThird, maleThirdCherbourg);
		
		//same lengths, differrent cats
		l.clear();
		l.add(portCherbourg);
		l.add(classFirst);
		l.add(sexMale);
		
		Combination maleCherbourgFirst = new Combination(l, titanicIdt);
		assertNotEquals(maleCherbourgFirst, maleThirdCherbourg);
	}
	
	@Test
	public void combinationItemCountTest() {
		//assertEquals(0, emptyCombination.getItemsCount());		
		assertEquals(161, maleSurvived.getItemsCount(),eps);
		assertEquals(127, femaleDied.getItemsCount(), eps);
		assertEquals(69, femaleFirstSouthampton.getItemsCount(), eps);
		assertEquals(70, maleThirdCherbourg.getItemsCount(), eps);
		assertEquals(118, maleDiedFirst.getItemsCount(), eps);
		
		//test combination without automatic calculation
		Combination maleSurvivedNotCalculated = new Combination(titanicIdt); maleSurvivedNotCalculated.addToCobination(survivedYes,false); maleSurvivedNotCalculated.addToCobination(sexMale, false);
		assertEquals(-1, maleSurvivedNotCalculated.getItemsCount(), eps);	
		maleSurvivedNotCalculated.computeItemsCount(titanicIdt);
		assertEquals(161, maleSurvivedNotCalculated.getItemsCount(), eps);	
		
		
	}

	@Test
	public void combinationFreqTest() {	
		//assertEquals(0, emptyCombination.getFrequency(), 0.0001);
		assertEquals(161d/titanicIdt.countRows(), maleSurvived.getFrequency(), 0.0001);
		assertEquals(127d/titanicIdt.countRows(), femaleDied.getFrequency(),0.0001);
		assertEquals(69d/titanicIdt.countRows(), femaleFirstSouthampton.getFrequency(), 0.0001);
		assertEquals(70d/titanicIdt.countRows(), maleThirdCherbourg.getFrequency(), 0.0001);
	}
	
	@Test
	public void combinationContainsCategoryTest() {
		assertTrue(maleSurvived.containsCategory(sexMale));
		assertTrue(maleSurvived.containsCategory(survivedYes));
		assertFalse(maleSurvived.containsCategory(survivedNo));
		assertFalse(maleSurvived.containsCategory(sexFemale));
		assertFalse(maleSurvived.containsCategory(portCherbourg));
	}
	@Test
	public void combinationContainsAttributeTest() {
		assertTrue(maleSurvived.containsAttribute(sexMale));
		assertTrue(maleSurvived.containsAttribute(survivedYes));
		assertTrue(maleSurvived.containsAttribute(sexFemale));
		assertTrue(maleSurvived.containsAttribute(survivedNo));
		assertFalse(maleSurvived.containsAttribute(portCherbourg));
	}
	
	@Test
	public void combinationCompareTest() {
		assertTrue(maleSurvived.compareTo( femaleDied) > 0);
		assertTrue(maleSurvived.compareTo( femaleSurvivedSecond) > 0);
		assertFalse(maleThirdCherbourg.compareTo( maleDiedFirst)>= 0);
		assertTrue(maleThirdCherbourg.compareTo( maleDiedFirst) < 0);
	}
	@Test
	public void assocRuleCompareTest() {
		//System.out.println(emptyRule.compareTo(rule2));
		assertTrue(rule1.compareTo(rule3) > 0);
		assertFalse(rule4.compareTo(rule2) >= 0);
		assertFalse(emptyRule.compareTo(rule2) >= 0);
		assertEquals(0, rule1.compareTo(rule1));
		
		AssociationRule tmpRule = new AssociationRule(female, survivedNo); //same as rule7
		assertTrue(tmpRule.compareTo(rule7) < 0);		
	}
	@Test
	public void assocRuleEqualsTest() {
		assertNotEquals(rule5, rule6);
		AssociationRule tmpRule = new AssociationRule(maleThird, survivedNo); //same as rule5
		assertEquals(rule5, tmpRule);
		
	}
	@Test
	public void subRuleTest() {
		assertTrue(rule5.isSubRuleOf(rule1));
		assertFalse(rule5.isSubRuleOf(rule2)); 
		assertTrue(rule6.isSubRuleOf(rule2));

		assertTrue(rule7.isSubRuleOf(rule3));
		assertFalse(rule7.isSubRuleOf(rule4));
		assertFalse(rule7.isSubRuleOf(rule1));
		assertFalse(rule7.isSubRuleOf(rule2));
		assertFalse(rule7.isSubRuleOf(rule5));	

		assertTrue(rule8.isSubRuleOf(rule3));
		assertFalse(rule8.isSubRuleOf(rule4));
		assertFalse(rule8.isSubRuleOf(rule1));
		assertFalse(rule8.isSubRuleOf(rule2));
		assertTrue(rule8.isSubRuleOf(rule3));
		
		//rule is subrule of itself
		assertTrue(rule5.isSubRuleOf(rule5));
		
		//emptyrule is subrule of any
		assertTrue(emptyRule.isSubRuleOf(rule1));
		assertTrue(emptyRule.isSubRuleOf(rule3));
		assertTrue(emptyRule.isSubRuleOf(rule5));
		assertTrue(emptyRule.isSubRuleOf(rule7));
		
		//-------------------------------------------------------------------------------------
		//test of containsSubrule - reverse version of isSubRuleOf
		assertTrue(rule1.containsSubRule(rule1));
		assertFalse(rule2.containsSubRule(rule5));
		assertTrue(rule2.containsSubRule(rule6));

		assertTrue(rule3.containsSubRule(rule7));
		assertFalse(rule4.containsSubRule(rule7));
		assertFalse(rule1.containsSubRule(rule7));
		assertFalse(rule2.containsSubRule(rule7));
		assertFalse(rule5.containsSubRule(rule7));	

		assertTrue(rule3.containsSubRule(rule8));
		assertFalse(rule4.containsSubRule(rule8));
		assertFalse(rule1.containsSubRule(rule8));
		assertFalse(rule2.containsSubRule(rule8));
		assertTrue(rule3.containsSubRule(rule8));
		
		//rule is subrule of itself
		assertTrue(rule5.containsSubRule(rule5));
		
		//emptyrule is subrule of any
		assertTrue(rule1.containsSubRule(emptyRule));
		assertTrue(rule3.containsSubRule(emptyRule));
		assertTrue(rule5.containsSubRule(emptyRule));
		assertTrue(rule7.containsSubRule(emptyRule));		
		
	}
	@Test
	public void ruleValidityTest() {
		assertEquals(0.785714286, rule1.getValidity(), 0.0001 );
		assertEquals(0.214285714, rule2.getValidity(), 0.0001 );
		assertEquals(0.043478261, rule3.getValidity(), 0.0001 );
		assertEquals(0.956521739, rule4.getValidity(), 0.0001 );
		assertEquals(0.847870183, rule5.getValidity(), 0.0001 );
		assertEquals(0.152129817, rule6.getValidity(), 0.0001 );
		assertEquals(0.272532189, rule7.getValidity(), 0.0001 );
		assertEquals(0.319587629, rule8.getValidity(), 0.0001 );	
	}
	@Test
	public void combinedWeightTest() {
		List<AssociationRule> l = new LinkedList<>();
		l.add(new AssociationRule(null,null,0.8));
		assertEquals(0.8, Functions.computeCombinedWeight(l), 0.001 );
		l.add(new AssociationRule(null,null,0.6));
		assertEquals(0.857143, Functions.computeCombinedWeight(l), 0.001 );
		l.add(new AssociationRule(null,null,0.3));
		assertEquals(0.72, Functions.computeCombinedWeight(l), 0.001 );
		l.add(new AssociationRule(null,null,0.75));
		assertEquals(0.885246, Functions.computeCombinedWeight(l), 0.001 );		
	}
	@Test 
	public void associationRuleItemCountTest() {		
		assertEquals(809, emptyRule.getItemsCount(), eps);
		assertEquals(55, rule1.getItemsCount(), eps);
		assertEquals(15, rule2.getItemsCount(), eps);
		assertEquals(3, rule3.getItemsCount(), eps);
		assertEquals(66, rule4.getItemsCount(), eps);
		assertEquals(418, rule5.getItemsCount(), eps);
		assertEquals(75, rule6.getItemsCount(), eps);
		assertEquals(127, rule7.getItemsCount(), eps);
		assertEquals(93, rule8.getItemsCount(), eps);			
	}
	@Test
	public void associationRulePremiseLengthTest() {
		assertEquals(0, emptyRule.getPremiseLength());
		assertEquals(3, rule1.getPremiseLength());
		assertEquals(3, rule2.getPremiseLength());
		assertEquals(3, rule3.getPremiseLength());
		assertEquals(3, rule4.getPremiseLength());
		assertEquals(2, rule5.getPremiseLength());
		assertEquals(2, rule6.getPremiseLength());
		assertEquals(1, rule7.getPremiseLength());
		assertEquals(2, rule8.getPremiseLength());	
	}
	@Test
	public void chiSquareTest() {
		List<AssociationRule> knowledgeBase = new LinkedList<AssociationRule>();
		knowledgeBase.add(emptyRule);
		knowledgeBase.add(emptyRule2);
		Set<Category> targetClasses = new HashSet<>();
		targetClasses.add(survivedNo);
		targetClasses.add(survivedYes);
		assertEquals(9.435, Functions.computeChiSquare(rule10, titanicIdt, knowledgeBase, targetClasses), 0.001);
		assertEquals(9.435, Functions.computeChiSquare(rule9, titanicIdt, knowledgeBase, targetClasses), 0.001);

		//test chisq of some rule with moret han 1 subrules already in knowledge base
		List<AssociationRule> subRules = new LinkedList<>();
		List<AssociationRule> subRules2 = new LinkedList<>();
		subRules.add(emptyRule);
		subRules2.add(emptyRule2);
		rule11.setWeight( Functions.computeNewRuleWeight(rule11.getValidity(), Functions.computeCombinedWeight(subRules)));
		rule12.setWeight( Functions.computeNewRuleWeight(rule12.getValidity(), Functions.computeCombinedWeight(subRules)));
		//also the opposite rules (succ is opposite class) must be present in KB
		
		rule13.setWeight( Functions.computeNewRuleWeight(rule13.getValidity(), Functions.computeCombinedWeight(subRules2)));
		rule14.setWeight( Functions.computeNewRuleWeight(rule14.getValidity(), Functions.computeCombinedWeight(subRules2)));
		
		knowledgeBase.add(rule11);
		knowledgeBase.add(rule12);
		knowledgeBase.add(rule13);
		knowledgeBase.add(rule14);
		
		assertEquals(6.363, Functions.computeChiSquare(rule5, titanicIdt, knowledgeBase, targetClasses), 0.001);
		assertEquals(6.363, Functions.computeChiSquare(rule6, titanicIdt, knowledgeBase, targetClasses), 0.001);
		
		knowledgeBase.add(new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, classFirst)), titanicIdt),survivedNo, 0.021748397));
		knowledgeBase.add(new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, classFirst)), titanicIdt),survivedYes, 1 - 0.021748397));
		
		knowledgeBase.add(new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, portCherbourg)), titanicIdt),survivedNo, 0.062487219));
		knowledgeBase.add(new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, portCherbourg)), titanicIdt),survivedYes, 1 - 0.062487219));
		
		assertEquals(19.7715, Functions.computeChiSquare(
				new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, classFirst, portCherbourg)), titanicIdt),survivedNo)
				, titanicIdt, knowledgeBase, targetClasses), 0.001);
		assertEquals(19.7715, Functions.computeChiSquare(
				new AssociationRule(new Combination(new LinkedList<Category>(Arrays.asList(sexFemale, classFirst, portCherbourg)), titanicIdt),survivedYes)
				, titanicIdt, knowledgeBase, targetClasses), 0.001);
	}
	
	@Test
	public void computeNewWeightTest() {
		List<AssociationRule> subRules = new LinkedList<>();
		List<AssociationRule> subRules2 = new LinkedList<>();
		subRules.add(emptyRule);
		subRules2.add(emptyRule2);
		assertEquals(0.553, Functions.computeNewRuleWeight(rule10.getValidity(), Functions.computeCombinedWeight(subRules)), 0.001);
		assertEquals(0.723, Functions.computeNewRuleWeight(rule11.getValidity(), Functions.computeCombinedWeight(subRules)), 0.001);
		assertEquals(0.643, Functions.computeNewRuleWeight(rule12.getValidity(), Functions.computeCombinedWeight(subRules)), 0.001);
		assertEquals(1-0.723, Functions.computeNewRuleWeight(rule13.getValidity(), Functions.computeCombinedWeight(subRules2)), 0.001);
		assertEquals(1-0.643, Functions.computeNewRuleWeight(rule14.getValidity(), Functions.computeCombinedWeight(subRules2)), 0.001);
		
		//newWeight of more than one subrules
		//must set weight of subrules first
		rule11.setWeight( Functions.computeNewRuleWeight(rule11.getValidity(), Functions.computeCombinedWeight(subRules)));
		rule12.setWeight( Functions.computeNewRuleWeight(rule12.getValidity(), Functions.computeCombinedWeight(subRules)));
		subRules.add(rule11);
		subRules.add(rule12);
		
		assertEquals(0.421, Functions.computeNewRuleWeight(rule5.getValidity(), Functions.computeCombinedWeight(subRules)), 0.001);
	
	}
	
	@Test 
	public void ruleIsApplicableTest() {
		Map<String, String> femaleFirstSouthYes = titanicIdt.getData().get(0); 
		Map<String, String> maleThirdCherbNo = titanicIdt.getData().get(678);
		assertTrue(Functions.ruleIsApplicable(rule1,maleThirdCherbNo));
		assertTrue(Functions.ruleIsApplicable(rule2,maleThirdCherbNo)); //succedent doesntmatter
		assertTrue(Functions.ruleIsApplicable(rule5,maleThirdCherbNo));
		assertTrue(Functions.ruleIsApplicable(rule6,maleThirdCherbNo));
		assertFalse(Functions.ruleIsApplicable(rule3,maleThirdCherbNo));
		assertFalse(Functions.ruleIsApplicable(rule7,maleThirdCherbNo));

		assertTrue(Functions.ruleIsApplicable(rule3,femaleFirstSouthYes));
		assertTrue(Functions.ruleIsApplicable(rule7,femaleFirstSouthYes));
		assertFalse(Functions.ruleIsApplicable(rule14,femaleFirstSouthYes));
		
		
	}
	
	@Test
	public void doWorkTest() {
		//results of these tests were compared with KEXTask for correctness
		List<AssociationRule> testedKnowledgeBase;
		List<AssociationRule> checkKnowledgeBase;
		
		int maxLenOfAnt = 2; double minFreqOfAnt = 0.01; double minValidityOfRule = 0.9; double alpha = 0.05;		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, titanicIdt); //6rules
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\titanic_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 3; minFreqOfAnt = 0; minValidityOfRule = 0; alpha = 0.05;		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, titanicIdt);//30 rules
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\titanic_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 5; minFreqOfAnt = 0.05; minValidityOfRule = 0.5; alpha = 0.01;		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, titanicIdt); //26 rules
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\titanic_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 5; minFreqOfAnt = 0.00; minValidityOfRule = 0.8; alpha = 0.05;		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, titanicIdt); //14 rules
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\titanic_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		//--------------------------------------------------------------------------------------------------------------
		//ACREDIT TESTS - BIGGER DATA, MORE ATTRIBUTES
		//for KEXTask already discretized data were provided
				
		//first check if the discretization was done as expected
		/*InputDataTable acreditDiscretizedIdtCheck = null; //deserialize the control idt
		ObjectInputStream in;
		try {
		    FileInputStream file = new FileInputStream("unittests\\acreditDiscretizedDataset.obj"); 
			in = new ObjectInputStream(file);
		    // Method for deserialization of object 
			acreditDiscretizedIdtCheck = (InputDataTable)in.readObject(); 
		    in.close(); 
		    file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		assertEquals(acreditDiscretizedIdtCheck, acreditIdt);*/
		/*
		maxLenOfAnt = 2; minFreqOfAnt = 0.05; minValidityOfRule = 0.9; alpha = 0.05;		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, acreditIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\acredit_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		*/
		maxLenOfAnt = 2; minFreqOfAnt = 0.06; minValidityOfRule = 0.8; alpha = 0.025; //72 rules (lisp 70)		
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, acreditIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\acredit_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);

		maxLenOfAnt = 3; minFreqOfAnt = 0.1; minValidityOfRule = 0.9; alpha = 0.05;	//22rules	
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, acreditIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\acredit_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);

		maxLenOfAnt = 4; minFreqOfAnt = 0.1; minValidityOfRule = 0.9; alpha = 0.05;	//36 rules	
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, acreditIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\acredit_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 3; minFreqOfAnt = 0.07; minValidityOfRule = 0.9; alpha = 0.05; //54 rules
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, acreditIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\acredit_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		//--------------------------------------------------------------------------------------------------------------
		//IRIS TESTS - Multiclass problem
		
		maxLenOfAnt = 2; minFreqOfAnt = 0.01; minValidityOfRule = 0.9; alpha = 0.05;	//48 rules	
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, irisIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\iris_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 5; minFreqOfAnt = 0.01; minValidityOfRule = 0.9; alpha = 0.05;	//63 rules	
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, irisIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\iris_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
		
		maxLenOfAnt = 5; minFreqOfAnt = 0.08; minValidityOfRule = 0; alpha = 0.05;	//51 rules	
		testedKnowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, irisIdt);
		checkKnowledgeBase = Functions.getSerializedKnowledgebase("unittests\\iris_" + maxLenOfAnt + "_" + minFreqOfAnt + "_" + minValidityOfRule + "_" + alpha + ".obj");
		assertEquals(testedKnowledgeBase, checkKnowledgeBase);
	}	
	
	@Test
	public void weightedExampleSetTests() {
		//specialy developed ExampleSet from rapidminer
		//because i dont have functionality for reading weighted dataset from file
		/*		 
		  att1	att2	w	label
		 -------------------------
			a	a		0.1	+
			b	a		0.2	+
			a	a		0.6	-
			b	a		0.7	-
			a	b		0.3	+
			b	b		0.5	+
			a	b		0.8	-
			b	b		0	-
		---------------------------		
				w total	3.2	
		 */
		InputDataTable weightedIdt = new InputDataTable(Functions.getSerializedExampleSet("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\weightedExampleSet.obj"));
		Category a1a = new Category("att1", "a", weightedIdt);
		Category a1b = new Category("att1", "b", weightedIdt);
		Category a2a = new Category("att2", "a", weightedIdt);
		Category a2b = new Category("att2", "b", weightedIdt);
		Category labelPlus = new Category("label", "+", weightedIdt);
		
		assertEquals(a1a.getItemsCount(), 1.8, eps);
		assertEquals(a1a.getFrequency(), 1.8/3.2, eps);
		
		assertEquals(a1b.getItemsCount(), 1.4, eps);
		assertEquals(a1b.getFrequency(), 1.4/3.2, eps);
		
		assertEquals(a2a.getItemsCount(), 1.6, eps);
		assertEquals(a2a.getFrequency(), 1.6/3.2, eps);
		
		assertEquals(a2b.getItemsCount(), 1.6, eps);
		assertEquals(a2b.getFrequency(), 1.6/3.2, eps);
		
		Combination aa = new Combination(weightedIdt); 
		aa.addToCobination(a1a); 
		aa.addToCobination(a2a);
		
		Combination ba = new Combination(weightedIdt); 
		ba.addToCobination(a1b); 
		ba.addToCobination(a2a);
		Combination bb = new Combination(weightedIdt); bb.addToCobination(a1b); bb.addToCobination(a2b);
		
		assertEquals(aa.getItemsCount(), 0.7, eps);
		assertEquals(aa.getFrequency(), 0.7/3.2, eps);
		
		assertEquals(ba.getItemsCount(), 0.9, eps);
		assertEquals(ba.getFrequency(), 0.9/3.2, eps);
		
		assertEquals(bb.getItemsCount(), 0.5, eps);
		assertEquals(bb.getFrequency(), 0.5/3.2, eps);
		
		AssociationRule r = new AssociationRule(ba, labelPlus);
		assertEquals(r.getItemsCount(), 0.2, eps);
		assertEquals(r.getValidity(), 0.2/0.9 , eps);
	}
}
