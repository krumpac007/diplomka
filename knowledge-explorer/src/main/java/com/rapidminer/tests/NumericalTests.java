package com.rapidminer.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.util.Precision;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.extension.data.InputDataTable;
import com.rapidminer.extension.data.ValueClassCount;
import com.rapidminer.extension.util.Functions;

/**
 * @author Vaclav Motyka
 *
 */
public class NumericalTests {
	private static InputDataTable irisIdt;
	private static InputDataTable discretizationIdt;
	private static InputDataTable discretizationEditedIdt;
	private static InputDataTable discretizationChisqIdt;

	@BeforeClass
	public static void initAll() {
		irisIdt = new InputDataTable("unittests\\datasets\\iris_ucm.csv", ",",
				"label");
		//Datasets prepared specially for discretization testing
		discretizationIdt = new InputDataTable(
				"unittests\\datasets\\discretization.csv", ",", "label");
		discretizationEditedIdt = new InputDataTable(
				"unittests\\datasets\\discretization_edited.csv", ",", "label");
		discretizationChisqIdt = new InputDataTable(
				"unittests\\datasets\\discretizationChiSq.csv", ",", "label");
	}

	@Test
	public void compareValueClassCount() {
		ValueClassCount vcc1 = new ValueClassCount("a1", 4.3, irisIdt, 0.05);
		ValueClassCount vcc2 = new ValueClassCount("a1", 4.9, irisIdt, 0.05);
		ValueClassCount vcc3 = new ValueClassCount("a1", 5.8, irisIdt, 0.05);
		assertTrue(vcc1.compareTo(vcc2) < 0);
		assertTrue(vcc2.compareTo(vcc1) > 0);
		assertTrue(vcc3.compareTo(vcc2) > 0);
	}

	private boolean compareDobleArrays(double[] a1, double[] a2, double eps) {
		if(a1.length != a2.length)
			return false;
		for(int i = 0; i < a1.length; i++) {
			if(!Precision.equals(a1[i], a2[i], eps))
				return false;
		}
		return true;
	}
	
	@Test
	public void discretizationTest() {
		//get ranges must be done before discretization since discretization changes the IDT
		double [] ranges = Functions.getDiscretizationRanges(discretizationIdt, "a1", 0.05);
		double [] expectedRanges = {1.99, 3.25, 3.5};
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		ranges = Functions.getDiscretizationRanges(discretizationEditedIdt, "a1", 0.05);
		expectedRanges = new double[]{0.18, 0.325, 0.35};
		assertTrue(compareDobleArrays(ranges, expectedRanges, 0.00000001));
		/*
		Functions.discretize(discretizationIdt, "a1", 0.05);
		List<String> a1 = new LinkedList<String>();
		List<String> a2 = new LinkedList<String>();
		List<String> a3 = new LinkedList<String>();
		List<String> a4 = new LinkedList<String>();
		List<String> a5 = new LinkedList<String>();
		a1.add("<2.0;3.25>");
		a1.add("<2.0;3.25>");
		a1.add("<3.25;3.5>");
		a1.add("<3.25;3.5>");
		a1.add("<3.25;3.5>");
		a1.add("<2.0;3.25>");
		a1.add("<2.0;3.25>");
		a1.add("<2.0;3.25>");
		a1.add("<2.0;3.25>");
		assertEquals(a1, discretizationIdt.getColumn("a1"));*/
        
		
		ranges = Functions.getDiscretizationRanges(discretizationIdt, "a2", 0.05);
		expectedRanges = new double[]{3.99, 5.25, 5.5};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		ranges = Functions.getDiscretizationRanges(discretizationEditedIdt, "a2", 0.05);
		expectedRanges = new double[]{0.0036, 0.00525, 0.0055};                                        
		assertTrue(compareDobleArrays(ranges, expectedRanges, 0.00000001));
		/*
		Functions.discretize(discretizationIdt, "a2", 0.05);
		a2.add("<4.0;5.25>");
		a2.add("<4.0;5.25>");
		a2.add("<4.0;5.25>");
		a2.add("<4.0;5.25>");
		a2.add("<4.0;5.25>");
		a2.add("<4.0;5.25>");
		a2.add("<5.25;5.5>");
		a2.add("<5.25;5.5>");
		a2.add("<5.25;5.5>");
		assertEquals(a2, discretizationIdt.getColumn("a2"));
		 */
		
		ranges = Functions.getDiscretizationRanges(discretizationIdt, "a3", 0.05);
		expectedRanges = new double[]{5.99, 6.25, 7.25, 7.5};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		ranges = Functions.getDiscretizationRanges(discretizationEditedIdt, "a3", 0.05);
		expectedRanges = new double[]{599.99,625,725,750};                                        
		assertTrue(compareDobleArrays(ranges, expectedRanges, 0.00000001));
		/*
		Functions.discretize(discretizationIdt, "a3", 0.05);
		a3.add("<6.0;6.25>");
		a3.add("<6.25;7.25>");
		a3.add("<6.25;7.25>");
		a3.add("<6.25;7.25>");
		a3.add("<6.25;7.25>");
		a3.add("<6.25;7.25>");
		a3.add("<7.25;7.5>");
		a3.add("<7.25;7.5>");
		a3.add("<7.25;7.5>");
		assertEquals(a3, discretizationIdt.getColumn("a3"));
*/
		
		ranges = Functions.getDiscretizationRanges(discretizationIdt, "a4", 0.05);
		expectedRanges = new double[]{7.99, 8.25, 9.25, 9.5};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		ranges = Functions.getDiscretizationRanges(discretizationEditedIdt, "a4", 0.05);
		expectedRanges = new double[]{79.99, 82.5, 92.5, 95.0};                                        
		assertTrue(compareDobleArrays(ranges, expectedRanges, 0.00000001));
	/*	
		Functions.discretize(discretizationIdt, "a4", 0.05);
		a4.add("<8.25;9.25>");
		a4.add("<8.25;9.25>");
		a4.add("<8.25;9.25>");
		a4.add("<8.25;9.25>");
		a4.add("<8.25;9.25>");
		a4.add("<8.0;8.25>");
		a4.add("<8.0;8.25>");
		a4.add("<8.0;8.25>");
		a4.add("<9.25;9.5>");
		assertEquals(a4, discretizationIdt.getColumn("a4"));
*/
		
		ranges = Functions.getDiscretizationRanges(discretizationIdt, "a5", 0.05);
		expectedRanges = new double[]{9.99, 10.75, 11.5};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
	
		ranges = Functions.getDiscretizationRanges(discretizationEditedIdt, "a5", 0.05);
		expectedRanges = new double[]{-11.45, -10.75, -10};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		//assertTrue(Arrays.equals(ranges, expectedRanges));
		
		/*
		Functions.discretize(discretizationIdt, "a5", 0.05);
		a5.add("<10.75;11.5>");
		a5.add("<10.0;10.75>");
		a5.add("<10.75;11.5>");
		a5.add("<10.75;11.5>");
		a5.add("<10.75;11.5>");
		a5.add("<10.0;10.75>");
		a5.add("<10.0;10.75>");
		a5.add("<10.0;10.75>");
		a5.add("<10.0;10.75>");
		assertEquals(a5, discretizationIdt.getColumn("a5"));*/
		
		
	}

/*	@Test
	public void discretizationChisqTest() {
		// Dataset has a1 same as a2. Difference is in chisq, for alpha 0.05 chisq
		// should pass but not for 0.01
		// resulting in different intervals
		
		Functions.discretize(discretizationChisqIdt, "a1", 0.05);
		Functions.discretize(discretizationChisqIdt, "a2", 0.01);
		List<String> a1 = new LinkedList<String>();
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.0;2.25>");
		a1.add("<2.25;2.5>");
		a1.add("<2.25;2.5>");
		a1.add("<2.25;2.5>");
		a1.add("<2.25;2.5>");
		a1.add("<2.25;2.5>");
		a1.add("<2.25;2.5>");

		List<String> a2 = new LinkedList<String>();
		for (int i = 0; i < 13; i++)
			a2.add("<2.0;2.5>");
		assertEquals(a1, discretizationChisqIdt.getColumn("a1"));
		assertEquals(a2, discretizationChisqIdt.getColumn("a2"));

		InputDataTable irisTmp = new InputDataTable(
				"c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\iris_ucm.csv", ",", "label");
		
		double [] ranges = Functions.getDiscretizationRanges(irisTmp, "a1", 0.05);
		double [] expectedRanges = {4.29, 5.45, 6.25,6.55,7.05,7.9};                                        
		assertTrue(Arrays.equals(ranges, expectedRanges));
		
		
		Functions.discretize(irisTmp, "a1", 0.05);
		List<String> a1iris = new LinkedList<String>();
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<4.3;5.45>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<7.05;7.9>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.55;7.05>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<6.25;6.55>");
		a1iris.add("<5.45;6.25>");
		a1iris.add("<5.45;6.25>");

		assertEquals(a1iris, irisTmp.getColumn("a1"));
	}
*/
	@Test
	public void mapWeightToValidityTest() {
		assertEquals(0.05, Functions.mapWeightToValidity(0.05, 2), 0.000001);
		assertEquals(0.0533333333333333, Functions.mapWeightToValidity(0.08, 3), 0.000001);
		assertEquals(0.055, Functions.mapWeightToValidity(0.11, 4), 0.000001);
		assertEquals(0.056, Functions.mapWeightToValidity(0.14, 5), 0.000001);
		assertEquals(0.0566666666666667, Functions.mapWeightToValidity(0.17, 6), 0.000001);
		assertEquals(0.0571428571428571, Functions.mapWeightToValidity(0.2, 7), 0.000001);
		assertEquals(0.0575, Functions.mapWeightToValidity(0.23, 8), 0.000001);
		assertEquals(0.0577777777777778, Functions.mapWeightToValidity(0.26, 9), 0.000001);
		assertEquals(0.058, Functions.mapWeightToValidity(0.29, 10), 0.000001);
		assertEquals(0.0581818181818182, Functions.mapWeightToValidity(0.32, 11), 0.000001);
		assertEquals(0.0583333333333333, Functions.mapWeightToValidity(0.35, 12), 0.000001);
		assertEquals(0.0584615384615385, Functions.mapWeightToValidity(0.38, 13), 0.000001);
		assertEquals(0.41, Functions.mapWeightToValidity(0.41, 2), 0.000001);
		assertEquals(0.293333333333333, Functions.mapWeightToValidity(0.44, 3), 0.000001);
		assertEquals(0.235, Functions.mapWeightToValidity(0.47, 4), 0.000001);
		assertEquals(0.2, Functions.mapWeightToValidity(0.5, 5), 0.000001);
		assertEquals(0.216666666666667, Functions.mapWeightToValidity(0.53, 6), 0.000001);
		assertEquals(0.245714285714286, Functions.mapWeightToValidity(0.56, 7), 0.000001);
		assertEquals(0.2825, Functions.mapWeightToValidity(0.59, 8), 0.000001);
		assertEquals(0.324444444444445, Functions.mapWeightToValidity(0.62, 9), 0.000001);
		assertEquals(0.37, Functions.mapWeightToValidity(0.65, 10), 0.000001);
		assertEquals(0.418181818181818, Functions.mapWeightToValidity(0.68, 11), 0.000001);
		assertEquals(0.468333333333333, Functions.mapWeightToValidity(0.71, 12), 0.000001);
		assertEquals(0.52, Functions.mapWeightToValidity(0.74, 13), 0.000001);
		assertEquals(0.77, Functions.mapWeightToValidity(0.77, 2), 0.000001);
		assertEquals(0.733333333333333, Functions.mapWeightToValidity(0.8, 3), 0.000001);
		assertEquals(0.745, Functions.mapWeightToValidity(0.83, 4), 0.000001);
		assertEquals(0.776, Functions.mapWeightToValidity(0.86, 5), 0.000001);
		assertEquals(0.816666666666667, Functions.mapWeightToValidity(0.89, 6), 0.000001);
		assertEquals(0.862857142857143, Functions.mapWeightToValidity(0.92, 7), 0.000001);
		assertEquals(0.9125, Functions.mapWeightToValidity(0.95, 8), 0.000001);
		assertEquals(1, Functions.mapWeightToValidity(1, 9), 0.000001);
	}

	@Test
	public void mapValidityToWeightTest() {
		assertEquals(0.05, Functions.mapValidityToWeight(0.05, 2), 0.000001);
		assertEquals(0.12, Functions.mapValidityToWeight(0.08, 3), 0.000001);
		assertEquals(0.22, Functions.mapValidityToWeight(0.11, 4), 0.000001);
		assertEquals(0.35, Functions.mapValidityToWeight(0.14, 5), 0.000001);
		assertEquals(0.502, Functions.mapValidityToWeight(0.17, 6), 0.000001);
		assertEquals(0.533333333333333, Functions.mapValidityToWeight(0.2, 7), 0.000001);
		assertEquals(0.56, Functions.mapValidityToWeight(0.23, 8), 0.000001);
		assertEquals(0.58375, Functions.mapValidityToWeight(0.26, 9), 0.000001);
		assertEquals(0.605555555555556, Functions.mapValidityToWeight(0.29, 10), 0.000001);
		assertEquals(0.626, Functions.mapValidityToWeight(0.32, 11), 0.000001);
		assertEquals(0.645454545454546, Functions.mapValidityToWeight(0.35, 12), 0.000001);
		assertEquals(0.664166666666667, Functions.mapValidityToWeight(0.38, 13), 0.000001);
		assertEquals(0.41, Functions.mapValidityToWeight(0.41, 2), 0.000001);
		assertEquals(0.58, Functions.mapValidityToWeight(0.44, 3), 0.000001);
		assertEquals(0.646666666666667, Functions.mapValidityToWeight(0.47, 4), 0.000001);
		assertEquals(0.6875, Functions.mapValidityToWeight(0.5, 5), 0.000001);
		assertEquals(0.718, Functions.mapValidityToWeight(0.53, 6), 0.000001);
		assertEquals(0.743333333333333, Functions.mapValidityToWeight(0.56, 7), 0.000001);
		assertEquals(0.765714285714286, Functions.mapValidityToWeight(0.59, 8), 0.000001);
		assertEquals(0.78625, Functions.mapValidityToWeight(0.62, 9), 0.000001);
		assertEquals(0.805555555555556, Functions.mapValidityToWeight(0.65, 10), 0.000001);
		assertEquals(0.824, Functions.mapValidityToWeight(0.68, 11), 0.000001);
		assertEquals(0.841818181818182, Functions.mapValidityToWeight(0.71, 12), 0.000001);
		assertEquals(0.859166666666667, Functions.mapValidityToWeight(0.74, 13), 0.000001);
		assertEquals(0.77, Functions.mapValidityToWeight(0.77, 2), 0.000001);
		assertEquals(0.85, Functions.mapValidityToWeight(0.8, 3), 0.000001);
		assertEquals(0.886666666666667, Functions.mapValidityToWeight(0.83, 4), 0.000001);
		assertEquals(0.9125, Functions.mapValidityToWeight(0.86, 5), 0.000001);
		assertEquals(0.934, Functions.mapValidityToWeight(0.89, 6), 0.000001);
		assertEquals(0.953333333333333, Functions.mapValidityToWeight(0.92, 7), 0.000001);
		assertEquals(0.971428571428571, Functions.mapValidityToWeight(0.95, 8), 0.000001);
		assertEquals(1, Functions.mapValidityToWeight(1, 9), 0.000001);
	}
	/*
	@Test
	public void tmp() {

	}*/
}
