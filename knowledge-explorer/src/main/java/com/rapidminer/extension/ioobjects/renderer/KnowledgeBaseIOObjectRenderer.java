package com.rapidminer.extension.ioobjects.renderer;

import java.awt.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.extension.data.Category;
import com.rapidminer.extension.data.Combination;
import com.rapidminer.extension.ioobjects.KnowledgeBaseIOObject;
import com.rapidminer.gui.renderer.AbstractRenderer;
import com.rapidminer.gui.renderer.AbstractTableModelTableRenderer;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.report.Reportable;

/**
 * @author Vaclav Motyka
 *
 */
public class KnowledgeBaseIOObjectRenderer extends AbstractTableModelTableRenderer {

	public static final String RENDERER_NAME = "knowledge_base_renderer";

	@Override
	public String getName() {
		return RENDERER_NAME;
	}

	@Override
	public TableModel getTableModel(Object renderable, IOContainer ioContainer, boolean isReporting) {
		if (renderable instanceof KnowledgeBaseIOObject) {
			KnowledgeBaseIOObject object = (KnowledgeBaseIOObject) renderable;
			List<AssociationRule> knowledgeBase = object.getKnowledgeBase();
			return new AbstractTableModel() {
				@Override
				public int getColumnCount() {
					return 4;
				}

				@Override
				public String getColumnName(int column) {
					if (column == 0)
						return "#";					
					if (column == 1)
						return "Premise";
					if (column == 2)
						return "Conclusion";
					return "weight";
				}

				@Override
				public int getRowCount() {
					return knowledgeBase.size();
				}

				@Override
				public Object getValueAt(int rowIndex, int columnIndex) {
					if (columnIndex == 0)
						return rowIndex;
					if (columnIndex == 1)
						return knowledgeBase.get(rowIndex).getPremise();
					if (columnIndex == 2)
						return knowledgeBase.get(rowIndex).getClassification();
					return knowledgeBase.get(rowIndex).getWeight();
				}

			};
		}
		return new DefaultTableModel();
	}
}
