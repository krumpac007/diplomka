package com.rapidminer.extension.ioobjects.renderer;

import java.awt.Component;

import com.rapidminer.gui.renderer.performance.MultiClassificationPerformanceRenderer;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.report.Reportable;

/**
 * @author Vaclav Motyka
 *
 */
public class ValidationResultIOObjectRenderer extends MultiClassificationPerformanceRenderer {

	@Override
	public Reportable createReportable(Object renderable, IOContainer ioContainer, int desiredWidth,
			int desiredHeight) {
		// TODO Auto-generated method stub
		return super.createReportable(renderable, ioContainer, desiredWidth, desiredHeight);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return super.getName();
	}

	@Override
	public Component getVisualizationComponent(Object renderable, IOContainer ioContainer) {
		// TODO Auto-generated method stub
		return super.getVisualizationComponent(renderable, ioContainer);
	}
 
}
