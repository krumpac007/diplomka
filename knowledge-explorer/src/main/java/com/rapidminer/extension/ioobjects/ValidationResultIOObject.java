package com.rapidminer.extension.ioobjects;

import com.rapidminer.extension.data.ValidationResult;
import com.rapidminer.operator.ResultObjectAdapter;

/**
 * @author Vaclav Motyka
 *
 */
public class ValidationResultIOObject extends ResultObjectAdapter {
	private String results;
	
	public ValidationResultIOObject(ValidationResult validationResult) {
		results = validationResult.toString();
	}

	@Override
	public String toString() {
		return results;
	}
	
}
