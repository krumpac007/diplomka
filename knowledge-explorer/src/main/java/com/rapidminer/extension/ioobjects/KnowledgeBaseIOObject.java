package com.rapidminer.extension.ioobjects;

import java.util.List;

import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.operator.ResultObjectAdapter;

/** 
 * @author Vaclav Motyka
 *
 */
public class KnowledgeBaseIOObject extends ResultObjectAdapter{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4472640718973613324L;
	
	private List<AssociationRule> knowledgeBase = null;
	private ValidationResultIOObject validationResult = null;
	public double dblkb = 0;
	
	public KnowledgeBaseIOObject(List<AssociationRule> knowledgeBase) {
		super();
		this.knowledgeBase = knowledgeBase;
	}

		@Override
	public String toString() {
		String res= "";
		
		if(validationResult != null) {					
			res += validationResult.toResultString();
		}	
		res += "-----------------------------------------------------------------";
		res += "Knowledge base: " + knowledgeBase.size() + " rules \n";
		for(AssociationRule rule : knowledgeBase) {
			res += rule + "\n";
		}
		
		return res;
	}

		public List<AssociationRule> getKnowledgeBase() {
			return knowledgeBase;
		}

		public void setKnowledgeBase(List<AssociationRule> knowledgeBase) {
			this.knowledgeBase = knowledgeBase;
		}

		public ValidationResultIOObject getValidationResult() {
			return validationResult;
		}

		public void setValidationResult(ValidationResultIOObject validationResult) {
			this.validationResult = validationResult;
		}
		
	
}
