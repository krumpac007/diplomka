package com.rapidminer.extension.ioobjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.ExampleSetUtilities.SetsCompareOption;
import com.rapidminer.example.set.ExampleSetUtilities.TypesCompareOption;
import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.extension.data.ResultCategory;
import com.rapidminer.extension.util.Functions;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.learner.PredictionModel;
import com.rapidminer.operator.learner.SimplePredictionModel;

/** Model for prediction based on knowledge base
 * @author Vaclav Motyka
 *
 */
public class KnowledgeExplorerModel extends SimplePredictionModel {

	private List<AssociationRule> knowledgeBase;
	
	
	public KnowledgeExplorerModel(ExampleSet exampleSet, SetsCompareOption sizeCompareOperator,
			TypesCompareOption typeCompareOperator, List<AssociationRule> knowledgeBase) {
		super(exampleSet, sizeCompareOperator, typeCompareOperator);
		// TODO Auto-generated constructor stub
		this.knowledgeBase = knowledgeBase;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6353196421982348135L;

	@Override
	public double predict(Example example) throws OperatorException {
		Map<String,String> ex = new HashMap<>();	
		for(Attribute att : example.getAttributes()) {
			ex.put(att.getName(), example.getValueAsString(att));
		}
		ResultCategory rc = Functions.classify(ex, knowledgeBase, 0.0);
		return getLabel().getMapping().getIndex(rc.getValue());
	}

	@Override
	public String toString() {
		return "KnowledgeExplorerModel [knowledgeBase=" + knowledgeBase + "]";
	}
	
	
}
