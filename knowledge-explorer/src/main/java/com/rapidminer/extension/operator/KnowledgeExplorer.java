package com.rapidminer.extension.operator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.logging.Level;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.jdesktop.swingx.auth.LoginService;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.Tools;
import com.rapidminer.example.table.BinominalAttribute;
import com.rapidminer.example.table.NominalAttribute;
import com.rapidminer.example.table.NominalMapping;
import com.rapidminer.extension.data.*;
import com.rapidminer.extension.ioobjects.KnowledgeBaseIOObject;
import com.rapidminer.extension.ioobjects.KnowledgeExplorerModel;
import com.rapidminer.extension.ioobjects.ValidationResultIOObject;
import com.rapidminer.extension.util.Functions;
import com.rapidminer.extension.util.TestClass;
import com.rapidminer.operator.IOObject;
import com.rapidminer.operator.Model;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorCapability;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ValueDouble;
import com.rapidminer.operator.ProcessSetupError.Severity;
import com.rapidminer.operator.learner.AbstractLearner;
import com.rapidminer.operator.learner.associations.AssociationRules;
import com.rapidminer.operator.learner.associations.BooleanAttributeItem;
import com.rapidminer.operator.learner.associations.FrequentItemSet;
import com.rapidminer.operator.learner.associations.Item;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.ExampleSetMetaData;
import com.rapidminer.operator.ports.metadata.ExampleSetPrecondition;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.operator.ports.metadata.SimpleMetaDataError;
import com.rapidminer.operator.ports.metadata.SimplePrecondition;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.Ontology;
import com.rapidminer.RapidMiner;


/** Operator implementing Knowledge explorer algorithm
 * @author Vaclav Motyka
 *
 */
public class KnowledgeExplorer extends AbstractLearner {
	//private InputPort exampleSetInput = getInputPorts().createPort("Input dataset");
	private OutputPort rulesOutput = getOutputPorts().createPort("rules");
	
	//PARAMETERS
	public static final String PARAMETER_MAX_LEN_OF_ANT = "Ant max. length";
	public static final String PARAMETER_MIN_FREQ_OF_ANT = "Ant min. freq";
	public static final String PARAMETER_MIN_VALIDITY_OF_RULE = "Rule min. validity";
	public static final String PARAMETER_ALPHA = "Alpha";
	public static final String PARAMETER_PERFORM_VALIDATION = "Perform X-validation";
	public static final String PARAMETER_VALIDATION_FOLDS = "X-validation folds";
	public static final String PARAMETER_UNCERTAINTY_BANDWIDTH = "Uncertainty bandwidth";
	private double validationAccuracy = 0;
	private int validationFolds = 0;
	private double validationPercentOfCertain = 0;

	private String labelName;
	private Set<Category> classes;
	private List<AssociationRule> knowledgeBase = null;
	
	public KnowledgeExplorer(OperatorDescription description) {
		super(description);
		classes = new HashSet<Category>();
		
		//getExampleSetInputPort().addPrecondition(new ExampleSetPrecondition(getExampleSetInputPort(), Ontology.NOMINAL, Attributes.LABEL_NAME));
		//register values that could be logged in rapidminer
		addValue(new ValueDouble("validation_accuracy","Accuracy of performed X-validation") {
			
			@Override
			public double getDoubleValue() {
				return validationAccuracy;
			}
		});
		
		addValue(new ValueDouble("validation_folds","Cross validation folds") {
			
			@Override
			public double getDoubleValue() {
				return validationFolds;
			}
		});

		addValue(new ValueDouble("validation_percent_of_certain","% of not uncertain results") {
			
			@Override
			public double getDoubleValue() {
				return validationPercentOfCertain;
			}
		});
		
	}

	
	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();
		types.add(new ParameterTypeInt(PARAMETER_MAX_LEN_OF_ANT, "Maximum length of antecedent", 1, Integer.MAX_VALUE, 2, false));
		types.add(new ParameterTypeDouble(PARAMETER_MIN_FREQ_OF_ANT, "Minimal frequency of antecedent", 0.0, 1.0, 0.01, false));
		types.add(new ParameterTypeDouble(PARAMETER_MIN_VALIDITY_OF_RULE, "Minimal validity of rule", 0.0, 1.0, 0.85, false));
		types.add(new ParameterTypeDouble(PARAMETER_ALPHA, "Alpha value for chi-square test.", 0.0, 1.0, 0.05, true));
		types.add(new ParameterTypeBoolean(PARAMETER_PERFORM_VALIDATION, "Perform cross validation of found rules.", false));
		
		//parameter folds show only if perform_validation is true
		ParameterType type = new ParameterTypeInt(PARAMETER_VALIDATION_FOLDS, "Number of folds in X-validation.", 2, 30, 10, false);
		type.registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_PERFORM_VALIDATION, true, true));
		types.add(type);
		
		type = new ParameterTypeDouble(PARAMETER_UNCERTAINTY_BANDWIDTH, "Uncertainty bandwidth for X-validation. For each example combined weight is "
				+ "computed from all the rules in knowledge base, that correspond to this example. If the combined weight "
				+ "is less than 0.5 + bandwidth, expected class of this example is marked as uncertain.", 0.0, 0.5, 0.025, false);
		type.registerDependencyCondition(new BooleanParameterCondition(this, PARAMETER_PERFORM_VALIDATION, true, true));
		types.add(type);
		return types;
	}

	@Override
	public Model learn(ExampleSet exampleSet) throws OperatorException  {		
		//Functions.serializeExampleSet(exampleSet, "c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\weightedExampleSet.obj");
	
		//get parameters
		int maxLenOfAnt = getParameterAsInt(PARAMETER_MAX_LEN_OF_ANT);
		double minFreqOfAnt = getParameterAsDouble(PARAMETER_MIN_FREQ_OF_ANT);
		double minValidityOfRule = getParameterAsDouble(PARAMETER_MIN_VALIDITY_OF_RULE);
		double alpha = getParameterAsDouble(PARAMETER_ALPHA);
		boolean performValidation = getParameterAsBoolean(PARAMETER_PERFORM_VALIDATION);
		int folds = 0;
		double uncertaintyBandwidth = 0.0;
		if(performValidation) {
			folds = getParameterAsInt(PARAMETER_VALIDATION_FOLDS);
			validationFolds = folds;
			uncertaintyBandwidth = getParameterAsDouble(PARAMETER_UNCERTAINTY_BANDWIDTH);
		}
		
		//LogService.getRoot().log(Level.INFO, "Creating inputdatable");
		InputDataTable inputDataTable = new InputDataTable(exampleSet);
		
		//for testing purposes all the logic is implemented in another class (that can be ran without rapidminer)
		LogService.getRoot().log(Level.INFO, "Looking for knowledge base");
		List<AssociationRule> knowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt,minFreqOfAnt, minValidityOfRule, alpha, inputDataTable);
		KnowledgeBaseIOObject kbioo = new KnowledgeBaseIOObject(knowledgeBase);
		this.knowledgeBase = knowledgeBase;
		
		if(performValidation) {
			LogService.getRoot().log(Level.INFO, "Performing validation");
			ValidationResult validationResult = Functions.crossValidation(folds, maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable, uncertaintyBandwidth);
			ValidationResultIOObject validationResultIOO = new ValidationResultIOObject(validationResult);
			validationAccuracy = validationResult.getAccuracy();
			validationPercentOfCertain = validationResult.getPercentOfCertain();
			kbioo.setValidationResult(validationResultIOO);
		}
		
		rulesOutput.deliver(kbioo);	
	
		KnowledgeExplorerModel model = new KnowledgeExplorerModel(exampleSet, null, null, knowledgeBase);
		return model;	
	}

	@Override
	public boolean supportsCapability(OperatorCapability capability) {
		if (capability == com.rapidminer.operator.OperatorCapability.BINOMINAL_ATTRIBUTES) {
			return true;
		}
		if (capability == com.rapidminer.operator.OperatorCapability.POLYNOMINAL_ATTRIBUTES) {
			return true;
		}
		if (capability == com.rapidminer.operator.OperatorCapability.POLYNOMINAL_LABEL) {
			return true;
		}
		if (capability == com.rapidminer.operator.OperatorCapability.BINOMINAL_LABEL) {
			return true;
		}
		if (capability == com.rapidminer.operator.OperatorCapability.WEIGHTED_EXAMPLES) {
			return true;
		}
		return false;
	}

}

