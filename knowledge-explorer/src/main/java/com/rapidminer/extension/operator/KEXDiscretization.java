package com.rapidminer.extension.operator;

import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

import org.apache.commons.lang.ArrayUtils;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.Statistics;
import com.rapidminer.example.Tools;
import com.rapidminer.extension.data.InputDataTable;
import com.rapidminer.extension.util.Functions;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.OperatorVersion;
import com.rapidminer.operator.UserError;
import com.rapidminer.operator.annotation.ResourceConsumptionEstimator;
import com.rapidminer.operator.ports.metadata.AttributeMetaData;
import com.rapidminer.operator.ports.metadata.ExampleSetMetaData;
import com.rapidminer.operator.ports.metadata.SetRelation;
import com.rapidminer.operator.preprocessing.PreprocessingModel;
import com.rapidminer.operator.preprocessing.discretization.AbstractDiscretizationOperator;
import com.rapidminer.operator.preprocessing.discretization.DiscretizationModel;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;
import com.rapidminer.parameter.conditions.EqualTypeCondition;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.Ontology;
import com.rapidminer.tools.OperatorResourceConsumptionHandler;

/** Operator for numerical attribute discretization based on algorithm described in knowledge explorer paper.
 * @author Vaclav Motyka
 *
 */
public class KEXDiscretization extends AbstractDiscretizationOperator {
	private String labelStr = "";
	private ExampleSet fullExampleSet = null; // example set including special attributes like label (see reason in
												// constructor)

	public KEXDiscretization(OperatorDescription description) {
		super(description);

		// Parent class of this operator removes the special attributes like label from
		// exampleset, and I need it for
		// finding intervals. I cannot find it in exampleset in createPreprocessingModel
		// method since it is removed there already
		/*try {
			fullExampleSet = super.getExampleSetInputPort().getData(ExampleSet.class);
			LogService.getRoot().log(Level.INFO, "fes:" + fullExampleSet);
			labelStr = fullExampleSet.getAttributes().getLabel().getName();
			LogService.getRoot().log(Level.INFO, "label:" + labelStr);
		} catch (UserError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

	/**
	 * Incompatible version, old version writes into the exampleset, if original
	 * output port is not connected.
	 */
	//private static final OperatorVersion VERSION_MAY_WRITE_INTO_DATA = new OperatorVersion(7, 1, 1);

	public static final String PARAMETER_ALPHA = "alpha";

	@Override
	protected Collection<AttributeMetaData> modifyAttributeMetaData(ExampleSetMetaData emd, AttributeMetaData amd)
			throws UndefinedParameterError {
		AttributeMetaData newAMD = new AttributeMetaData(amd.getName(), Ontology.NOMINAL, amd.getRole());
		Set<String> valueSet = new TreeSet<String>();

		newAMD.setValueSet(valueSet, SetRelation.SUPERSET);
		return Collections.singletonList(newAMD);
	}

	@Override
	public PreprocessingModel createPreprocessingModel(ExampleSet exampleSet) throws OperatorException {
			DiscretizationModel model = new DiscretizationModel(exampleSet);
			// Functions.serializeExampleSet(super.getExampleSetInputPort().getData(ExampleSet.class),
			//		 "c:\\Users\\Administrator\\Desktop\\diplomka\\weightedExampleSet.obj");
			double alpha = getParameterAsDouble(PARAMETER_ALPHA);

			fullExampleSet = super.getExampleSetInputPort().getData(ExampleSet.class);
			//LogService.getRoot().log(Level.INFO, "fes:" + fullExampleSet);

			//input cannot have missing values
			Tools.onlyNonMissingValues(fullExampleSet, getOperatorClassName(), this);
			
			labelStr = fullExampleSet.getAttributes().getLabel().getName();
			LogService.getRoot().log(Level.INFO, "label:" + labelStr);

			// exampleSet.recalculateAllAttributeStatistics();
			// ranges are boundary values for each interval
			HashMap<Attribute, double[]> ranges = new HashMap<Attribute, double[]>();
			InputDataTable idt = new InputDataTable(fullExampleSet); // I have to use label string found later, bcs this
																		// exampleSet doesnt contain it in this time

			for (Attribute attribute : exampleSet.getAttributes()) {
				if (attribute.isNumerical()) { // skip nominal and date attributes
					//LogService.getRoot().log(Level.INFO, attribute.getName());
					double[] binRange = Functions.getDiscretizationRanges(idt, attribute.getName(), alpha);
					ranges.put(attribute, binRange);
					/*for (int j = 0; j < binRange.length; j++) {
						LogService.getRoot().log(Level.INFO, "" + binRange[j]);
					}*/

				}
			}
			model.setRanges(ranges, "", DiscretizationModel.RANGE_NAME_INTERVAL, 2);
			return (model);
	}

	@Override
	public Class<? extends PreprocessingModel> getPreprocessingModelClass() {
		return DiscretizationModel.class;
	}

	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = super.getParameterTypes();
		ParameterType type = new ParameterTypeDouble(PARAMETER_ALPHA, "Alpha value for chi-square test.", 0.0, 1.0,
				0.05);
		// type.registerDependencyCondition(new BooleanParameterCondition(this,
		// PARAMETER_DEFINE_BOUNDARIES, true, true));
		types.add(type);

		return types;
	}
/*
	@Override
	public boolean writesIntoExistingData() {
		if (getCompatibilityLevel().isAbove(VERSION_MAY_WRITE_INTO_DATA)) {
			return false;
		} else {
			// old version: true only if original output port is connected
			return isOriginalOutputConnected() && super.writesIntoExistingData();
		}
	}

	@Override
	public OperatorVersion[] getIncompatibleVersionChanges() {
		return (OperatorVersion[]) ArrayUtils.addAll(super.getIncompatibleVersionChanges(),
				new OperatorVersion[] { VERSION_MAY_WRITE_INTO_DATA });
	}
	*/
}
