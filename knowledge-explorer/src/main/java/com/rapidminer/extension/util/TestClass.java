package com.rapidminer.extension.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.extension.data.Category;
import com.rapidminer.extension.data.InputDataTable;
import com.rapidminer.extension.data.ValidationResult;
import com.rapidminer.extension.ioobjects.KnowledgeExplorerModel;
import com.rapidminer.extension.operator.KnowledgeExplorer;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.tools.Ontology;


/** Class impleemnted purely for testing without need to start rapidminer. 
 * @author Vaclav Motyka
 *
 */
public class TestClass {
	// PARAMETERS
	/*public static final String PARAMETER_MAX_LEN_OF_ANT = "ANT max length";
	public static final String PARAMETER_MIN_FREQ_OF_ANT = "ANT min freq";
	public static final String PARAMETER_MIN_VALIDITY_OF_RULE = "RULE min validity";
	//private static String labelName = "class";
	*/
	//static Set<Category> classes;
	/*private static InputDataTable monks3Idt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\monks 3 labeled.csv", "\t", "class");
	private static InputDataTable discretizationIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\discretization.csv", ",", "label");
	private static InputDataTable irisUcmIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\iris_ucm.csv", ",", "label");
	private static InputDataTable titanicIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\titanic.csv", ",", "Survived");
	//acredit with rows containing missing values deleted
	private static InputDataTable acreditNomissingIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\acredit\\acredit_missing_rows_deleted.csv", ",", "label");
	private static InputDataTable acreditDiscretizedIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\acredit_discretized_kex.csv", ",", "label");
	private static InputDataTable irisDiscretizedIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\iris_discretized_kex.csv", ",", "label");
	//private static InputDataTable carIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\car.csv", ",", "class");
	//private static InputDataTable zooIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\ZOO\\zoo.csv", ",", "type");
	private static InputDataTable mushroomIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\test_datasets\\mushroom.csv", ",", "type");
	*/
	//static double dbl_epsilon = 0.000000001;
	
	public static void main(String[] args) {
		// get parameters
		InputDataTable abaloneIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\test_datasets\\abalone.csv", ",", "Label");
		InputDataTable acreditDiscretizedIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\acredit_discretized_kex.csv", ",", "label");
		InputDataTable mushroomIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\test_datasets\\mushroom.csv", ",", "type");
		InputDataTable carIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\car.csv", ",", "class");
		InputDataTable zooIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\ZOO\\zoo.csv", ",", "type");
		InputDataTable titanicAgeIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\test_datasets\\titanic_with_age.csv", ",", "Survived");
		InputDataTable titanicIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\titanic.csv", ",", "Survived");
		InputDataTable irisDiscretizedIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\unittests\\datasets\\iris_discretized_kex.csv", ",", "label");
		InputDataTable irisBerkaIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\berka\\iris_berka.txt", "\t", "label");
		InputDataTable len2BerkaIdt = new InputDataTable("c:\\Users\\Administrator\\Desktop\\diplomka\\datasets\\berka\\len2_berka.txt", "\t", "label");
		ExampleSet weightedExampleSet = Functions.getSerializedExampleSet("c:\\Users\\Administrator\\Desktop\\diplomka\\knowledge-explorer\\weightedExampleSet.obj");
		InputDataTable weightedIdt = new InputDataTable(weightedExampleSet);
		
		int maxLenOfAnt = 2;
		double minFreqOfAnt = 0.01;
		double minValidityOfRule = 0.9;
		double alpha = 0.05;
		double uncertaintyBandwidth = 0.025;
		
		InputDataTable inputDataTable = weightedIdt;
		List<AssociationRule> knowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable);
	
	
		
		
		
		//List<AssociationRule> knowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable);
		//Functions.serializeKnowledgebase(Functions.knowledgeExplorer(maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable), filename);
		//ValidationResult res = Functions.crossValidation(10, maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable, uncertaintyBandwidth);
		//System.out.println(maxLenOfAnt + "," + res.getAccuracy());
		
		
		List<Double> times = new LinkedList<>();
		
	/*for(Map<String, String> row : inputDataTable.getData()) {
			ResultCategory rowInferredClass = Functions.classify(row, knowledgeBase, 0.025);
			System.out.println(rowAsString(row) + rowInferredClass);
		}*/
		
		
		//Functions.crossValidation(3, maxLenOfAnt, minFreqOfAnt, minValidityOfRule, alpha, inputDataTable, uncertaintyBandwidth);
		
	}
	
	private static String rowAsString(Map<String, String> row) {
		String res="";
		for(String key : row.keySet()) {
			//res += key + ":" + row.get(key) + " ";
			res += row.get(key) + " ";
		}
		return res;
	}
}
