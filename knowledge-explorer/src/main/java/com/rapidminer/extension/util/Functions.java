package com.rapidminer.extension.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.util.Precision;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.extension.data.AssociationRule;
import com.rapidminer.extension.data.Category;
import com.rapidminer.extension.data.Combination;
import com.rapidminer.extension.data.InputDataTable;
import com.rapidminer.extension.data.Interval;
import com.rapidminer.extension.data.ResultCategory;
import com.rapidminer.extension.data.ValidationResult;
import com.rapidminer.extension.data.ValueClassCount;
import com.rapidminer.tools.LogService;

public class Functions {
	/**
	 * @param rules
	 * @return combined weight of rules
	 */
	public static double computeCombinedWeight(List<AssociationRule> rules) {
		//computeCombinedWeightFromWeights was implemented later, so instedd having same code twice i just call it

		List<Double> weights = new LinkedList<>();
		for(AssociationRule rule : rules) {
			weights.add(rule.getWeight());
		}
		return computeCombinedWeightFromWeights(weights);
	}
	
	/**
	 * @param weights
	 * @return combined weight from weights
	 */
	public static double computeCombinedWeightFromWeights(List<Double> weights) {
		if(weights.size() == 0)
			return 0;
		double weight = weights.get(0);
		for(int i = 1; i < weights.size(); i++) {
			double currWeight = weight;
			double nextWeight = weights.get(i);
			weight = (currWeight * nextWeight) / ((currWeight * nextWeight) + (1-currWeight)*(1-nextWeight));
		}
		return weight;
	}
	

	/**
	 * @param rule
	 * @param idt
	 * @param knowledgeBase
	 * @param targetClasses
	 * @return Computes chi square of rule based on target classes and currently know knowledge base
	 */
	public static double computeChiSquare(AssociationRule rule, InputDataTable idt, List<AssociationRule> knowledgeBase, Set<Category> targetClasses) {
		//I found in the end, that values from not(Ant) should not be used in this calculation
		//If we want to find the "real" chi square of the rule, it would be used, but cummulative weight couldnt be used then - normal formula would have to be used
		
		double chisq = 0;
		int degreesOfFreedom = targetClasses.size() - 1;
		ChiSquaredDistribution chiSquaredDistribution = new ChiSquaredDistribution(degreesOfFreedom);
		for (Category tClass : targetClasses) {
			AssociationRule antCRule = new AssociationRule(rule.getPremise(), tClass);
			
			//get counts of contingency table
			//antCCount - count of items which have values of antecedant and value of current target class
			double antCCount = antCRule.getItemsCount();
		
			//antCount - count of items of antecedent, class doesnt matter
			double antCount = antCRule.getPremise().getItemsCount();
			
			List<AssociationRule> subRulesOfCurrentClass = new LinkedList<>();
			for (AssociationRule kbRule : knowledgeBase) {
				//dont add subrule if the whole subrule is in KB
				//reason is that first there is rule ant => C(1) tested first and if passes it is added to KB
				//then there is tested rule ant => C(2).. but chisq is sum of chisq(ant=>C1)+chisq(ant=>C2) - but
				//chisq(ant=>C1) computes wrong value if ant=>C1 is already in KB (because of wrong cummulative weight)
				if(kbRule.equals(antCRule)){
					continue;
				}
				if (kbRule.isSubRuleOf(antCRule)) {
					//LogService.getRoot().log(Level.INFO, "[2]Adding subrule " + kbRule.toString());
					//System.out.println("Adding subrule " + kbRule.toString());
					subRulesOfCurrentClass.add(kbRule);
				}
			}
			double combinedWeightOfRuleImpliingCurrentClass = Functions
					.computeCombinedWeight(subRulesOfCurrentClass);
			//System.out.println("combined weight " + combinedWeightOfRuleImpliingCurrentClass);
			
			double antCExp = antCount * mapWeightToValidity(combinedWeightOfRuleImpliingCurrentClass, idt.classesCount()); 
			
			if(antCExp != 0)
				chisq += ((antCCount - antCExp) * (antCCount - antCExp)) / (antCExp);

			//double chisqFromTable = chiSquaredDistribution.inverseCumulativeProbability(1 - alpha);
		}
		
		return chisq;
	}
	
	/**
	 * @param validity
	 * @param subrulesCombinedWeight
	 * @return weight of new rule based on validity and comibined weight of subrules
	 */
	public static double computeNewRuleWeight(double validity, double subrulesCombinedWeight) {
		if(validity == 1) return 0.99;
		if(validity == 0) return 0.01;
		
		double u = ((validity)/(1-validity))/((subrulesCombinedWeight)/(1-subrulesCombinedWeight));
		return u / (1+u);
	}
	
	/**
	 * @param validity
	 * @param classesCount
	 * @return Weight mapped to validity based on number of target classes
	 */
	public static double mapValidityToWeight(double validity, int classesCount) {
		if(validity < 1.0/classesCount) {
			return ((double)classesCount / 2) * validity; 
		}else {
			return (((double)classesCount / (2*(classesCount-1)))*validity) + (1-((double)classesCount/(2*(classesCount-1))));
		}
	}
	
	/**
	 * @param weight
	 * @param classesCount
	 * @return Validity mapped to weight based on number of target classes
	 */
	public static double mapWeightToValidity(double weight, int classesCount) {
		if(weight < 0.5) {
			return (2.0 / (double)classesCount ) * weight;
		}else {
			return ((2*(classesCount-1)/((double)classesCount)*weight) + (1-((2*(classesCount-1))/(double)classesCount)));
		}
	}
	
	/* discretization will be done by rapidminer automatically - no need for these methods
	 * 
	public static void discretize(InputDataTable idt, double alpha) {
		for(String attribute : idt.getAttributeTypes().keySet()) {
			if(idt.getAttributeTypes().get(attribute) == Ontology.REAL){
				discretize(idt, attribute, alpha);
				idt.setAttributeType(attribute, Ontology.POLYNOMINAL); //update attribute value
			}
		}
	}
	
	public static void discretize(InputDataTable idt, String attribute, double alpha) {
		SortedSet<Double> sortedValuesSet = new TreeSet<>();
		SortedSet<ValueClassCount> sortedValueClassSet = new TreeSet<>();
		//get all values of attribute, sorted
		for(Map<String, String> row : idt.getData()) {
			double value = Double.parseDouble(row.get(attribute));
			sortedValuesSet.add(value);
		}
		//Step 2 in main loop
		for(Double d : sortedValuesSet) {
			ValueClassCount valueClassCount = new ValueClassCount(attribute, d, idt, alpha);	
			sortedValueClassSet.add(valueClassCount);			
		}
		
		ValueClassCount[] sortedValueClassArray = sortedValueClassSet.toArray(new ValueClassCount[sortedValueClassSet.size()]);
		
		//Interval METHOD
		List<Interval> intervals = getIntervals(sortedValueClassArray);
		
		//change values in datatable to intervals
		for(Map<String, String> row : idt.getData()) {
			String attributeValue = row.get(attribute);
			for(Interval i : intervals) {
				if(i.inInterval(Double.parseDouble(attributeValue))) {
					row.put(attribute, i.toString());
				}
			}
		}
	}*/
	
	
	/**
	 * @param idt
	 * @param attribute
	 * @param alpha value used for chi square test
	 * @return border values of intervals for attribute discretization. 
	 */
	public static double[] getDiscretizationRanges(InputDataTable idt, String attribute, double alpha) {
		SortedSet<Double> sortedValuesSet = new TreeSet<>();
		SortedSet<ValueClassCount> sortedValueClassSet = new TreeSet<>();
		//get all values of attribute, sorted
		for(Map<String, String> row : idt.getData()) {
			double value = Double.parseDouble(row.get(attribute));
			sortedValuesSet.add(value);
		}
		//Step 2 in main loop
		for(Double d : sortedValuesSet) {
			ValueClassCount valueClassCount = new ValueClassCount(attribute, d, idt, alpha);	
			sortedValueClassSet.add(valueClassCount);			
		}
		
		ValueClassCount[] sortedValueClassArray = sortedValueClassSet.toArray(new ValueClassCount[sortedValueClassSet.size()]);
		
		//Interval METHOD
		List<Interval> intervals = getIntervals(sortedValueClassArray);
		double[] ranges = new double [intervals.size() + 1]; //intervals boundaries
		// I want the first boundary to be some value lower than minimum value in the dataset
		// the reason is that rapidminer implicitly create lowest interval from -infinty
		// so if the lowest value in dataset is e.g. 0.5 and first interval should be 0.5-1.0, then
		// rapidminer will classify 0.5 to interval -inf - 0.5, not 0.5 - 1.0
		//ranges[0] = intervals.get(0).getLowerBound() - 1; 
		
		//So i have to find some small number to subtract from lower bound of first interval
		//number to subtract will be the decimal part of the number/2
		//this is done in the i == 0 part
			
		for(int i = 0; i < intervals.size(); i++) {
			if(i == 0) {
				double numberToSubtract;
				double decimalPart = intervals.get(i).getLowerBound() - (int)intervals.get(i).getLowerBound();
				if(Math.abs(decimalPart) < 0.000000000001) //whole number
					numberToSubtract = 0.01;
				else 
					numberToSubtract = decimalPart / 10.0;
				
				ranges[i] = intervals.get(i).getLowerBound() - numberToSubtract;
			}else {
				ranges[i] = intervals.get(i).getLowerBound();
				
				//last item of boundaries will be last intervals upper bound
				if(i == intervals.size()-1)
					ranges[i+1] = intervals.get(i).getUpperBound();
			}
		}
		return ranges;
	}
	
	/**
	 * @param example
	 * @param knowledgeBase
	 * @param uncBandwith
	 * @return Expected class of example based on knowledge base
	 */
	public static ResultCategory classify(Map<String, String> example, List<AssociationRule> knowledgeBase, double uncBandwith) {
		Map<Category, List<Double>> classWeights = new HashMap<>();
		for(AssociationRule rule : knowledgeBase) {
			if(ruleIsApplicable(rule, example)) {
				if(!classWeights.containsKey(rule.getClassification())) { //class not present yet, add it
					List<Double> weightsList = new LinkedList<>();
					weightsList.add(rule.getWeight());
					classWeights.put(rule.getClassification(), weightsList);
				}else {
					classWeights.get(rule.getClassification()).add(rule.getWeight()); //add current weight to existing ones
				}
			}
		}
		
		if(classWeights.size() == 0) {//no applicable rule found
			Category someClass = knowledgeBase.get(0).getClassification();
			return new ResultCategory(someClass.getAttribute(),"",someClass.getIdt(), false, true); //return NOT PREDICTED category
		}
		
		Map<Category, Double> classCombinedWeights = new HashMap<>();
		//combined weight for every class
		for(Category cat : classWeights.keySet()) {
			classCombinedWeights.put(cat, computeCombinedWeightFromWeights(classWeights.get(cat)));
		}
		double maxWeight = 0;
		Category maxWeightCategory = null; 
		for(Category cat : classCombinedWeights.keySet()) {
			double w = classCombinedWeights.get(cat);
			if(w > maxWeight) {
				maxWeight = w;
				maxWeightCategory = cat;
			}
		}
		if(maxWeightCategory == null ) {
			Category someClass = knowledgeBase.get(0).getClassification();
			return new ResultCategory(someClass.getAttribute(),"",someClass.getIdt(), false, true); //return NOT PREDICTED category		
		}
		//System.out.println(maxWeightCategory.getAttribute() + " " + maxWeightCategory.getValue() + " " + maxWeightCategory.getIdt() + " " +  maxWeight);
		ResultCategory resultCategory = new ResultCategory(maxWeightCategory.getAttribute(), maxWeightCategory.getValue(), maxWeightCategory.getIdt(), false, false);
		if(maxWeight < 0.5+uncBandwith) {
			resultCategory.setUncertain(true);
		}
		return resultCategory;
	}	
	
	/**
	 * @param rule
	 * @param example
	 * @return true if rule is applicable on example
	 */
	public static boolean ruleIsApplicable(AssociationRule rule, Map<String, String> example) {
		//each category of rule premise must be present in example
		if(rule.getPremise() == null)
			return true;
		for(Category cat : rule.getPremise().getCombination()) {
			if(example.get(cat.getAttribute()).equals(cat.getValue()) == false) { //if category from rule's value differs from example's, rule cannot be applicable 
				return false;
			}
		}
		return true;
	}
	
	/** Serialize knowledge base object to disk
	 * @param knowledgeBase
	 * @param filename
	 */
	public static void serializeKnowledgebase (List<AssociationRule> knowledgeBase, String filename)  {
	    File f = new File(filename);
	    FileOutputStream fos;
	    
		try {
			fos = new FileOutputStream(f);
		    ObjectOutputStream oos = new ObjectOutputStream(fos);
		    oos.writeObject(knowledgeBase);
		    oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** Serialize exampleset object to disk
	 * @param es
	 * @param filename
	 */
	public static void serializeExampleSet (ExampleSet es, String filename)  {
	    File f = new File(filename);
	    FileOutputStream fos;
		try {
			fos = new FileOutputStream(f);
		    ObjectOutputStream oos = new ObjectOutputStream(fos);
		    oos.writeObject(es);
		    oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/** 
	 * @param filename
	 * @return Knowledge base deserialized from file 
	 */
	public static List<AssociationRule> getSerializedKnowledgebase(String filename){
		List<AssociationRule> res = null;
		ObjectInputStream in;
		try {
		    FileInputStream file = new FileInputStream(filename); 
			in = new ObjectInputStream(file);
		    // Method for deserialization of object 
		    res = (List<AssociationRule>)in.readObject(); 
		    in.close(); 
		    file.close();
		    return res;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param filename
	 * @return ExampleSet deserialized from file 
	 */
	public static ExampleSet getSerializedExampleSet(String filename){
		ExampleSet res = null;
		ObjectInputStream in;
		try {
		    FileInputStream file = new FileInputStream(filename); 
			in = new ObjectInputStream(file);
		    // Method for deserialization of object 
		    res = (ExampleSet)in.readObject(); 
		    in.close(); 
		    file.close();
		    return res;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static List<Interval> getIntervals(ValueClassCount[] sortedValueClassArray) {
		List<Interval> intervals = new LinkedList<>(); 
		Interval currentInterval = new Interval(sortedValueClassArray[0].getValue(), sortedValueClassArray[0].getValue(), sortedValueClassArray[0].getResultClass());
		currentInterval.addValueClassCount(sortedValueClassArray[0]);
		for(int i = 1; i < sortedValueClassArray.length; i++) {
			ValueClassCount currentVcc = sortedValueClassArray[i]; 
			if(StringUtils.equals(currentVcc.getResultClass(),currentInterval.getIntervalClass())) {
				currentInterval.setUpperBound(currentVcc.getValue());
				currentInterval.addValueClassCount(currentVcc);	
			}else { //current value has different class then currently tested interval
				intervals.add(currentInterval);
				currentInterval = new Interval(sortedValueClassArray[i].getValue(),sortedValueClassArray[i].getValue(), sortedValueClassArray[i].getResultClass());
				currentInterval.addValueClassCount(sortedValueClassArray[i]);
			}			
		}
		//add last interval
		intervals.add(currentInterval);		
		
		//Step 2 in method INTERVAL
		processIntervals(intervals);		
		/*for(Interval i : intervals) {
			System.out.println(i);
		}*/
		return intervals;
	}
	
	/** Represents step 2 and step 3 of the INTERVAL procedure
	 * @param intervals
	 */
	private static void processIntervals(List<Interval> intervals) {
		if(intervals.size() == 1) return;
		for(int i = 0; i < intervals.size();) {
			Interval currInterval = intervals.get(i);
			if(currInterval.getIntervalClass() == null) { //unknown class
				if(i == 0) {//first interval unknown class
					//merge with second interval
					Interval nextInterval = intervals.get(i+1);
					mergeIntervals(currInterval, nextInterval);
					intervals.remove(i); //remove current interval--dont change i since next interval is shifted to currents place
					continue;
					
				}else if(i == intervals.size() -1) {//last interval unknown class
					//merge with last but one interval
					Interval prevInterval = intervals.get(i-1);
					mergeIntervals(prevInterval, currInterval);
					intervals.remove(i-1); //remove prev interval - this will cause loop end
					continue;
				}
				
				Interval prevInterval = intervals.get(i-1);
				Interval nextInterval = intervals.get(i+1);
				
				if(StringUtils.equals(prevInterval.getIntervalClass(), nextInterval.getIntervalClass())) {
					//merge all 3 intervals
					mergeIntervals(prevInterval, currInterval, nextInterval);
					intervals.remove(i);
					intervals.remove(i-1);
					continue;
				}else {
					//count classes of examples in this interval and merge it to interval with class with highest count
					Map<String, Integer> classesCounts = new HashMap<>();
					//compute number of examples of each classes for all values in interval and then assing the interval to the hifhest one 
					for(ValueClassCount vcc : currInterval.getValueClassesInInterval()) {
						for(String clas : vcc.getClassesCounts().keySet()) {
							int classCount = vcc.getClassesCounts().get(clas);
							if(classesCounts.containsKey(clas)) {
								//increase count
								int currCount = classesCounts.get(clas);
								classesCounts.replace(clas, currCount+classCount);
							}else {
								classesCounts.put(clas, classCount);
							}
						}						
					}
					//choose the higher class from neighbour intervals
					String prevClass = prevInterval.getIntervalClass();
					int prevClassCount =  0;					
					if(classesCounts.containsKey(prevClass))
						prevClassCount = classesCounts.get(prevClass);
					
					String nextClass = nextInterval.getIntervalClass();
					int nextClassCount = 0;
					if(classesCounts.containsKey(nextClass))
						nextClassCount = classesCounts.get(nextClass);
					
					if(nextClassCount > prevClassCount) {
						//merge to next class
						mergeIntervals(currInterval, nextInterval);
						intervals.remove(i);
						continue;
					}else {
						//merge to prev class
						mergeIntervals(prevInterval, currInterval);
						intervals.remove(i-1);						
					}					
				}
			}else { //current interval doesnt have unknown class
				i++;//move to next interval
			}
		}
		//change the lower and upper bounds so we create continuous coverage
		//Step 3 in method Interval
		for(int i = 1; i < intervals.size(); i++) {
			Interval prevInterval = intervals.get(i-1);
			Interval currInterval = intervals.get(i);			
			double prevUBound = prevInterval.getUpperBound();
			double currLBount = currInterval.getLowerBound();
			prevInterval.setUpperBound(prevUBound + ((currLBount - prevUBound)/2));
			currInterval.setLowerBound(prevInterval.getUpperBound());			
		}
	}
	
	//merges i1 into i2 - one should have class null
	private static void mergeIntervals(Interval i1, Interval i2) {
		i2.setLowerBound(i1.getLowerBound());
		for(ValueClassCount vcc : i1.getValueClassesInInterval()) {
			i2.addValueClassCount(vcc);
		}
		//situation when last interval has class null - merge into it but chenge its class to i1"s
		if(i2.getIntervalClass() == null) {
			i2.setIntervalClass(i1.getIntervalClass());
		}
	}
	
	//merges all 3 intervals into i3
	private static void mergeIntervals(Interval i1, Interval i2, Interval i3) {
		i3.setLowerBound(i1.getLowerBound());
		for(ValueClassCount vcc : i1.getValueClassesInInterval()) {
			i3.addValueClassCount(vcc);
		}
		for(ValueClassCount vcc : i2.getValueClassesInInterval()) {
			i3.addValueClassCount(vcc);
		}
	}

	
	/** Performs X validation
	 * @param x
	 * @param maxLenOfAnt
	 * @param minFreqOfAnt
	 * @param minValidityOfRule
	 * @param alpha
	 * @param idt
	 * @param uncertaintyBandwidth
	 * @return Result of validation
	 */
	public static ValidationResult crossValidation(int x, double maxLenOfAnt, double minFreqOfAnt, double minValidityOfRule, double alpha, InputDataTable idt, double uncertaintyBandwidth) {
		List<Map<String, String>> shuffledData = new ArrayList<>(idt.getData()); //get copy of idt data for shuffling
		Collections.shuffle(shuffledData);
		int windowSize = idt.countRows()/x; //how many elements will be in test group
		
		int totalCorrect = 0;
		int totalIncorrect = 0;
		int totalUncertain = 0;
		int totalNotPredicted = 0;
		String labelAttribute = idt.getLabelAttribute();
		//ValidationResult validationResult = new ValidationResult(x);
		
		for(int i = 0; i < x; i++) {
			LogService.getRoot().log(Level.INFO, "Cross validation step " + i);
			int correct = 0;
			int incorrect = 0;
			int uncertain = 0;
			int notPredicted = 0;
			//get training and testing data
			List<Map<String, String>> trainingData = new LinkedList<>();
			List<Map<String, String>> testingData = new LinkedList<>();
			for(int j = 0; j < shuffledData.size(); j++) {
				if(j >= i*windowSize && j < (i+1)*windowSize) {
					testingData.add(shuffledData.get(j));
				}else {
					if(i == x - 1 && j >=  (i+1)*windowSize ) {
						testingData.add(shuffledData.get(j)); //if we are in last window, add all the data that is left
					}else { 
						trainingData.add(shuffledData.get(j));
					}
				}
			}
			//create testing and training InputDataTable
			InputDataTable trainingIdt = new InputDataTable(trainingData, idt.getLabelAttribute());
			//trainingIdt.setAttributeTypes(idt.getAttributeTypes());
			InputDataTable testingIdt = new InputDataTable(testingData, idt.getLabelAttribute());
			//testingIdt.setAttributeTypes(idt.getAttributeTypes());
			
			//create knowledgeBase based on training data
			List<AssociationRule> knowledgeBase = Functions.knowledgeExplorer(maxLenOfAnt,minFreqOfAnt,minValidityOfRule,alpha, trainingIdt);
			for(Map<String, String> row : testingIdt.getData()) {
				ResultCategory rowInferredClass = classify(row, knowledgeBase, uncertaintyBandwidth);
				if(rowInferredClass.isUncertain()) {
					uncertain++;
				}else if(rowInferredClass.isNotPredicted()) {
					notPredicted++;
				}else if(rowInferredClass.getValue().equals(row.get(labelAttribute))) {
					correct++;
				}else {
					incorrect++;
				}
			}
			totalCorrect += correct;
			totalIncorrect += incorrect;
			totalNotPredicted += notPredicted;
			totalUncertain += uncertain;
			
			System.out.println("correct = " + correct + " incorrect = " + incorrect + " uncertain = " + uncertain + " not predicted = " + notPredicted);
			System.out.println("Accuracy = " + String.format("%.2f", (double)correct / (correct + incorrect)));
		}
		String msg = x + " X-validation:\n";
		System.out.println(x + " X-validation:");
		
		msg += "correct = " + totalCorrect + " incorrect = " + totalIncorrect + " uncertain = " + totalUncertain + " not predicted = " + totalNotPredicted + "\n";
		System.out.println("correct = " + totalCorrect + " incorrect = " + totalIncorrect + " uncertain = " + totalUncertain + " not predicted = " + totalNotPredicted);
		
		msg += "Accuracy = " + String.format("%.2f", (double)totalCorrect / (totalCorrect + totalIncorrect));
		System.out.println("Accuracy = " + String.format("%.2f", (double)totalCorrect / (totalCorrect + totalIncorrect)));
		return new ValidationResult(x, totalCorrect, totalIncorrect, totalUncertain, totalNotPredicted);
	}

	/** The root method of the whole operator. Creates knowledge base based on KEX algorithm
	 * @param maxLenOfAnt
	 * @param minFreqOfAnt
	 * @param minValidityOfRule
	 * @param alpha
	 * @param inputDataTable
	 * @return Knowledge base as list of rules
	 */
	public static List<AssociationRule> knowledgeExplorer(double maxLenOfAnt, double minFreqOfAnt, double minValidityOfRule, double alpha, InputDataTable inputDataTable) {
		try {
			//System.out.println("starting");
			//LogService.getRoot().log(Level.INFO, "starting");
			int addedRules = 0;
			
			// get all categories
			Set<Category> allCategories = Functions.getAllCategories(inputDataTable);// new HashSet<>();
	
			// ----------------------------------- INITIALIZATION ------------------------------------------
	
			// step 1, find categories with suff. freq.
			SortedSet<Category> categoriesWithSufficientFrequency = Functions.getCategoriesWithSufficientFrequency(allCategories,
					minFreqOfAnt);
			
			// step 2
			// generate rules
			PriorityQueue<AssociationRule> open = new PriorityQueue<>(Collections.reverseOrder()); // I want max to be
																									// on top, so I need
																									// to use
																									// reverseorder
																									// since naturrally
																									// the PQ is ordered
			//add all rules with sufficient frequency to OPEN																						// as Min heap
			for (Category cat : categoriesWithSufficientFrequency) {
				Combination comb = new Combination(inputDataTable);
				comb.addToCobination(cat);
				
				for(int i = 0; i < inputDataTable.getClasses().size(); i++) {
					Category clas = (Category) inputDataTable.getClasses().toArray()[i];
					AssociationRule rule = new AssociationRule(comb, clas);
					open.add(rule);
				}	
			}
	
			// step 3
			//Generate empty rules and add them to KB
			List<AssociationRule> knowledgeBase = new LinkedList<>();
			for(int i = 0; i < inputDataTable.getClasses().size(); i++) {
				Category clas = (Category) inputDataTable.getClasses().toArray()[i];
				AssociationRule emptyRule = new AssociationRule(null, clas ,mapValidityToWeight(clas.getFrequency(), inputDataTable.getClasses().size()));
				knowledgeBase.add(emptyRule);
			}
			
			// -------------------------------------- MAIN LOOP -----------------------------------------------
					
			while (!open.isEmpty()) {
				//log that algo is running
				if(open.size() % 1000 == 0) 
					LogService.getRoot().log(Level.INFO, "Rules to test: " + open.size());
				
				AssociationRule rule = open.poll();			
				//Step 2
				double ruleValidity = rule.getValidity();
				//System.out.println("Polled " + rule.toString() + "validity " + ruleValidity);
				//LogService.getRoot().log(Level.INFO, "Polled " + rule.toString());
				
				//Step 3
				if (ruleValidity > minValidityOfRule || ruleValidity  < 1-minValidityOfRule
						|| Precision.equals(ruleValidity, minValidityOfRule, 0.0000001) || Precision.equals(ruleValidity, 1-minValidityOfRule, 0.0000001)) {//test equality correctway
				
					//Step 3.1.1 - Step 3.1 is contained in setWeight
					double chisq = computeChiSquare(rule, inputDataTable, knowledgeBase, inputDataTable.getClasses());
					//System.out.println("\t chisq= " + chisq);
					ChiSquaredDistribution csd = new ChiSquaredDistribution(inputDataTable.getClasses().size() - 1);
					double tableChisq = csd.inverseCumulativeProbability(1 - alpha);
					if(chisq > tableChisq) {
						//System.out.println("KNOWLEDGE BASE RULE FOUND " + rule + " chisq " + chisq);
						//System.out.println("Total hypotheses: " + addedRules + " To test: " + open.size());
						
						//find subrules for combined weight combination and add all new rule for each class (antecedent is same in all, only classes change)
						for(int i = 0; i < inputDataTable.getClasses().size(); i++) {
							Category clas = (Category) inputDataTable.getClasses().toArray()[i];
							AssociationRule newRule = new AssociationRule(rule.getPremise(), clas);
							List<AssociationRule> subRules = new LinkedList<>();
							for (AssociationRule kbRule : knowledgeBase) {
								if (kbRule.isSubRuleOf(newRule)) {
									subRules.add(kbRule);
								} 							
							}
							//set new weight using combination function on the subrules - validity must be transformed to weight in case of multiclass dataset
							//in case of 2 class dataset the mapping is 1:1
							newRule.setWeight(computeNewRuleWeight(mapValidityToWeight(newRule.getValidity(), inputDataTable.classesCount()), computeCombinedWeight(subRules)));
							
							//add the new rule - it shouldnt be possible, but just to be sure, check if the rule is not present yet
							if(!knowledgeBase.contains(newRule)) {
								//System.out.println("Adding to KB:" + newRule);
								knowledgeBase.add(newRule);
							}
						}							
					}					
				}
				
				//Step 4
				if (rule.getPremiseLength() < maxLenOfAnt) {
					for(Category c : categoriesWithSufficientFrequency) {
						//add to antecedent only categories, which have lower frequency than the lowest one of categories in antecedent
						//here I think there is a mistake in the pseudocode
						//Implemented like this, the algorithm works fine, corresponds with the paper text and gives same results as KEXTask
						if(!rule.getPremise().containsAttribute(c) && c.getFrequency() <= rule.getPremise().getMinimalFrequency()) {							
							//Step 4.1.1
							Combination antA = new Combination(inputDataTable);
							
							//for better performance dont do itemscount on antA automatically but after adding both premise and A
							//that should save 50% time
							antA.addToCobination(rule.getPremise(), false);
							antA.addToCobination(c);
							//System.out.println( "Creating new antecedent " + antA);
							
							//Step 4.1.2 - I dont have to add newRule with each class on right side, because if the rule ant -> Ci 
							//is not yet in Open (doesnt have sufficient frequency) I cannot increase its frequency by adding more categories to its antecednt
							if(antA.getFrequency() >= minFreqOfAnt) {
								AssociationRule newRule = new AssociationRule(antA, rule.getClassification());
								//System.out.println("\t Adding new rule to OPEN " + newRule);
								//double newRuleValidity = newRule.getValidity();
								//if (ruleValidity > minValidityOfRule || ruleValidity  < 1-minValidityOfRule
									//	|| Precision.equals(ruleValidity, minValidityOfRule, TestClass.dbl_epsilon) || Precision.equals(ruleValidity, 1-minValidityOfRule, TestClass.dbl_epsilon)) {
								open.add(newRule);
								addedRules++;
								//}
							}
						}
					}
				}
			}
			System.out.println("KNOWLEDGE BASE: " + knowledgeBase.size() + " rules:");
			//LogService.getRoot().log(Level.INFO, "rules");
			for(AssociationRule kbRule : knowledgeBase) {
				//LogService.getRoot().log(Level.INFO, kbRule.toString());
				System.out.println(kbRule);
			}
			
			return knowledgeBase;
			
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			String exceptionAsString = sw.toString();
			// LogService.getRoot().log(Level.INFO, e.getCause().get);
			//LogService.getRoot().log(Level.INFO, exceptionAsString);
			System.out.println(exceptionAsString);
			return null;
		}
	}

	private static Set<Category> getAllCategories(InputDataTable inputDataTable) {
		Set<Category> allCategories = new HashSet<>();
		
		for(Map<String,String> row : inputDataTable.getData()) {
			Set<String> keys = row.keySet();
			for(String key : keys) {
				//if(!key.equals(labelName)) { //dont create category from label attribute
				if(!key.equals(inputDataTable.getLabelAttribute())) { //dont create category from label attribute
					//for performance improvement, compute ItemCounts only if the category is not present in the set yet
					Category cat = new Category(key, row.get(key), inputDataTable,false);
					//Category cat = new Category(key, row.get(key), inputDataTable);
					
					if(!allCategories.contains(cat)) { 
						cat.computeItemsCount(inputDataTable);  
						allCategories.add(cat);
					}
					
				}else { //i kinda dont understand why this is here.. probably residuum from when this method was in testclass
					Category cat = new Category(key, row.get(key), inputDataTable, false);
					//if(!TestClass.classes.contains(cat)) {
					if(!inputDataTable.getClasses().contains(cat)) {
						cat.computeItemsCount(inputDataTable);
						//TestClass.classes.add(cat);	
						inputDataTable.getClasses().add(cat);	
					}
					
				}
			}			
		}
		return allCategories;
	}

	private static SortedSet<Category> getCategoriesWithSufficientFrequency(Set<Category> allCategories, double minFreq) {
		SortedSet<Category> cats = new TreeSet<>();
		for(Category cat : allCategories) {
			if(cat.getFrequency() >= minFreq) {
				cats.add(cat);
			}
		}
		return cats;
	}
	
}
