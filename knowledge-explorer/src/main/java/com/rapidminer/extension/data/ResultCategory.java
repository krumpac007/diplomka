package com.rapidminer.extension.data;

/**
 * @author Vaclav Motyka
 *
 */
public class ResultCategory extends Category {
	private boolean uncertain = false;
	private boolean notPredicted = false;

	public ResultCategory(String attribute, String value, InputDataTable idt, boolean uncertain, boolean notPredicted) {
		super(attribute, value, idt);
		this.uncertain = uncertain;
		this.notPredicted = notPredicted;
	}

	public boolean isUncertain() {
		return uncertain;
	}

	public void setUncertain(boolean uncertain) {
		this.uncertain = uncertain;
	}

	public boolean isNotPredicted() {
		return notPredicted;
	}

	public void setNotPredicted(boolean notPredicted) {
		this.notPredicted = notPredicted;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (notPredicted ? 1231 : 1237);
		result = prime * result + (uncertain ? 1231 : 1237);
		return result;
	}

	@Override
	public String toString() {
		String res = value;// super.toString();
		if (notPredicted)
			res += " NOT PREDICTED";
		if (uncertain)
			res += " UNCERTAIN ";
		else
			res += " CERTAIN ";
		return res;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultCategory other = (ResultCategory) obj;
		if (notPredicted != other.notPredicted)
			return false;
		if (uncertain != other.uncertain)
			return false;
		return true;
	}

}
