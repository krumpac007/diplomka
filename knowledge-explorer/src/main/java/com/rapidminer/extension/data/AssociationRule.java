package com.rapidminer.extension.data;

import java.io.Serializable;

/**
 * @author Vaclav Motyka
 *
 */
public class AssociationRule implements Comparable<AssociationRule>, Serializable{

	private static final long serialVersionUID = 314867238627473332L;
	private Combination premise;
	private Category classification;
	private double weight;
	private double validity; 
	private double itemsCount;
	private int id; //id have to be used for comparing two same rules to keep insertion order in priority queue
	private static int countOfRulesCreated = 0;

	/** Creates an association rule with weight 0
	 * @param premise 
	 * @param classification 
	 */
	public AssociationRule(Combination premise, Category classification) {
		this(premise, classification, 0.0);
	}

	/** Creates an association rule with specified weight
	 * @param premise 
	 * @param classification 
	 * @param weight
	 */
	public AssociationRule(Combination premise, Category classification, double weight) {
		super();
		this.premise = premise;
		this.classification = classification;
		this.weight = weight;
		this.itemsCount = computeItemsCount();
		computeValidity();
		id = countOfRulesCreated++;
	}
	private double computeValidity() {
		//it was tested that it doesnt matter if I use freqeucny or itemscount for computation
		Combination premiseAndClass = new Combination();
		premiseAndClass.addToCobination(premise);
		premiseAndClass.addToCobination(classification);
		
		if(premise == null) 
			return premiseAndClass.getFrequency();
		if(premise.getItemsCount() == 0) 
			return 0;
		
		validity = premiseAndClass.getFrequency() / premise.getFrequency();
		return validity;
	}
	
	private double computeItemsCount() {
		if(classification == null) return 0;
		if(premise == null) {
			return classification.getItemsCount();
		}
		//for performance purposes, count item count only after the full tmpComb is creating (ie. both premise and class) 
		Combination tmpComb  = new Combination(premise.getCombination(), premise.getIdt(), false);
		tmpComb.addToCobination(classification);
		return tmpComb.getItemsCount();
	}
	
	
	/**  
	 * @param masterRule
	 * @return true if this rule is subrule of masterRule
	 */
	public boolean isSubRuleOf(AssociationRule masterRule) {
		if(!classification.equals(masterRule.getClassification()))
			return false;
		if(premise == null)
			return true;
		//each catogory of premise must be part of masterrules premise for this to be subrule of master
		for(Category c : premise.getCombination()) {
			if(!masterRule.getPremise().containsCategory(c))
				return false;
		}
		return true;
	}
	
	/**
	 * @param subRule
	 * @return true if subRule is subrule of this rule
	 */
	public boolean containsSubRule(AssociationRule subRule) {
		return subRule.isSubRuleOf(this);
	}
	
	@Override
	public String toString() {
		String msg = "";
		//return msg + premise + " => " + classification + "(cnt " + itemsCount + ")-(wght " + String.format("%.3f", weight) + ")-vldt(" + String.format("%.2f", validity) + ")";
		//return msg + premise + " => " + classification + "(cnt " + itemsCount + ")-(wght " + String.format("%.3f", weight) + ")-vldt(" + validity + ")";
		if (premise != null)
			return msg + premise + " >÷< " + classification + "\t" + String.format("%.3f", weight) + "\t" + String.format("%.3f", premise.getFrequency());
		else
			return msg + premise + " >÷< " + classification + "\t" + String.format("%.3f", weight) + "\t0";
	}

	@Override
	public int compareTo(AssociationRule o) {
		if (premise == null) return -1; 
		int res = premise.compareTo(o.getPremise());
		if (res == 0) //for two same rules compare by id to keep order of creation. Earlier created rule has higher value
			return (o.getId() - id);
		return res;
	}

	public int getPremiseLength() {
		if (premise == null) return 0;
		return premise.getCombination().size();
	}
	
	public Combination getPremise() {
		return premise;
	}

	public Category getClassification() {
		return classification;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getValidity() {
		return validity;
	}

	public double getItemsCount() {
		return itemsCount;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classification == null) ? 0 : classification.hashCode());
		result = prime * result + ((premise == null) ? 0 : premise.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssociationRule other = (AssociationRule) obj;
		if (classification == null) {
			if (other.classification != null)
				return false;
		} else if (!classification.equals(other.classification))
			return false;
		if (premise == null) {
			if (other.premise != null)
				return false;
		} else if (!premise.equals(other.premise))
			return false;
		return true;
	}

	
	
	
}
