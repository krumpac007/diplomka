package com.rapidminer.extension.data;

import java.io.Serializable;
import java.util.BitSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vaclav Motyka
 *
 */
public class Category implements Comparable<Category>, Serializable{
	private static final long serialVersionUID = 9040751306383616618L;
	protected String attribute;
	protected String value;
	protected double itemsCount = -1;
	protected InputDataTable idt;
	protected double frequency;
	//protected Set<Integer> rowIndexes = new HashSet<>(); //store row indexes this category appears on, so when creating combination we dont have to go through the whole idt
	protected BitSet presentInRows; //bit vector. every bit i represent if this category is present on row i of idt
	
	/** Generates Category A(v) where A is attribute and v is value. Then computes number of examples with this category in idt 
	 * 
	 * @param attribute
	 * @param value
	 * @param idt 
	 */
	public Category(String attribute, String value, InputDataTable idt) {
		this(attribute, value, idt, true);
	}

	
	/** Creates Category object. Computation of number of examples is optional
	 * @param attribute
	 * @param value
	 * @param idt
	 * @param computeItemsCountAutomatically Specifies if computation of number of examples is done
	 */
	public Category(String attribute, String value, InputDataTable idt, boolean computeItemsCountAutomatically) {
		super();
		this.attribute = attribute;
		this.value = value;
		this.idt = idt;
		presentInRows = new BitSet(idt.countRows());
		if(computeItemsCountAutomatically) {
			//this.itemsCount = computeItemsCount(idt);
			computeItemsCount(idt);
			//this.frequency = (double)itemsCount / idt.countRows();
		}
	}
	
	/** Computes number of examples in idt containing this category and correspongding frequency.
	 * @param idt
	 */
	public void computeItemsCount(InputDataTable idt) {
		if(itemsCount != -1) //dont compute more than once
			//return itemsCount;
			return;
		
		double itemCount = 0;
		List<Map<String,String>> data = idt.getData();		
		
		int rowIndex = 0;
		for(Map<String, String> row : data) {			
			if(row.containsKey(attribute) && row.get(attribute).equals(value)) {
				if(idt.hasWeightAttribute()) {
					itemCount += Double.parseDouble(row.get(idt.getWeightAttribute())); 
				}
				else {	
					itemCount++;
				}
				presentInRows.set(rowIndex);
			}
			rowIndex++;
		}		
		this.itemsCount = itemCount;
		if(!idt.hasWeightAttribute()) {
			this.frequency = (double)itemsCount / idt.countRows();
		}else
			this.frequency = (double)itemsCount / idt.getWeightsSum();
	}

	public String getAttribute() {
		return attribute;
	}

	public String getValue() {
		return value;
	}
	
	public InputDataTable getIdt() {
		return idt;
	}

	public double getItemsCount() {
		return itemsCount;
	}

	public double getFrequency() {
		return frequency;
	}

	public BitSet getPresentInRows() {
		return presentInRows;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		//return attribute + "(" + value + ")^" + itemsCount + "^" + String.format("%.2f", frequency);
		return attribute + "(" + value + ")";
	}

	@Override
	public int compareTo(Category other) {
		int res = attribute.compareTo(other.getAttribute());
		if(res == 0) {
			return value.compareTo(other.getValue());
		}
		return res;
		
	}
	
	
	
	
}
