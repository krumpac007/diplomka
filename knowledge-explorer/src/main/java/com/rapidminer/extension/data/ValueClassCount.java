package com.rapidminer.extension.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;

/** This class represents steps 2.1 and 2.2 of the KEX algorithm
 * @author Vaclav Motyka
 *
 */
public class ValueClassCount implements Comparable<ValueClassCount> {
	private String attribute;
	private double value;
	private Map<String, Integer> classesCounts;
	private String resultClass;
	private InputDataTable idt;
	private int numOfExamples = 0; // number of examples of current value
	private double chisq = 0;
	private double alpha;
	private Interval interval;

	public ValueClassCount(String attribute, double value, InputDataTable idt, double alpha) {
		this.value = value;
		this.attribute = attribute;
		this.idt = idt;
		this.alpha = alpha;
		this.classesCounts = new HashMap<>(); // mapping class to number of examples having the class and this.value
		computeClassesCount();
	}

	// Step 2.1
	private void computeClassesCount() {
		List<String> classValues = idt.getClassesValues();
		/*
		 * for(String clas : classValues ) { classesCounts.put(clas, 0); }
		 */
		for (Map<String, String> row : idt.getData()) {
			double rowValue = Double.parseDouble(row.get(attribute));
			if (rowValue == this.value) {
				String rowClass = row.get(idt.getLabelAttribute());
				if (classesCounts.containsKey(rowClass)) {
					int oldClassCount = classesCounts.get(rowClass);
					int newClassCount = oldClassCount + 1;
					classesCounts.replace(rowClass, newClassCount);
				} else {
					classesCounts.put(row.get(idt.getLabelAttribute()), 1);
				}
				numOfExamples++;
			}
		}
		assign();
	}

	// Step 2.2
	private void assign() {
		if (classesCounts.size() == 1) {
			resultClass = (String) classesCounts.keySet().toArray()[0]; // get the only value
			return;
		}

		for (String clas : idt.getClassesValues()) {
			Category currentClassCategory = new Category(idt.getLabelAttribute(), clas, idt);
			double currentClassCount = currentClassCategory.getItemsCount();

			int currentNumberOfExamples = 0;
			if (classesCounts.containsKey(clas)) // if some class has this value, it is stored in classesCounts
				currentNumberOfExamples = classesCounts.get(clas);

			double expectedNumOfExamples = (double) (numOfExamples * currentClassCount) / idt.countRows();
			chisq += ((currentNumberOfExamples - expectedNumOfExamples)
					* (currentNumberOfExamples - expectedNumOfExamples)) / expectedNumOfExamples;
		}
		int degreesOfFreedom = idt.getClassesValues().size() - 1;
		ChiSquaredDistribution chiSquaredDistribution = new ChiSquaredDistribution(degreesOfFreedom);
		double chisqFromTable = chiSquaredDistribution.inverseCumulativeProbability(1 - alpha);
		// System.out.println(" table " + chisqFromTable);
		if (chisq > chisqFromTable) {
			// result class is the one with highest freq.
			String maxCountClass = (String) classesCounts.keySet().toArray()[0];
			int maxCount = classesCounts.get(maxCountClass);
			for (String clas : classesCounts.keySet()) {
				int currentClassCount = classesCounts.get(clas);
				if (currentClassCount > maxCount) {
					maxCountClass = clas;
					maxCount = currentClassCount;
				}
			}
			resultClass = maxCountClass;
		} else {
			// result class unknown
			resultClass = null;
		}
	}
/*
	public String getAttribute() {
		return attribute;
	}
*/

	public double getValue() {
		return value;
	}

	public String getResultClass() {
		return resultClass;
	}
/*
	public Interval getInterval() {
		return interval;
	}
*/
	public Map<String, Integer> getClassesCounts() {
		return classesCounts;
	}

	@Override
	public String toString() {
		String counts = "";
		for (String key : classesCounts.keySet()) {
			counts += key + ":" + classesCounts.get(key) + " ";
		}
		return "ValueCount> " + value + " -> " + counts + " CHISQ: " + chisq + " ---> " + resultClass;
	}

	@Override
	public int compareTo(ValueClassCount other) {
		return Double.compare(value, other.value);
	}

}
