package com.rapidminer.extension.data;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.rapidminer.tools.LogService;


/**
 * @author Vaclav Motyka
 *
 */
public class Combination implements Comparable<Combination>, Serializable{

	private static final long serialVersionUID = -3146107554694037101L;
	private List<Category> combination = new LinkedList<>();
	private double itemsCount = -1;
	private double frequency;
	private static InputDataTable idt = null;
	private BitSet presentInRows = new BitSet();

	
	/** Creates empty combination (idt must be already set)
	 * 
	 */
	public Combination() {				
		if(Combination.idt == null) {
			LogService.getRoot().log(Level.INFO, "Combination IDT is null!!");
		}
	}	
	
	/** Creates empty combination and sets its idt 
	 * @param idt
	 */
	public Combination(InputDataTable idt) {
		Combination.idt = idt;
	}

	/** Creates new combination and adds category c to it and compute number of examples containing this combination
	 * @param c
	 */
	public Combination(Category c) {
		combination.add(c);
		if(Combination.idt == null) {
			LogService.getRoot().log(Level.INFO, "Combination IDT is null!!");
		}
		computeItemsCount(idt);
		//this.itemsCount = computeItemsCount(idt);
		//this.frequency = (double)itemsCount / idt.countRows();
	}
	
	/** Creates new combination with categories from parameter combination. Computaion of examples is done.
	 * @param combination
	 * @param idt
	 */
	public Combination(List<Category> combination, InputDataTable idt) {
		this(combination, idt, true);
	}
	
	/** Creates new combination with categories from parameter combination. Computaion of examples is optional.
	 * @param combination
	 * @param idt
	 * @param computeItemsCountAutomatically
	 */
	public Combination(List<Category> combination, InputDataTable idt, boolean computeItemsCountAutomatically) {
		super();
		//this.combination = combination;
		this.combination = new LinkedList<>();
		for(Category c : combination) {
			this.combination.add(c);
		}
		Combination.idt = idt;
		if(computeItemsCountAutomatically) {
			computeItemsCount(idt);
		}
	};
		
	/** Adds c to combination and computes number of examples containg the resulting combination.
	 * @param c
	 */
	public void addToCobination(Category c) {
		addToCobination(c, true);
	}
	
	/** Adds c to combination and computes number of examples containg the resulting combination.
	 * @param c
	 */
	public void addToCobination(Combination c) {
		addToCobination(c, true);
	}
	
	/** Adds c to combination. Computation number of examples containg the resulting combination is optional.
	 * @param c
	 */
	public void addToCobination(Category c, boolean computeItemsCountAutomatically) {
		if(computeItemsCountAutomatically) {
			if(c != null) {
				combination.add(c);
				computeItemsCount(idt);
			}		
		}else {
			if(c != null) {
				combination.add(c);
			}
		}
	}
	
	/** Adds c to combination. Computation number of examples containg the resulting combination is optional.
	 * @param c
	 */
	public void addToCobination(Combination c, boolean computeItemsCountAutomatically ) {
		if(computeItemsCountAutomatically) {
			if(c != null) {
				for (Category cat : c.getCombination()) {
					this.combination.add(cat);
				}
				computeItemsCount(idt);
			}
		}else {
			if(c != null) {
				for (Category cat : c.getCombination()) {
					this.combination.add(cat);
				}	
			}
		}
	}
	
	public void computeItemsCount(InputDataTable idt) {
		BitSet fullBitSet = new BitSet(); //bitset of all 1s
		fullBitSet.set(0, idt.countRows()); //set 1 for every row
		
		for(Category category : combination) {
			fullBitSet.and(category.getPresentInRows()); //make logical AND for every combination's bit vector, that leaves us with bit vector with 1s on row that fullfills all categories
		}
		if(idt.hasWeightAttribute()) {
			double weight =0; //weight will be sum of weights of all rows that remained in fullbitset (ie contain the combination)
			
			//i is row(bit) index
			for (int i = fullBitSet.nextSetBit(0); i != -1; i = fullBitSet.nextSetBit(i + 1)) {
			    weight += Double.parseDouble(idt.getData().get(i).get(idt.getWeightAttribute()));
			}
			itemsCount = weight;			
			frequency = itemsCount / idt.getWeightsSum();
			
		}else {
			itemsCount = fullBitSet.cardinality();
			frequency = itemsCount / idt.countRows();
		}
	}
	
	
	//Combination contains category ( = attribute, value)
	/** 
	 * @param targetCategory
	 * @return true if this category contains category targetCategory
	 */
	public boolean containsCategory(Category targetCategory) {		
		for(Category c : combination) {
			if(c.equals(targetCategory)) {
				return true;
			}
		}
		return false;
	}
	
	//contains category with attibute (value of attribute is not important)
	/**
	 * @param targetCategory
	 * @return true if this combination contains attribute (value of attribute doesnt matter)
	 */
	public boolean containsAttribute(Category targetCategory) {		
		for(Category c : combination) {
			String attribute = c.getAttribute();
			if(attribute.equals(targetCategory.getAttribute())) {
				return true;
			}
		}
		return false;
	}
	
	public List<Category> getCombination() {
		return combination;
	}
/*
	public void setCombination(List<Category> combination) {
		this.combination = combination;
	}
*/
	public double getItemsCount() {
		return itemsCount;
	}
	
	public double getFrequency() {
		return frequency;
	}

	public static InputDataTable getIdt() {
		return idt;
	}

	@Override
	public String toString() {
		String msg = "";
		boolean first = true;
		Collections.sort(combination);
		for (Category cat : combination) {
			if(!first) msg += " & ";
			msg += cat.toString();
			first = false;
		}
		return msg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((combination == null) ? 0 : combination.hashCode());
		long temp;
		temp = Double.doubleToLongBits(frequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(itemsCount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Combination other = (Combination) obj;
		if (combination == null) {
			if (other.combination != null)
				return false;
		}
		if (Double.doubleToLongBits(frequency) != Double.doubleToLongBits(other.frequency))
			return false;
		if (itemsCount != other.itemsCount)
			return false;
		
		//two combinations are equal if the size is same
		//and all categories from one are present in the second
		//this comparison is enough becouse each category can be contained only once in the combination (according to the algorithm)
		if(combination.size() != other.getCombination().size())
			return false;
		
		for(Category c : combination) {
			if(!other.containsCategory(c))
				return false;
		}
		
		return true;
	}

	@Override
	public int compareTo(Combination o) {
		return Double.compare(frequency, o.getFrequency());
	}

	
	/**
	 * @return minimal frequency of any category in this combination 
	 */
	public double getMinimalFrequency() {
		double minf = combination.get(0).getFrequency();
		for(Category c : combination) {
			if(c.getFrequency() < minf)
				minf = c.getFrequency();
		}
		return minf;
	}

	
	
}
