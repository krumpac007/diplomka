package com.rapidminer.extension.data;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Vaclav Motyka
 *
 */
public class Interval {
	private double lowerBound;
	private double upperBound;
	private String intervalClass;
	List<ValueClassCount> valueClassesInInterval = new LinkedList<>();

	/** Creates interval with lower and upper bound. Sets its class to intervalClass
	 * @param lowerBound
	 * @param upperBound
	 * @param intervalClass
	 */
	public Interval(double lowerBound, double upperBound, String intervalClass) {
		super();
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.intervalClass = intervalClass;
	}

	public void addValueClassCount(ValueClassCount vcc) {
		valueClassesInInterval.add(vcc);
	}

	public List<ValueClassCount> getValueClassesInInterval() {
		return valueClassesInInterval;
	}

	public String getIntervalClass() {
		return intervalClass;
	}

	public void setIntervalClass(String intervalClass) {
		this.intervalClass = intervalClass;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	public double getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public boolean inInterval(double value) {
		if (value >= lowerBound && value <= upperBound)
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((intervalClass == null) ? 0 : intervalClass.hashCode());
		long temp;
		temp = Double.doubleToLongBits(lowerBound);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(upperBound);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Interval other = (Interval) obj;
		if (intervalClass == null) {
			if (other.intervalClass != null)
				return false;
		} else if (!intervalClass.equals(other.intervalClass))
			return false;
		if (Double.doubleToLongBits(lowerBound) != Double.doubleToLongBits(other.lowerBound))
			return false;
		if (Double.doubleToLongBits(upperBound) != Double.doubleToLongBits(other.upperBound))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// return "<" + String.format("%.2f",lowerBound) + ";" +
		// String.format("%.2f",upperBound) + ">(" + intervalClass + ")";
		// return "<" + String.format("%.2f",lowerBound) + ";" +
		// String.format("%.2f",upperBound) + ">";
		return "<" + Double.toString(Math.round(lowerBound * 100.0) / 100.0) + ";"
				+ Double.toString(Math.round(upperBound * 100.0) / 100.0) + ">"; // formatting to 2 dec. spots
	}

}
