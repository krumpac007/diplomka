package com.rapidminer.extension.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.Ontology;

/**
 * @author Vaclav Motyka
 *
 */
public class InputDataTable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5688708246856111863L;
	private List<Map<String, String>> data = new ArrayList<>();
	private String labelAttribute = "";
	private String weightAttribute = "";
	private Set<Category> classes = null;
	private boolean hasWeightAttribute = false;
	private double weightsSum = -1;

	/** Creates new IDT from data and sets labelAttribute
	 * @param data
	 * @param labelAttribute
	 */
	public InputDataTable(List<Map<String, String>> data, String labelAttribute) {
		this.labelAttribute = labelAttribute;
		this.data = data;
		//findAttributeTypes();
		classes = new HashSet<>();
		findClasses();
	}

	/** Creates new IDT from exampleset. Label and weight attributes is set automatically from examplesSet.
	 * @param exampleSet
	 */
	public InputDataTable(ExampleSet exampleSet) {
		//this(exampleSet, exampleSet.getAttributes().getLabel().getName());
		Attributes attributes = exampleSet.getAttributes();
		Attribute labelAtt = exampleSet.getAttributes().getLabel();
		Attribute weightAtt = exampleSet.getAttributes().getWeight();
		if(weightAtt != null) {
			hasWeightAttribute = true;
			weightAttribute = weightAtt.getName();
		}
		
		labelAttribute = labelAtt.getName();
		int i = 1;
		//convert input data to table
		for(Example e : exampleSet) {
			LogService.getRoot().log(Level.INFO, "IDT> Creating row "+ (i++));
			Map<String, String> row = new HashMap<>();
			row.put(labelAttribute, e.getValueAsString(labelAtt));
			if(hasWeightAttribute)
				row.put(weightAttribute, e.getValueAsString(weightAtt));
			
			for (Attribute a : attributes) {				
				row.put(a.getName(), e.getValueAsString(a));
			}
			data.add(row);
		}
		classes = new HashSet<>();
		findClasses();
		
		if(hasWeightAttribute)
			computeWeightsSum();
	}
	
	/** Creates IDT from file
	 * @param path
	 * @param delimiter
	 * @param labelAttribute
	 */
	public InputDataTable(String path, String delimiter, String labelAttribute) {
		parseFile(path, delimiter);
		//this(path, delimiter);
		this.labelAttribute = labelAttribute;
		classes = new HashSet<>();
		findClasses();
	}

	/**
	 * @return List of values of label attribute
	 */
	public List<String> getClassesValues() {
		List<String> res = new LinkedList<>();
		for (Category c : classes) {
			res.add(c.getValue());
		}
		return res;
	}

	/** 
	 * @return data in form of ¨table¨ (list of maps representing each row)
	 */
	public List<Map<String, String>> getData() {
		return data;
	}

	public Set<Category> getClasses() {
		return classes;
	}

	public String getLabelAttribute() {
		return labelAttribute;
	}

	private void findClasses() {
		//int i = 0;
		LogService.getRoot().log(Level.INFO, "Finding classes");
		for (Map<String, String> m : data) {
			String value = m.get(labelAttribute);
			Category c = new Category(labelAttribute, value, this, false);//dont comute itemcount for every observation
			if(!classes.contains(c)) {
				c.computeItemsCount(this);
				classes.add(c);			
			}
			
			//System.out.println(i++);
		}
		LogService.getRoot().log(Level.INFO, "Classes found");
	}

	/**
	 * @return number of rows of this IDT
	 */
	public int countRows() {
		return data.size();
	};

	/**
	 * @return sum of weights of all rows
	 */
	private double computeWeightsSum() {
		if(!hasWeightAttribute())
			return countRows();
		else {
			double totalWeight = 0;
			for(Map<String, String> row : data) {
				totalWeight += Double.parseDouble(row.get(weightAttribute));
			}
			this.weightsSum = totalWeight;
			return totalWeight;
		}		
	}
	
	public double getWeightsSum() {
		return weightsSum;
	}
	/**
	 * @return number of classes of this IDT
	 */
	public int classesCount() {
		return classes.size();
	}

	public List<String> getColumn(String columnName) {
		List<String> res = new LinkedList<>();
		for (Map<String, String> row : data) {
			res.add(row.get(columnName));
		}
		return res;
	}

	public void print() {
		for (Map<String, String> m : data) {
			// m.forEach((key, value) -> LogService.getRoot().log(Level.INFO, (key + ":" +
			// value)));
			m.forEach((key, value) -> System.out.print(key + ":" + value + " "));
			System.out.println();
		}
	}

	public void printCSV() {
		boolean first = true;
		String msg = "";
		for (String att : data.get(0).keySet()) {
			if (!first) {
				msg += ",";
			}
			msg += att;
			first = false;
		}
		System.out.println(msg);

		for (Map<String, String> row : data) {
			msg = "";
			first = true;
			for (String att : row.keySet()) {
				if (!first) {
					msg += ",";
				}
				msg += row.get(att);
				first = false;
			}
			System.out.println(msg);
		}
	}

	private void parseFile(String path, String delimiter) {
		BufferedReader reader;
		boolean firstLine = true;
		String[] attributes = null;
		data = new LinkedList<>();

		try {
			reader = new BufferedReader(new FileReader(path));
			String line = reader.readLine();
			while (line != null) {
				if (line.isEmpty())
					continue;
				if (firstLine) {
					attributes = line.split(delimiter);
					firstLine = false;
					line = reader.readLine();
					continue;
				}

				String[] values = new String[attributes.length];
				for (int i = 0; i < values.length; i++) {
					values[i] = "";
				}
				// in case there are some values missing (esp. in last column, if I usedd split
				// only, exception would be thornw)
				String[] lineArr = line.split(delimiter);
				for (int i = 0; i < lineArr.length; i++) {
					values[i] = lineArr[i];
				}
				Map<String, String> row = new HashMap<>();
				for (int i = 0; i < attributes.length; i++) {
					row.put(attributes[i], values[i]);
				}
				data.add(row);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		//result = prime * result + ((attributeTypes == null) ? 0 : attributeTypes.hashCode());
		result = prime * result + ((classes == null) ? 0 : classes.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((labelAttribute == null) ? 0 : labelAttribute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InputDataTable other = (InputDataTable) obj;
		/*if (attributeTypes == null) {
			if (other.attributeTypes != null)
				return false;
		} else if (!attributeTypes.equals(other.attributeTypes))
			return false;*/
		if (classes == null) {
			if (other.classes != null)
				return false;
		} else if (!classes.equals(other.classes))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (labelAttribute == null) {
			if (other.labelAttribute != null)
				return false;
		} else if (!labelAttribute.equals(other.labelAttribute))
			return false;
		return true;
	}

	/**
	 * @return true if this IDT has weight attribute
	 */
	public boolean hasWeightAttribute() {
		return this.hasWeightAttribute;
	}

	/**
	 * @return true if this IDT has label attribute
	 */
	public String getWeightAttribute() {
		return weightAttribute;
	}

	
}
