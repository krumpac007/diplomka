package com.rapidminer.extension.data;


/** Represents validation results
 * @author Vaclav Motyka
 *
 */
public class ValidationResult {
	private double accuracy = 0;
	private int iterations;
	private int correct;
	private int incorrect;
	private int uncertain;
	private int notPredicted;
	private double percentOfCertain;
	
	
	public ValidationResult(int iterations, int correct, int incorrect, int uncertain, int notPredicted) {
		this.iterations = iterations;
		this.correct = correct;
		this.incorrect = incorrect;
		this.uncertain = uncertain;
		this.notPredicted = notPredicted;
		accuracy = (double)correct / (correct + incorrect);
		percentOfCertain = ((double)correct + incorrect) / ((double)correct + incorrect + uncertain + notPredicted);
	}
	
	
	public double getAccuracy() {
		return accuracy;
	}

	public double getPercentOfCertain() {
		return percentOfCertain;
	}
	
	public int getIterations() {
		return iterations;
	}


	public int getCorrect() {
		return correct;
	}


	public int getIncorrect() {
		return incorrect;
	}


	public int getUncertain() {
		return uncertain;
	}


	public int getNotPredicted() {
		return notPredicted;
	}

	@Override
	public String toString() {
		String msg = iterations + "-Validation:\n";
		msg += accuracy + "\n";
		msg += "correct = " + correct + " incorrect = " + incorrect + " uncertain = " + uncertain + " not predicted = " + notPredicted + "\n";
		
		return msg;
	}
	
	
	
	
	
	
}
