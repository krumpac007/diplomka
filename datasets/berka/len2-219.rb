---------------------------- Create knowledge base --------------------------- 
  Analysis done on Wed Oct 11 08:39:17 2000
  Input file : len2.kad 
  Output file: len2-219.rb 
------------------------------------------------------------------------------ 
C:\EXPERT\KEX\LENSES.ASC
Contact lenses data by Cendrowska
96  5
age
3
a young
b pre-presbyotic
c presbyotic
prescription
2
m myope
h hypermerope
astigmatic
2
n no
y yes
tears production rate
2
r reduced
n normal
class
3
h hard lenses
s soft lenses
n none lenses


      INPUT PARAMETERS

    Combination:  5.
     Min length (n): 1
     Max length (n): 2
  Min frequency (n): 1
  Max frequency (n): 200
   Min validity (%): 90.000000
   Max validity (%): 100.000000
   Min coverage (%): 0.000000
   Max coverage (%): 100.000000




           FREQUENCIES OF CATEGORIES

 Att.  cat.    fr.    cat.    fr.    cat.    fr.    cat.    fr.    cat.    fr.
--------------------------------------------------------------------------------  
   1    1a    32.00    1b    32.00    1c    32.00  
   2    2m    48.00    2h    48.00  
   3    3n    48.00    3y    48.00  
   4    4r    48.00    4n    48.00  
   5    5h    16.00    5s    20.00    5n    60.00  



          META PARAMETERS

 Use only positive literals (y/n) ?  y
Insert empty rule (y/n) ?  y
Test implications (y/n) ?  y
Insert rules with weight equal to 0.5 (n/y) ?  n
Expand implications with validity = 100% (n/y) ?  n
Ignore missing classes (n/y) ?  n



                  GENERATED RULES

                Frequencies                              
   no.      left     right      both   weight   Implication
------------------------------------------------------------------------
     1     96.00     16.00     16.00   0.1667   0-  ==>  5h
     2     96.00     20.00     20.00   0.2083   0-  ==>  5s
     3     96.00     60.00     60.00   0.6250   0-  ==>  5n
     4     48.00     16.00      0.00   0.0500   4r  ==>  5h
     5     48.00     20.00      0.00   0.0385   4r  ==>  5s
     6     48.00     60.00     48.00   0.9828   4r  ==>  5n



     NUMBER OF GENERATED IMPLICATIONS

                      validity                    
length     0   (0,50)   50  (50,100)  100      sum
-----------------------------------------------------
   1       0      2      0      1      1         4
   2       0      0      0      0      3         3
-----------------------------------------------------
total      0      2      0      1      4         7



     NUMBER OF GENERATED RULES   6



End

