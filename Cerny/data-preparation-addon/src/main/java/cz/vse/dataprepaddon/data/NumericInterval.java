package cz.vse.dataprepaddon.data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import cz.vse.dataprepaddon.ClassIntervalsOperator;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Jan on 19.4.2015.
 */
public class NumericInterval extends ValueClassSet {
    private double upperBound;
    private double lowerBound;


    public NumericInterval(Attribute valueAttribute, Attribute targetAttribute) {
        super(valueAttribute, targetAttribute);
    }

    @Override
    public boolean addExample(Example example) {
        boolean result = super.addExample(example);

        updateBounds();

        return result;
    }


    @Override
    public void append(ValueSet other) {
        super.append(other);

        Collections.sort(values,new NumericValueCountComparator());

        updateBounds();
    }

    public void appendBefore(ValueSet other) {
        other.append(this);

        updateBounds();
    }

    private void updateBounds() {
        Object topValue = values.get(0).value;
        Object bottomValue = values.get(values.size()-1).value;

        if(topValue instanceof Double && bottomValue instanceof Double) {
            upperBound = (Double)bottomValue;
            lowerBound = (Double)topValue;
        }
        else {
            //TODO -errror
        }
    }



    public double getLowerBound() {
        return lowerBound;
    }

    public double getUpperBound() {
        return upperBound;
    }

    public String toString() {
        return lowerBound + "-" + upperBound + "(" + getClassCode() + ")";
    }


    class NumericValueCountComparator implements Comparator<ValueCount> {
        @Override
        public int compare(ValueCount o1, ValueCount o2) {

            return Double.compare((Double)o1.value, (Double)o2.value);
        }
    }
}
