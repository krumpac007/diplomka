package cz.vse.dataprepaddon;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.tools.Ontology;
import cz.vse.dataprepaddon.data.CategoryGroup;
import cz.vse.dataprepaddon.data.NumericInterval;
import cz.vse.dataprepaddon.data.ValueCount;

import java.util.*;

/**
 * Created by Jan on 14.4.2015.
 */
public class ClassGroupingOperator extends Operator {
    public static final String PARAMETER_TARGET = "Target attribute";
    public static final String PARAMETER_GROUP = "Attribute to group";
    public InputPort datasetInput = getInputPorts().createPort("Input dataset");
    public OutputPort outputPort = getOutputPorts().createPort("Output dataset (includes new attribute)");

    private List<ValueCount> normalCounts;
    private List<CategoryGroup> groups;


    public ClassGroupingOperator(OperatorDescription description) {
        super(description);
    }


    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeAttribute(PARAMETER_TARGET, "Target attribute", datasetInput));
        types.add(new ParameterTypeAttribute(PARAMETER_GROUP, "Attribute to be grouped", datasetInput));

        return types;
    }

    @Override
    public void doWork() throws OperatorException {

        normalCounts = new ArrayList<>();
        groups = new ArrayList<>();

        ExampleSet inputSet = datasetInput.getData();

        String targetAttributeName = getParameterAsString(PARAMETER_TARGET);
        String groupAttributeName = getParameterAsString(PARAMETER_GROUP);

        Attribute targetAttribute = Util.findAttributeByName(inputSet, targetAttributeName);
        Attribute groupAttribute = Util.findAttributeByName(inputSet, groupAttributeName);

        initValueClassInfo(inputSet, groupAttribute, targetAttribute);

        assign(0.2);
        group();

        deliver(inputSet, groupAttribute, groupAttributeName + "_grouped");
    }

    /**
     * Create list of values of the attribute to be grouped, calculate distribution in classes for each of the values
     * @param inputSet
     * @param attribute
     * @param targetAttribute
     */
    private void initValueClassInfo(ExampleSet inputSet, Attribute attribute, Attribute targetAttribute) {
        for(int i = 0; i < inputSet.size(); i++) {
            Example example = inputSet.getExample(i);
            String attributeValue = example.getValueAsString(attribute);
            String targetValue = example.getValueAsString(targetAttribute);
            ValueCount count = Util.getClassCount(targetValue, normalCounts);

            if(count == null) {
                count = new ValueCount(targetValue);
                normalCounts.add(count);
            }
            else {
                count.count++;
            }

            CategoryGroup group = (CategoryGroup) Util.findValueSetByAttributeValue(groups, attributeValue);

            if(group != null) {
                group.incTarget(targetValue);
            }
            else {
                CategoryGroup categoryGroup = new CategoryGroup(attribute, targetAttribute);
                categoryGroup.addExample(example);

                groups.add(categoryGroup);

            }
        }
    }

    private void assign(double alpha) {
        for(CategoryGroup group : groups) {
            group.assign(normalCounts, alpha);
        }
    }

    private void group() {
        String currentClassCode = "";
        NumericInterval currentInterval = null;

        Map<String,CategoryGroup> groupsByClassCode = new TreeMap<>();

        for(CategoryGroup group : groups) {
            String classCode = group.getClassCode();

            if(groupsByClassCode.containsKey(classCode)) {
                CategoryGroup currentGroup = groupsByClassCode.get(classCode);
                currentGroup.append(group);
            }
            else {
                groupsByClassCode.put(classCode,group);
            }
        }

        groups = new ArrayList<>(groupsByClassCode.values());
    }



    private void deliver(ExampleSet inputSet, Attribute groupedAttribute, String groupAttributeName) {
        Attribute attribute = AttributeFactory.createAttribute(groupAttributeName, Ontology.STRING);
        attribute.setTableIndex(inputSet.size());

        inputSet.getExampleTable().addAttribute(attribute);
        inputSet.getAttributes().addRegular(attribute);

        for(Example example : inputSet) {
            CategoryGroup group = (CategoryGroup) Util.findValueSetByAttributeValue(groups, example.getValueAsString(groupedAttribute));
            if(group != null) {
                example.setValue(attribute, group.getGroupIDWithClassInfo());
            }
            else {
                //TODO -throw error
            }
        }

        outputPort.deliver(inputSet);
    }
}
