package cz.vse.dataprepaddon;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.*;
import com.rapidminer.tools.Ontology;
import cz.vse.dataprepaddon.data.CategoryGroup;
import cz.vse.dataprepaddon.data.ValueCount;
import cz.vse.dataprepaddon.data.NumericInterval;

import java.util.*;

/**
 * Created by Jan on 5.4.2015.
 */
public class ClassIntervalsOperator extends Operator {
    public static final String PARAMETER_TARGET = "Target attribute";
    public static final String PARAMETER_DISCRETIZED = "Attribute to discretize";
    public static final String PARAMETER_MIN_INTERVAL_SIZE = "Minimum interval size";
    public static final String PARAMETER_ALPHA = "ChiSq Level of Significance";
    public InputPort datasetInput = getInputPorts().createPort("Input dataset");
    public OutputPort outputPort = getOutputPorts().createPort("Output dataset (includes new attribute)");

    private List<ValueCount> normalCounts;
    private List<NumericInterval> intervals;


    public ClassIntervalsOperator(OperatorDescription description) {
        super(description);
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeAttribute(PARAMETER_TARGET, "Target attribute", datasetInput));
        types.add(new ParameterTypeAttribute(PARAMETER_DISCRETIZED, "Attribute to be discretized", datasetInput));
        types.add(new ParameterTypeInt(PARAMETER_MIN_INTERVAL_SIZE, "Minimal number of data objects allowed in an interval", 1, 1000000));
        types.add(new ParameterTypeDouble(PARAMETER_ALPHA, "Significance level of the ChiSq test", 0, 0.5));

        return types;
    }


    @Override
    public void doWork() throws OperatorException {
        try {   //INITIALIZATION
            normalCounts = new ArrayList<>();
            intervals = new ArrayList<>();

            double alpha = getParameterAsDouble(PARAMETER_ALPHA);
            String distretizeAttributeName = getParameterAsString(PARAMETER_DISCRETIZED);
            ExampleSet inputSet = datasetInput.getData();
            Attribute distretizeAttribute = Util.findAttributeByName(inputSet, distretizeAttributeName);

            initDataSet();
            assign(alpha);

            for (NumericInterval interval : intervals) {
                //logError("upper:" + interval.getUpperBound() + ";lower:" + interval.getLowerBound());
            }


            Collection<NumericInterval> resultIntervals = interval2();

            //logError("5");
            for (NumericInterval interval : resultIntervals) {
             //  logError("upper:" + interval.getUpperBound() + ";lower:" + interval.getLowerBound());
            }

            deliver(inputSet, resultIntervals, distretizeAttribute, distretizeAttributeName + "_DISCRETIZED");
        } catch (Exception e) {
            Util.printStackTrace(this, e);
        }
    }

    private void initDataSet() throws OperatorException {
        ExampleSet inputSet = datasetInput.getData();
        String targetAttributeName = getParameterAsString(PARAMETER_TARGET);
        String distretizeAttributeName = getParameterAsString(PARAMETER_DISCRETIZED);

        Attribute targetAttribute = Util.findAttributeByName(inputSet, targetAttributeName);
        Attribute distretizeAttribute = Util.findAttributeByName(inputSet, distretizeAttributeName);


        //CREATE CLASS-VALUE INFORMATION STRUCTURE
        for (int i = 0; i < inputSet.size(); i++) {
            Example example = inputSet.getExample(i);
            Double numericValue = example.getNumericalValue(distretizeAttribute);
            String targetValue = example.getValueAsString(targetAttribute);
            ValueCount count = getClassCount(targetValue, normalCounts);

            if (count == null) {
                count = new ValueCount(targetValue);
                normalCounts.add(count);
            } else {
                count.count++;
            }

            NumericInterval interval = (NumericInterval) Util.findValueSetByAttributeValue(intervals, numericValue);

            if (interval != null) {
                interval.incTarget(targetValue);
            } else {
                NumericInterval newInterval = new NumericInterval(distretizeAttribute, targetAttribute);
                newInterval.addExample(example);
                intervals.add(newInterval);

            }
        }

        Collections.sort(intervals, new NumericIntervalCustomComparator());

    }

    private void processThreeIntervals(Queue<NumericInterval> intervalsSource, Queue<NumericInterval> intervalsDestination) {
        NumericInterval first = intervalsSource.poll();
        NumericInterval second = intervalsSource.poll();
        NumericInterval third = intervalsSource.poll();

        String class1 = first.getClassCode();
        String class2 = second.getClassCode();
        String class3 = third.getClassCode();

        if (!class2.equals("?") || !class1.equals(class3)) {
            intervalsDestination.add(first);
            intervalsDestination.add(second);
            intervalsDestination.add(third);
        } else {
            first.append(second);
            first.append(third);
            intervalsDestination.add(first);
        }


    }

    private void processTwoIntervals(Queue<NumericInterval> intervalsSource, Queue<NumericInterval> intervalsDestination) {
        NumericInterval first = intervalsSource.poll();
        NumericInterval second = intervalsSource.poll();

        String class1 = first.getClassCode();
        String class2 = second.getClassCode();


        if (!class1.equals("?") && !class2.equals("?") && !class1.equals(class2)) {
            //different classes, do nothing

            intervalsDestination.add(first);
            intervalsDestination.add(second);
            return;
        }

        String newClass = "";
        if (class1.equals("?")) {
            newClass = class2;
            first.setClassCode(class2);
        } else {
            newClass = class1;
        }

        first.append(second);
        intervalsDestination.add(first);

        return;
    }

    private void assign(double alpha) {
        for (NumericInterval interval : intervals) {
            interval.assign(normalCounts, alpha);
        }

        for(NumericInterval interval : intervals) {
            //logError(interval.toString());
        }


    }

    private Collection<NumericInterval> interval2() throws UndefinedParameterError {
        int minIntervalSize = getParameterAsInt(PARAMETER_MIN_INTERVAL_SIZE);

        List<NumericInterval> newIntervals = new ArrayList<>();

        NumericInterval previousInterval = null;
        for(NumericInterval interval : intervals) {
            if(Double.isNaN(interval.getUpperBound())){
                continue;
            }

            if(previousInterval == null) {
                previousInterval = interval;
            }
            else if (!previousInterval.getClassCode().equals(interval.getClassCode())) {
                newIntervals.add(previousInterval);
                previousInterval = interval;
            }
            else {
                interval.append(previousInterval);
                previousInterval = interval;

            }
        }

        newIntervals.add(previousInterval);

        for(NumericInterval interval : newIntervals) {
            if(interval.examplesSize() < minIntervalSize) {
                interval.setClassCode("?");
            }
        }


        LinkedList<NumericInterval> intervalsToProcess = new LinkedList<>(newIntervals);
        Queue<NumericInterval> processedIntervals = new LinkedList<>();

        while (!intervalsToProcess.isEmpty()) {
            switch (intervalsToProcess.size()) {
                case 1:
                    //logError("Last interval " + intervalsToProcess.peek().toString());
                    processedIntervals.add(intervalsToProcess.poll());

                    break;
                case 2:
                    NumericInterval firstI = intervalsToProcess.get(0);
                    NumericInterval secondI = intervalsToProcess.get(1);
                    //logError("Two last intervals " + firstI.toString() + ";" + secondI.toString());

                    if(firstI.getClassCode().equals(secondI.getClassCode())) {
                        firstI.append(secondI);
                        processedIntervals.add(firstI);
                        //logError("Combined interval " + firstI.toString());

                    }
                    else if(!firstI.getClassCode().equals("?") && !secondI.getClassCode().equals("?")){
                        processedIntervals.add(firstI);
                        processedIntervals.add(secondI);
                    }
                    else if(firstI.getClassCode().equals("?")) {
                        secondI.append(firstI);
                        processedIntervals.add(secondI);
                    }
                    else if(secondI.getClassCode().equals("?")) {
                        firstI.append(secondI);
                        processedIntervals.add(firstI);
                    }
                    intervalsToProcess.poll();
                    intervalsToProcess.poll();
                    break;
                default:

                    NumericInterval first = intervalsToProcess.get(0);
                    NumericInterval second = intervalsToProcess.get(1);
                    NumericInterval third = intervalsToProcess.get(2);
                    //logError("Three intervals " + first.toString() + ";" + second.toString() + ";" + third.toString());

                    if(first.getClassCode().equals(second.getClassCode())) {
                        second.append(first);
                        intervalsToProcess.poll();

                      //  logError("Combined two intervals " + second.toString());
                    }
                    else if(second.getClassCode().equals("?")) {
                        if(first.getClassCode().equals(third.getClassCode())) {
                            third.append(second);
                            third.append(first);

                     //       logError("Combined three intervals " + third.toString());

                            intervalsToProcess.poll();
                            intervalsToProcess.poll();
                        }
                        else {
                            NumericInterval newInterval1 = new NumericInterval(first.getValueAttribute(), first.getTargetAttribute());
                            NumericInterval newInterval2 = new NumericInterval(first.getValueAttribute(), first.getTargetAttribute());

                            newInterval1.append(first);
                            newInterval1.append(second);
                            newInterval1.setClassCode(first.getClassCode());

                            newInterval2.append(second);
                            newInterval2.append(third);
                            newInterval2.setClassCode(third.getClassCode());

                            double chiSq1 = newInterval1.chiSq(normalCounts);
                            double chiSq2 = newInterval2.chiSq(normalCounts);

                            if (chiSq1 < chiSq2) { //first and second
                                second.append(first);

                            //    logError("Combined first two intervals " + second.toString());

                                intervalsToProcess.poll();
                            } else { //second and third
                                processedIntervals.add(first);
                                third.append(second);

                            //    logError("Combined second two intervals " + third.toString());
                                intervalsToProcess.poll();
                                intervalsToProcess.poll();
                            }
                        }
                    }
                    else {
                        processedIntervals.add(intervalsToProcess.poll());
                    }

                    break;
            }
        }
            return processedIntervals;
    }

    private Collection<NumericInterval> interval() throws UndefinedParameterError {
        int minIntervalSize = getParameterAsInt(PARAMETER_MIN_INTERVAL_SIZE);
        List<NumericInterval> newIntervals = new ArrayList<>();
        String currentClassCode = "";
        NumericInterval currentInterval = null;

        //logError("Phase 1");

        for (NumericInterval interval : intervals) {
            String classCode = interval.getClassCode();

            if (!currentClassCode.equals(classCode)) {

                currentClassCode = classCode;
                currentInterval = interval;

                newIntervals.add(interval);
               // logError("Interval added " + interval.toString());
            } else {
                currentInterval.append(interval);
            //    logError("Interval appended " + interval.toString() + " to " + currentInterval.toString());
            }
        }

     //   logError("Phase 2");

        for (NumericInterval interval : newIntervals) {
            if (interval.examplesSize() < minIntervalSize) {
                interval.setClassCode("?");
            }
        }

        LinkedList<NumericInterval> intervalsToProcess = new LinkedList<>(newIntervals);
        Queue<NumericInterval> processedIntervals = new LinkedList<>();

        while (!intervalsToProcess.isEmpty()) {
            switch (intervalsToProcess.size()) {
                case 1:
                    processedIntervals.add(intervalsToProcess.poll());
                    break;
                case 2:
                    processTwoIntervals(intervalsToProcess, processedIntervals);
                    break;
                default:
                    NumericInterval first = intervalsToProcess.peekFirst();
                    NumericInterval second = intervalsToProcess.get(1);
                    NumericInterval third = intervalsToProcess.get(2);
                    if (second.equals("?")) {
                        if (third.getClassCode().equals(first.getClassCode())) {
                            processThreeIntervals(intervalsToProcess, processedIntervals);
                        } else {
                            NumericInterval newInterval1 = new NumericInterval(first.getValueAttribute(), first.getTargetAttribute());
                            NumericInterval newInterval2 = new NumericInterval(first.getValueAttribute(), first.getTargetAttribute());

                            newInterval1.append(first);
                            newInterval1.append(second);
                            newInterval1.setClassCode(first.getClassCode());

                            newInterval2.append(second);
                            newInterval2.append(third);
                            newInterval2.setClassCode(third.getClassCode());

                            double chiSq1 = newInterval1.chiSq(normalCounts);
                            double chiSq2 = newInterval2.chiSq(normalCounts);

                            if (chiSq1 < chiSq2) { //first and second
                                processTwoIntervals(intervalsToProcess, processedIntervals);
                            } else { //second and third
                                processedIntervals.add(intervalsToProcess.poll());
                                processTwoIntervals(intervalsToProcess, processedIntervals);
                            }

                        }
                    } else {
                        processedIntervals.add(intervalsToProcess.poll());
                    }


                    break;
            }


        }
        return processedIntervals;

    }

    public static ValueCount getClassCount(String targetValue, List<ValueCount> list) {
        for (ValueCount count : list) {
            if (count.value.equals(targetValue)) {
                return count;
            }
        }

        return null;
    }

    class NumericIntervalCustomComparator implements Comparator<NumericInterval> {
        @Override
        public int compare(NumericInterval o1, NumericInterval o2) {

            return Double.compare(o1.getUpperBound(), o2.getUpperBound());
        }
    }

    private void deliver(ExampleSet inputSet, Collection<NumericInterval> discretizedIntevals, Attribute attributeToDiscretize, String discretizedAttributeName) {
        //logError("Delivery start");
        Attribute attribute = AttributeFactory.createAttribute(discretizedAttributeName, Ontology.STRING);
        attribute.setTableIndex(inputSet.size());

        inputSet.getExampleTable().addAttribute(attribute);
        inputSet.getAttributes().addRegular(attribute);

        for (Example example : inputSet) {
            Double value = example.getNumericalValue(attributeToDiscretize);

            NumericInterval interval = findInterval(discretizedIntevals, value);
            if (interval != null) {
                example.setValue(attribute, interval.toString());
            } else {
                example.setValue(attribute, "?");
            }

        }


        outputPort.deliver(inputSet);
        //logError("Delivery end");
    }

    public NumericInterval findInterval(Collection<NumericInterval> collection, double value) {
        for (NumericInterval interval : collection) {
            if (interval.getUpperBound() >= value && interval.getLowerBound() <= value) {
                return interval;
            }
        }

        return null;
    }

}
