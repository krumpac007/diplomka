package cz.vse.dataprepaddon.data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.tools.Ontology;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan on 19.4.2015.
 */
public class ValueSet {
    protected List<ValueCount> values;
    private List<Example> examples;
    private Attribute valueAttribute;

    public ValueSet(Attribute valueAttribute) {
        values = new ArrayList<>();
        examples = new ArrayList<>();
        this.valueAttribute = valueAttribute;
    }

    public boolean addExample(Example example) {
        Object value = null;
        switch(valueAttribute.getValueType()) {
            case Ontology.REAL:
            case Ontology.INTEGER:
            case Ontology.NUMERICAL:
                value = new Double(example.getNumericalValue(valueAttribute));
                break;
            default:
                value = example.getValueAsString(valueAttribute);
                break;
        }

        examples.add(example);
        return addValue(values, value);
    }

    /**
     * Add value, return true if new value was added
     * @param value
     * @return
     */
    protected boolean addValue(List<ValueCount> values, Object value) {
        if(value == null) {
            return false;
        }

        for(ValueCount valueCount: values) {
            if(valueCount.value.equals(value)) {
                valueCount.inc();
                return false;
            }
        }

        ValueCount valueCount = new ValueCount(value);
        values.add(valueCount);
        return true;
    }

    public List<Example> getExamples() {
        return examples;
    }

    public List<ValueCount> getValues() {
        return values;
    }

    public void append(ValueSet other) {
        examples.addAll(other.getExamples());
        values.addAll(other.getValues());
    }

    public Attribute getValueAttribute() {
        return valueAttribute;
    }

    public int examplesSize() {
        return examples.size();
    }

    public int uniqueValuesSize() {
        return values.size();
    }

    public boolean containsValue(Object value) {
        for(ValueCount valueCount : values) {
            if(valueCount.value.equals(value)) {
                return true;
            }
        }

        return false;
    }
}
