package cz.vse.dataprepaddon.data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;

/**
 * Created by Jan on 19.4.2015.
 */
public class NoiseValueSet extends ValueSet{
    public NoiseValueSet(Attribute valueAttribute) {
        super(valueAttribute);
    }

    public double calcuateAccuracy() {
        int sum = 0;
        ValueCount mostCommon = null;

        for(ValueCount targetValue : values) {
            if(mostCommon == null || mostCommon.count < targetValue.count) {
                mostCommon = targetValue;

                sum += targetValue.count;
            }
        }

        return mostCommon.count / sum;
    }

    public int calcuateContradictions() {
        int contradictions = 0;
        ValueCount mostCommon = null;

        for(ValueCount targetValue : values) {
            if(mostCommon == null || mostCommon.count < targetValue.count) {
                mostCommon = targetValue;
            }
        }

        for(ValueCount targetValue : values) {
            if(!targetValue.value.equals(mostCommon.value)) {
                contradictions+= targetValue.count;
            }
        }

        return contradictions;
    }

    public static int calculateHash(Attribute target, Example example) {
        String hashEntry = "";
        for(Attribute attribute : example.getAttributes()) {
            if(!attribute.equals(target)) {
                hashEntry += example.getValueAsString(attribute);
            }
        }

        return hashEntry.hashCode();
    }
}
