package cz.vse.dataprepaddon;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.*;
import com.rapidminer.operator.Operator;
import com.rapidminer.tools.Ontology;
import com.rapidminer.tools.container.Pair;
import cz.vse.dataprepaddon.data.ValueClassSet;
import cz.vse.dataprepaddon.data.ValueCount;
import cz.vse.dataprepaddon.data.ValueSet;
import org.apache.commons.math3.stat.inference.ChiSquareTest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan on 13.4.2015.
 */
public class Util {
    static ExampleTable createExampleTable(List<Pair<String, Object>> values) {
            //List<Attribute> attributes, Iterable<Row> inputRows) {

        List<Attribute> attributes = new ArrayList<>();
        for(Pair<String, Object> value : values) {
            Attribute attribute = AttributeFactory.createAttribute(value.getFirst(), Ontology.REAL);
            attributes.add(attribute);
            attribute.setDefault((Double)value.getSecond());
        }

        MemoryExampleTable table = new MemoryExampleTable(attributes);

        //for(Row row : inputRows){
          //  DataRow dataRow = factory.createRow(row);
           // table.addDataRow(dataRow);
        //}
        return table;
    }

    public static Attribute findAttributeByName(ExampleSet exampleSet, String attributeName) {
        for(Attribute attribute : exampleSet.getAttributes()) {
            if(attribute.getName().equals(attributeName)) {
                return attribute;
            }
        }

        return null;
    }

    public static ValueCount getClassCount(String targetValue, List<ValueCount> list) {
        for(ValueCount count : list) {
            if(count.value.equals(targetValue)) {
                return count;
            }
        }

        return null;
    }

    public static double chiSq(List<ValueCount> normalCounts, List<ValueCount> groupCounts) {
        double[] expectedCounts = new double[normalCounts.size()];
        long[] actualCounts = new long[normalCounts.size()];


        int actualCountsSum = groupCounts.size();
        int allCountsSum = normalCounts.size();

        double ratio = (double)actualCountsSum / (double) allCountsSum;

        for(int i = 0; i < normalCounts.size(); i++) {
            ValueCount count = normalCounts.get(i);

            expectedCounts[i] = (double) count.count * ratio;

            actualCounts[i] = 0;

            for(ValueCount countActual : groupCounts) {
                if(countActual.value.equals(count.value)) {
                    actualCounts[i] = countActual.count;
                }
            }
        }

        for(int i = 0; i < expectedCounts.length; i++) {
        }

        ChiSquareTest test = new ChiSquareTest();
        //    double result = test.chiSquareTest(expectedCounts, actualCounts);
        double result =  test.chiSquareTest(expectedCounts, actualCounts);
        return result;
    }

    public static boolean chiSq(List<ValueCount> normalCounts, List<ValueCount> groupCounts, double alpha) {
        double[] expectedCounts = new double[normalCounts.size()];
        long[] actualCounts = new long[normalCounts.size()];


        int actualCountsSum = groupCounts.size();
        int allCountsSum = normalCounts.size();

        double ratio = (double)actualCountsSum / (double) allCountsSum;

        for(int i = 0; i < normalCounts.size(); i++) {
            ValueCount count = normalCounts.get(i);

            expectedCounts[i] = (double) count.count * ratio;

            actualCounts[i] = 0;

            for(ValueCount countActual : groupCounts) {
                if(countActual.value.equals(count.value)) {
                    actualCounts[i] = countActual.count;
                }
            }
        }

        for(int i = 0; i < expectedCounts.length; i++) {
        }

        ChiSquareTest test = new ChiSquareTest();
    //    double result = test.chiSquareTest(expectedCounts, actualCounts);
        boolean result =  test.chiSquareTest(expectedCounts, actualCounts, alpha);
        return result;
    }


    public static ValueSet findValueSetByAttributeValue(List<? extends ValueClassSet> valueSets, Object attributeValue) {
        for(ValueSet set : valueSets) {
            if(set.containsValue(attributeValue)) {
                return set;
            }
        }
        return null;
    }

    public static void printStackTrace(Operator o, Exception e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
    }

    public static ExampleSet createResultsSet(Attribute[] attributes, Object[] resultValues) {

        DataRowFactory dataRowFactory = new DataRowFactory(DataRowFactory.TYPE_DOUBLE_ARRAY, '.');

            DataRow resultRow = dataRowFactory.create(3);

            MemoryExampleTable table = new MemoryExampleTable(attributes);
            table.addDataRow(resultRow);
            ExampleSet output = table.createExampleSet();

            for(int i = 0; i < attributes.length; i++) {
                Object value = resultValues[i];
                if(value instanceof Number) {
                    output.getExample(0).setValue(attributes[i], ((Number) value).doubleValue());
                }
                else {
                    output.getExample(0).setValue(attributes[i], value.toString());
                }
            }

        return output;
    }

    public static double log(double x, int base)
    {
        return (Math.log(x) / Math.log(base));
    }

}
