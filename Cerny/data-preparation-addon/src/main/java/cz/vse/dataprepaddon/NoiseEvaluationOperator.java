package cz.vse.dataprepaddon;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.SimpleExampleSet;
import com.rapidminer.example.table.*;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.tools.Ontology;
import cz.vse.dataprepaddon.data.NoiseValueSet;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jan on 31.1.2015.
 */
public class NoiseEvaluationOperator extends Operator {
    public static final String PARAMETER_TARGET = "Target attribute";
    private Map<Integer, NoiseValueSet> entries;
    public InputPort datasetInput = getInputPorts().createPort("Input dataset");
    public OutputPort outputPort = getOutputPorts().createPort("Contradictions, error, accuracy");
    public NoiseEvaluationOperator(OperatorDescription description) {
        super(description);
    }


    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeAttribute(PARAMETER_TARGET, "Target attribute", datasetInput));
        return types;
    }

    @Override
    public void doWork() throws OperatorException {
        //initialization
        entries = new HashMap<>();

        ExampleSet inputSet = datasetInput.getData();

        Attributes attributes = inputSet.getAttributes();
        String targetAttributeName = getParameterAsString(PARAMETER_TARGET);
        Attribute targetAttribute = null;

        for(Attribute attribute : attributes) {
            if(attribute.getName().equals(targetAttributeName)) {
                targetAttribute = attribute;
                break;
            }
        }

        if(targetAttribute == null) {
            return; //TODO - throw error about badly enetered parameter - target
        }

        //create a structure for all entry attribute combinations, count occurence of all target values
        for(int i = 0; i < inputSet.size(); i++) {
            Example example = inputSet.getExample(i);

            Integer hash = NoiseValueSet.calculateHash(targetAttribute, example);

            if(entries.containsKey(hash)) {
                entries.get(hash).addExample(example);
            }
            else {
                NoiseValueSet newObjectCountAndClass = new NoiseValueSet(targetAttribute);
                newObjectCountAndClass.addExample(example);
                entries.put(hash, newObjectCountAndClass);
            }
        }

        //calculate the sum of all and of the correct - non contradictory objects

        int sum = 0;
        int contradictions = 0;


        for(NoiseValueSet objectCountAndClass : entries.values()) {
            contradictions += objectCountAndClass.calcuateContradictions();
            sum += objectCountAndClass.examplesSize();
        }

        double error = (double)contradictions / (double)sum;
        double accuracy = 1 - error;

        Attribute contradictionsCountAttr = AttributeFactory.createAttribute("contradictions", Ontology.REAL);
        Attribute accuracyAttr = AttributeFactory.createAttribute("accuracy", Ontology.REAL);
        Attribute errorAttr = AttributeFactory.createAttribute("error", Ontology.REAL);

        Attribute[] resultAttributes = {contradictionsCountAttr, accuracyAttr, errorAttr};

        ExampleSet output = Util.createResultsSet(resultAttributes, new Object[]{new Integer(contradictions), new Double(accuracy), new Double(error)});
        outputPort.deliver(output);


    }

}
