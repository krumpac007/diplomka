package cz.vse.dataprepaddon;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.*;
import com.rapidminer.tools.Ontology;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Jan on 11.4.2015.
 */
public class AttributeRelevanceOperator extends Operator {
    public static final String PARAMETER_TARGET = "Target attribute";
    public static final String PARAMETER_ATTRIBUTE = "Attribute to evaluate";
    public static final String PARAMETER_MEASURE = "Evaluation Measure";
    public static final String PARAMETER_ALPHA = "ChiSq Level of Significance (only relevant for ChiSq measure";
    public static final String MEASURE_CHISQ = "Chi Square";
    public static final String MEASURE_ENTROPY = "Entropy";
    public static final String MEASURE_MI = "Mutual Information";

    public static final String[] MEASURE_TYPES = {MEASURE_CHISQ, MEASURE_ENTROPY, MEASURE_MI};

    public InputPort datasetInput = getInputPorts().createPort("Input dataset");
    public OutputPort outputPort = getOutputPorts().createPort("Measure of relevance");

    public AttributeRelevanceOperator(OperatorDescription description) {
        super(description);
    }

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> types = super.getParameterTypes();
        types.add(new ParameterTypeAttribute(PARAMETER_TARGET, "Target attribute", datasetInput));
        types.add(new ParameterTypeAttribute(PARAMETER_ATTRIBUTE, "Analyzed attribute", datasetInput));


        ParameterType measureParameterType = new ParameterTypeStringCategory(PARAMETER_MEASURE,"Measure type", MEASURE_TYPES);

        types.add(measureParameterType);
       // types.add(new ParameterTypeDouble(PARAMETER_ALPHA,"TODO",0,0.5));

        return types;
    }

    @Override
    public void doWork() throws OperatorException {
        try {
            ExampleSet inputSet = datasetInput.getData();
            String measureType = getParameterAsString(PARAMETER_MEASURE);

            RealMatrix matrix = initMatrix(inputSet);

            double measureValue = 0;

            switch (measureType) {
                case MEASURE_CHISQ:
                    measureValue = computeChiSq(matrix);
                    break;
                case MEASURE_ENTROPY:
                    measureValue = computeEntropy(matrix);
                    break;
                case MEASURE_MI:
                    measureValue = computeMutualInformation(matrix);
                    break;
            }

            Attribute attribute = AttributeFactory.createAttribute(measureType, Ontology.REAL);

            //logError("tt:" + measureValue);

            ExampleSet output = Util.createResultsSet(new Attribute[]{attribute}, new Object[]{new Double(measureValue)});

            outputPort.deliver(output);
        } catch (Exception e) {
            Util.printStackTrace(this, e);
        }
    }

    private RealMatrix initMatrix(ExampleSet inputSet) throws OperatorException{
        String targetAttributeName = getParameterAsString(PARAMETER_TARGET);
        String analyzedAttributeName = getParameterAsString(PARAMETER_ATTRIBUTE);

        Attribute targetAttribute = null;
        Attribute analyzedAttribute = null;

        //find attributes
        for(Attribute attribute : inputSet.getAttributes()) {
            if(attribute.getName().equals(targetAttributeName)) {
                targetAttribute = attribute;
            }
            else if(attribute.getName().equals(analyzedAttributeName)) {
                analyzedAttribute = attribute;
            }

            if(analyzedAttribute != null && targetAttribute != null) {
                break;
            }
        }

        if(analyzedAttribute == null || targetAttribute == null) {
            //logError("Error: Can't find attribute");
            return null;
        }

        //get list of values for target and analyzed attribute
        List<String> targetValues = getValues(inputSet, targetAttribute);
        List<String> analyzedAttributeValues = getValues(inputSet, analyzedAttribute);

        //init matrix
        RealMatrix matrix = new Array2DRowRealMatrix(analyzedAttributeValues.size(), targetValues.size());


        for(int i = 0; i < analyzedAttributeValues.size(); i++) {
            for(int j = 0; j < targetValues.size(); j++) {
                matrix.setEntry(i,j,0);
            }
        }

        //load matrix with data
        for(Example example : inputSet) {
            String analyzedValue = example.getValueAsString(analyzedAttribute);
            String targetValue = example.getValueAsString(targetAttribute);

            int i = analyzedAttributeValues.lastIndexOf(analyzedValue);
            int j = targetValues.lastIndexOf(targetValue);

            if(i != -1 && j != -1) {
                matrix.addToEntry(i, j, 1);
            }
            else {
               // //logError("No match for values" + analyzedValue + ";" + targetValue + ";" + i + ";" + j);
            }
            ////logError("i:" + i + ";j:" + j + ";" + matrix.getEntry(i,j));
        }
        //logError(analyzedAttributeValues.toString());
        //logError(targetValues.toString());
        //logError(matrix.toString());

        return matrix;
    }


    private List<String> getValues(ExampleSet exampleSet, Attribute attribute) {
        Set<String> values = new TreeSet<>();
        for(Example example : exampleSet) {
            String value = example.getValueAsString(attribute);

            values.add(value);
        }

        List<String> result = new ArrayList<>();
        result.addAll(values);

        return result;
    }

    private double computeChiSq(RealMatrix matrix) {
        double n = getSum(matrix);
        double result = 0;

        for(int i = 0; i < matrix.getRowDimension(); i++) {
            for (int j = 0; j < matrix.getColumnDimension(); j++) {
                double r = getColSum(matrix,j);
                double s = getRowSum(matrix,i);
                double bottom = r*s;
                double a = matrix.getEntry(i,j);
                double top = Math.pow(matrix.getEntry(i,j)-(bottom/n),2);

                result += top/bottom;
            }
        }

        result*=n;

        return result;
    }

    private double computeEntropy(RealMatrix matrix) {
        double sum = 0;
        for(int i = 0; i < matrix.getRowDimension(); i++) {
            double riDivn = getRowSum(matrix,i)/getSum(matrix);
            double sumPart = riDivn*entropyComputeHi(matrix, i);

            sum += sumPart;
        }
        return sum;
    }

    private double entropyComputeHi(RealMatrix matrix, int i) {
        double sum = 0;
        double rowSum = getRowSum(matrix,i);

        for(int j = 0; j < matrix.getColumnDimension(); j++) {
            double aDivByR = matrix.getEntry(i,j)/rowSum;
            double log = Util.log(aDivByR,matrix.getColumnDimension());
            //logError("adivByR:" + aDivByR + ";log:" + log);
            if(aDivByR == 0) {
                sum += 0;
            }
            else {
                sum += -aDivByR * log;
            }
        }

        return sum;
    }

    private double computeMutualInformation(RealMatrix matrix) {
        double MI = mutualComputeMI(matrix);
        double n = getSum(matrix);

        double bottomSum = 0;

        for(int j = 0; j < matrix.getColumnDimension();j++) {
            double sDivn = getColSum(matrix,j)/ n;
            if(sDivn != 0) {
                bottomSum+= - sDivn * Util.log(sDivn, matrix.getColumnDimension());
            }
        }

        double result = MI/bottomSum;
        return result;
    }

    private double mutualComputeMI(RealMatrix matrix) {
        double sum = 0;
        double n = getSum(matrix);

        for(int i = 0; i < matrix.getRowDimension(); i++) {
            for (int j = 0; j < matrix.getColumnDimension(); j++) {
                double a = matrix.getEntry(i,j);
                double log = Util.log((n*a)/(getRowSum(matrix,i)*getColSum(matrix,j)), matrix.getColumnDimension());

                if(a != 0)
                    sum+= a*log;
            }
        }
        sum *= 1/n;

        return sum;
    }

    private double getColSum(RealMatrix matrix, int j) {
        double sum = 0;
        for(int i = 0; i < matrix.getRowDimension(); i++) {
            sum+= matrix.getEntry(i,j);
        }

        //logError("colSum" + j + ":" + sum);

        return sum;
    }

    private double getRowSum(RealMatrix matrix, int i) {
        double sum = 0;
        for(int j = 0; j < matrix.getColumnDimension(); j++) {
            sum+= matrix.getEntry(i,j);
        }

        //logError("rowSum" + i + ":" + sum);
        return sum;
    }

    private double getSum(RealMatrix matrix) {
        double sum = 0;

        for(int i = 0; i < matrix.getRowDimension(); i++) {
            for(int j = 0; j < matrix.getColumnDimension(); j++) {
                sum += matrix.getEntry(i,j);
            }
        }
        //logError("sum:" + sum);
        return sum;
    }

}
