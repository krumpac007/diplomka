package cz.vse.dataprepaddon;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.operator.ports.metadata.SimplePrecondition;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeInt;

import java.util.List;

/**
 * Created by Jan on 30.1.2015.
 */
public class TestOperator extends Operator {
    public InputPort exampleSetInput = getInputPorts().createPort("example set input");
    public OutputPort exampleSetOutput = getOutputPorts().createPort("example set input");

    public static final String PARAMETERTRAININGPERCENTAGE="percentage of training data";

    @Override
    public List<ParameterType> getParameterTypes() {
        List<ParameterType> parameterTypes = super.getParameterTypes();
        parameterTypes.add(new ParameterTypeInt(PARAMETERTRAININGPERCENTAGE, "This parameter defines the percentage of training data", 0,100,30, false));

        return parameterTypes;
    }

    public TestOperator(OperatorDescription description) {
        super(description);

        exampleSetInput.addPrecondition(new SimplePrecondition(exampleSetInput,new MetaData(ExampleSet.class)));
    }

    @Override
    public void doWork() throws OperatorException {
        ExampleSet input = exampleSetInput.getData();

       // exampleSetOutput.deliver(output);
    }
}
