package cz.vse.dataprepaddon.data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;

/**
 * Created by Jan on 19.4.2015.
 */
public class CategoryGroup extends ValueClassSet {
    private String groupID = "";
    public CategoryGroup(Attribute valueAttribute, Attribute targetAttribute) {
        super(valueAttribute,targetAttribute);
    }

    @Override
    public boolean addExample(Example example) {
        boolean added = super.addExample(example);
        if(added) {
            updateGroupID(example);
        }
        return added;
    }

    @Override
    public void append(ValueSet other) {
        super.append(other);

        if(other instanceof CategoryGroup) {
            CategoryGroup group = (CategoryGroup) other;

            groupID += "_"+group.getGroupID();
        }
        else {
            //TODO - throw error
        }
    }

    private void updateGroupID(Example example) {
        String value = example.getValueAsString(getValueAttribute());


        if(groupID.equals("")) {
            groupID = value;
        }
        else {
            groupID+="_" + value;
        }
    }

    public String getGroupID() {
        return groupID;
    }

    public String getGroupIDWithClassInfo() {
        return groupID+"_class_" + getClassCode();
    }
}
