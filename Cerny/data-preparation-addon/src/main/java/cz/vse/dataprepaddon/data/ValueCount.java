package cz.vse.dataprepaddon.data;

/**
 * Created by Jan on 19.4.2015.
 */
public class ValueCount {
    public Object value;
    public int count;

    public ValueCount(Object value) {
        this.value = value;
        count = 1;
    }

    public void inc() {
        count++;
    }
}
