package cz.vse.dataprepaddon.data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import cz.vse.dataprepaddon.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jan on 20.4.2015.
 */
public class ValueClassSet extends ValueSet {
    private List<ValueCount> targetValues;
    private Attribute targetAttribute;
    private String classCode;

    public ValueClassSet(Attribute valueAttribute, Attribute targetAttribute) {
        super(valueAttribute);
        this.targetAttribute = targetAttribute;

        targetValues = new ArrayList<>();
    }

    @Override
    public boolean addExample(Example example) {
        super.addExample(example);

        String targetValue = example.getValueAsString(targetAttribute);
        return  addValue(targetValues, targetValue);

    }


    @Override
    public void append(ValueSet other) {
        super.append(other);

        if(other instanceof ValueClassSet) {
            ValueClassSet otherInterval = (ValueClassSet) other;

            targetValues.addAll(otherInterval.targetValues);
        }
        else
        {
            //TODO - throw error
        }



    }

    public boolean chiSq(List<ValueCount> normalCounts, double alpha) {
        return Util.chiSq(normalCounts, targetValues, alpha);
    }

    public double chiSq(List<ValueCount> normalCounts) {
        return Util.chiSq(normalCounts, targetValues);
    }

    public boolean containsTargetValue(Object value) {
        for(ValueCount valueCount : targetValues) {
            if(valueCount.value.equals(value)) {
                return true;
            }
        }

        return false;
    }

    public ValueCount getTargetValue(Object value) {
        for(ValueCount valueCount : targetValues) {
            if(valueCount.value.equals(value)) {
                return valueCount;
            }
        }

        return null;
    }

    public void  incTarget(Object targetAttributeValue) {
        ValueCount targetValue =getTargetValue(targetAttributeValue);
        if(targetValue != null) {
            targetValue.inc();
        }
        else {
            ValueCount newValue = new ValueCount(targetAttributeValue);
            targetValues.add(newValue);
        }


    }

    public void assign(List<ValueCount> normalCounts, double alpha) {
        if(targetValues.size() == 1) {
            classCode = targetValues.get(0).value.toString();
        }
        else if(chiSq(normalCounts,alpha)) {
            classCode = mostCommon();

        }
        else {
            classCode = "?";
        }
    }

    private String mostCommon() {
        int max = 0;
        String result = null;
        for(ValueCount count : targetValues) {
            if(count.count > max) {
                result = count.value.toString();
                max = count.count;
            }
        }

        return result;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public Attribute getTargetAttribute() {
        return targetAttribute;
    }
}
